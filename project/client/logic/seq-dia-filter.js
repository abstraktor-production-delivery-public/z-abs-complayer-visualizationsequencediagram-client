
'use strict';

import Const from './const';
import LogDataGuiSubType from 'z-abs-funclayer-engine-cs/clientServer/log/log-data-gui-sub-type';
import ActorPhaseConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';


class SeqDiaFilter {
  constructor() {
    this.zoomFilter = null;
    this.eventListeners = new Map();
    this.filters = new Map();
    
    // FILTER VALUES
    
    this.executedStateEvents = true;
    this.notExecutedStateEvents = true;
    
    this.serverEvents = true;
    this.connectionEvents = true;
    
    this.messageEvents = true;
    this.messageDetailEvents = true;
    
    this.protocolData = true;
    this.protocolInfoData = false;
    this.protocolIpData = true;
    this.protocolAddressNameData = false;
    this.protocolNameData = true;
    this.protocolTransportData = false;
    this.protocolInstanceData = true;
    
    this.stackEvents = true;
    this.stackInfoEvents = false;
    this.stackInstanceEvents = true;
    this.stackProtocolEvents = true;
    
    this.guiEvents = true;
    this.guiObjectEvents = true;
    this.guiActionEvents = true;
    this.guiFunctionEvents = true;
    this.guiInfoData = true;
    this.guiInstanceData = true;
    this.guiProtocolData = true;
    
    this.phases = [true, true, true, true, true];
    
    //////////////////////////////
        
    this.filterShowMessageContent = true;
    this.filterShowMessageContentOpen = true;
    this.filterShowSentMessageNumbers = true;
    this.filterShowReceivedMessageNumbers = true;
    
    this.filterShowAddresses = true;
    
    
    
    // VISIBLE FILTERS
    
    this.seq_dia_show__display__ip_address = null;
    this.seq_dia_show__display__address_name = null;
    
    // KEYS - COMPLEX - COLUMN
    
    this.seq_dia__data_protocol = null;
    this.seq_dia__data_protocol_info = null;
    this.seq_dia__data_protocol_ip = null;
    this.seq_dia__data_protocol_address_name = null;
    this.seq_dia__data_protocol_name = null;
    this.seq_dia__data_protocol_transport = null;
    this.seq_dia__data_protocol_instance = null;
    
    this.seq_dia__data_stack = null;
    this.seq_dia__data_stack_info = null;
    this.seq_dia__data_stack_instance = null;
    this.seq_dia__data_stack_protocol = null;
    
    this.seq_dia__data_gui = null;
    this.seq_dia__data_gui_info = null;
    
    this.seq_dia_show__phase_data__actor_column = null;
    this.seq_dia_show__phase_pre__actor_column = null;
    this.seq_dia_show__phase_exec__actor_column = null;
    this.seq_dia_show__phase_post__actor_column = null;
    this.seq_dia_show__phase_pre_post__actor_column = null;
    this.seq_dia_show__phase_all__actor_column = null;
    
    this.filterCss = document.createElement('style');
    document.head.appendChild(this.filterCss);
    this.styleSheet = this.filterCss.sheet;
  }
  
  init(sequenceDiagramFilter) {
    this.executedStateEvents = sequenceDiagramFilter.executedStateEvents;
    this.notExecutedStateEvents = sequenceDiagramFilter.notExecutedStateEvents;
    
    this.serverEvents = sequenceDiagramFilter.serverEvents;
    this.connectionEvents = sequenceDiagramFilter.connectionEvents;
      
    this.messageEvents = sequenceDiagramFilter.messageEvents;
    this.messageDetailEvents = sequenceDiagramFilter.messageDetailEvents;
    
    this.protocolData = sequenceDiagramFilter.protocolData;
    this.protocolInfoData = sequenceDiagramFilter.protocolInfoData;
    this.protocolIpData = sequenceDiagramFilter.protocolIpData;
    this.protocolAddressNameData = sequenceDiagramFilter.protocolAddressNameData;
    this.protocolNameData = sequenceDiagramFilter.protocolNameData;
    this.protocolTransportData = sequenceDiagramFilter.protocolTransportData;
    this.protocolInstanceData = sequenceDiagramFilter.protocolInstanceData;
    
    this.stackEvents = sequenceDiagramFilter.stackEvents;
    this.stackInfoEvents = sequenceDiagramFilter.stackInfoEvents;
    this.stackInstanceEvents = sequenceDiagramFilter.stackInstanceEvents;
    this.stackProtocolEvents = sequenceDiagramFilter.stackProtocolEvents;
    
    this.guiEvents = sequenceDiagramFilter.guiEvents;
    this.guiObjectEvents = sequenceDiagramFilter.guiObjectEvents;
    this.guiActionEvents = sequenceDiagramFilter.guiActionEvents;
    this.guiFunctionEvents = sequenceDiagramFilter.guiFunctionEvents;
    this.guiInfoData = sequenceDiagramFilter.guiInfoData;
    this.guiInstanceData = sequenceDiagramFilter.guiInstanceData;
    this.guiProtocolData = sequenceDiagramFilter.guiProtocolData;
    
    this.phases = sequenceDiagramFilter.phases;
    
    /////////////////////
    
    
    this.filterShowMessageContent = sequenceDiagramFilter.showMessageContent;
    this.filterShowMessageContentOpen = sequenceDiagramFilter.showMessageContentOpen;
    this.filterShowSentMessageNumbers = sequenceDiagramFilter.showSentMessageNumbers;
    this.filterShowReceivedMessageNumbers = sequenceDiagramFilter.showReceivedMessageNumbers;
    this.filterShowAddresses = sequenceDiagramFilter.showAddresses;
    
    // VISIBLE FILTERS
    
    this.seq_dia_show__display__ip_address = this._createFilter('seq_dia_show__display__ip_address', 'text', sequenceDiagramFilter.showAddresses);
    this.seq_dia_show__display__address_name = this._createFilter('seq_dia_show__display__address_name', 'text', !sequenceDiagramFilter.showAddresses);
    
    // COMPLEX - COLUMN
    
    this.seq_dia__data_protocol = this._createFilter('seq_dia__data_protocol', 'div', sequenceDiagramFilter.protocolData);
    this.seq_dia__data_protocol_info = this._createFilter('seq_dia__data_protocol_info', 'div', sequenceDiagramFilter.protocolInfoData);
    this.seq_dia__data_protocol_ip = this._createFilter('seq_dia__data_protocol_ip', 'div', sequenceDiagramFilter.protocolIpData);
    this.seq_dia__data_protocol_address_name = this._createFilter('seq_dia__data_protocol_address_name', 'div', sequenceDiagramFilter.protocolAddressNameData);
    this.seq_dia__data_protocol_name = this._createFilter('seq_dia__data_protocol_name', 'div', sequenceDiagramFilter.protocolNameData);
    this.seq_dia__data_protocol_transport = this._createFilter('seq_dia__data_protocol_transport', 'div', sequenceDiagramFilter.protocolTransportData);
    this.seq_dia__data_protocol_instance = this._createFilter('seq_dia__data_protocol_instance', 'div', sequenceDiagramFilter.protocolInstanceData);
    
    this.seq_dia__data_stack = this._createFilter('seq_dia__data_stack', 'div', sequenceDiagramFilter.stackEvents);
    this.seq_dia__data_stack_info = this._createFilter('seq_dia__data_stack_info', 'div', sequenceDiagramFilter.stackInfoEvents);
    this.seq_dia__data_stack_instance = this._createFilter('seq_dia__data_stack_instance', 'div', sequenceDiagramFilter.stackInstanceEvents);
    this.seq_dia__data_stack_protocol = this._createFilter('seq_dia__data_stack_protocol', 'div', sequenceDiagramFilter.stackProtocolEvents);
    
    this.seq_dia__data_gui = this._createFilter('seq_dia__data_gui', 'div', sequenceDiagramFilter.guiEvents);
    this.seq_dia__data_gui_info = this._createFilter('seq_dia__data_gui_info', 'div', sequenceDiagramFilter.guiInfoData);
    this.seq_dia__data_gui_instance = this._createFilter('seq_dia__data_gui_instance', 'div', sequenceDiagramFilter.guiInstanceData);
    this.seq_dia__data_gui_protocol = this._createFilter('seq_dia__data_gui_protocol', 'div', sequenceDiagramFilter.guiProtocolData);
        
    ///////////////////
    
    this.seq_dia_show__phase_data__actor_column = this._createFilter('seq_dia_show__phase_data__actor_column', 'div', sequenceDiagramFilter.phases[ActorPhaseConst.DATA]);
    this.seq_dia_show__phase_pre__actor_column = this._createFilter('seq_dia_show__phase_pre__actor_column', 'div', sequenceDiagramFilter.phases[ActorPhaseConst.DATA] || sequenceDiagramFilter.phases[ActorPhaseConst.PRE]);
    this.seq_dia_show__phase_exec__actor_column = this._createFilter('seq_dia_show__phase_exec__actor_column', 'div', sequenceDiagramFilter.phases[ActorPhaseConst.DATA] || sequenceDiagramFilter.phases[ActorPhaseConst.EXEC]);
    this.seq_dia_show__phase_post__actor_column = this._createFilter('seq_dia_show__phase_post__actor_column', 'div', sequenceDiagramFilter.phases[ActorPhaseConst.DATA] || sequenceDiagramFilter.phases[ActorPhaseConst.POST]);
    this.seq_dia_show__phase_pre_post__actor_column = this._createFilter('seq_dia_show__phase_pre_post__actor_column', 'div', sequenceDiagramFilter.phases[ActorPhaseConst.DATA] || sequenceDiagramFilter.phases[ActorPhaseConst.PRE] || sequenceDiagramFilter.phases[ActorPhaseConst.POST]);
    this.seq_dia_show__phase_all__actor_column = this._createFilter('seq_dia_show__phase_all__actor_column', 'div', true, -1);
    
    const cssRules = this.styleSheet.cssRules;
    this.filters.forEach((filter) => {
      const rule = filter.visible ? 'display:block' : 'display:none;'
      const index = this.styleSheet.insertRule(`${filter.type}.${filter.name} { ${rule} }`, cssRules.length);
      filter.index = index;
    });
  }
    
  update(sequenceDiagramFilter) {
    if(this.phases[ActorPhaseConst.DATA] !== sequenceDiagramFilter.phases[ActorPhaseConst.DATA]
      || this.phases[ActorPhaseConst.PRE] !== sequenceDiagramFilter.phases[ActorPhaseConst.PRE]
      || this.phases[ActorPhaseConst.EXEC] !== sequenceDiagramFilter.phases[ActorPhaseConst.EXEC]
      || this.phases[ActorPhaseConst.POST] !== sequenceDiagramFilter.phases[ActorPhaseConst.POST]
      || this.phases[ActorPhaseConst.NONE] !== sequenceDiagramFilter.phases[ActorPhaseConst.NONE]
    ) {
      this.setPhases(sequenceDiagramFilter.phases);
    }
    if(this.executedStateEvents !== sequenceDiagramFilter.executedStateEvents) {
      this.setExecutedStateEvents(sequenceDiagramFilter.executedStateEvents);
    }
    if(this.notExecutedStateEvents !== sequenceDiagramFilter.notExecutedStateEvents) {
      this.setNotExecutedStateEvents(sequenceDiagramFilter.notExecutedStateEvents);
    }
    if(this.serverEvents !== sequenceDiagramFilter.serverEvents) {
      this.setServerEvents(sequenceDiagramFilter.serverEvents);
    }
    if(this.connectionEvents !== sequenceDiagramFilter.connectionEvents) {
      this.setConnectionEvents(sequenceDiagramFilter.connectionEvents);
    }
    if(this.protocolData !== sequenceDiagramFilter.protocolData
      || this.protocolInfoData !== sequenceDiagramFilter.protocolInfoData
      || this.protocolIpData !== sequenceDiagramFilter.protocolIpData
      || this.protocolAddressNameData !== sequenceDiagramFilter.protocolAddressNameData
      || this.protocolNameData !== sequenceDiagramFilter.protocolNameData
      || this.protocolTransportData !== sequenceDiagramFilter.protocolTransportData
      || this.protocolInstanceData !== sequenceDiagramFilter.protocolInstanceData
    ) {
      this.setProtocolData(sequenceDiagramFilter.protocolData, sequenceDiagramFilter.protocolInfoData, sequenceDiagramFilter.protocolIpData, sequenceDiagramFilter.protocolAddressNameData, sequenceDiagramFilter.protocolNameData, sequenceDiagramFilter.protocolTransportData, sequenceDiagramFilter.protocolInstanceData);
    }
    if(this.messageEvents !== sequenceDiagramFilter.messageEvents) {
      this.setMessageEvents(sequenceDiagramFilter.messageEvents);
    }
    if(this.messageDetailEvents !== sequenceDiagramFilter.messageDetailEvents) {
      this.setMessageDetailEvents(sequenceDiagramFilter.messageDetailEvents);
    }
    if(this.stackEvents !== sequenceDiagramFilter.stackEvents
      || this.stackInfoEvents !== sequenceDiagramFilter.stackInfoEvents
      || this.stackInstanceEvents !== sequenceDiagramFilter.stackInstanceEvents
      || this.stackProtocolEvents !== sequenceDiagramFilter.stackProtocolEvents
    ) {
      this.setStackData(sequenceDiagramFilter.stackEvents, sequenceDiagramFilter.stackInfoEvents, sequenceDiagramFilter.stackInstanceEvents, sequenceDiagramFilter.stackProtocolEvents);
    }
    if(this.guiEvents !== sequenceDiagramFilter.guiEvents
      || this.guiInfoData !== sequenceDiagramFilter.guiInfoData
      || this.guiInstanceData !== sequenceDiagramFilter.guiInstanceData
      || this.guiProtocolData !== sequenceDiagramFilter.guiProtocolData
    ) {
      this.setGuiData(sequenceDiagramFilter.guiEvents, sequenceDiagramFilter.guiInfoData, sequenceDiagramFilter.guiInstanceData, sequenceDiagramFilter.guiProtocolData);
    }
    if(this.guiObjectEvents !== sequenceDiagramFilter.guiObjectEvents) {
      this.setGuiObjectEvents(sequenceDiagramFilter.guiObjectEvents);
    }
    if(this.guiActionEvents !== sequenceDiagramFilter.guiActionEvents) {
      this.setGuiActionEvents(sequenceDiagramFilter.guiActionEvents);
    }
    if(this.guiFunctionEvents !== sequenceDiagramFilter.guiFunctionEvents) {
      this.setGuiFunctionEvents(sequenceDiagramFilter.guiFunctionEvents);
    }
    
    ////////////////////////////
    
    
    if(this.filterShowMessageContent !== sequenceDiagramFilter.showMessageContent) {
      this.showMessageContentEvents(sequenceDiagramFilter.showMessageContent);
    }
    if(this.filterShowMessageContentOpen !== sequenceDiagramFilter.showMessageContentOpen) {
      this.filterShowMessageContentOpen = sequenceDiagramFilter.showMessageContentOpen;
    }
    
    if(this.filterShowSentMessageNumbers !== sequenceDiagramFilter.showSentMessageNumbers) {
      this.showSentMessageNumbers(sequenceDiagramFilter.showSentMessageNumbers);
    }
    this.showReceivedMessageNumbers(sequenceDiagramFilter.showReceivedMessageNumbers);
    this.showAddresses(sequenceDiagramFilter.showAddresses);
  }
  
  setPhases(phases) {
    this.phases = phases;
  }
  
  getPhase(phaseId) {
    return this.phases[phaseId];
  }
    
  setExecutedStateEvents(show) {
    this.executedStateEvents = show;
  }
  
  getStateExecutedEvents(phaseId) {
    return this.executedStateEvents && this.phases[phaseId];
  }
  
  setNotExecutedStateEvents(show) {
    this.notExecutedStateEvents = show;
  }
  
  getNotExecutedStateEvents(phaseId) {
    return this.notExecutedStateEvents && this.phases[phaseId];
  }
  
  setConnectionEvents(show) {
    this.connectionEvents = show;
  }
  
  getConnectionEvents(phaseId) {
    return this.connectionEvents && this.phases[phaseId];
  }
  
  setServerEvents(show) {
    this.serverEvents = show;
  }
  
  getServerEvents(phaseId) {
    return this.serverEvents && this.phases[phaseId];
  }
  
  setProtocolData(protocolData, protocolInfoData, protocolIpData, protocolAddressNameData, protocolNameData, protocolTransportData, protocolInstanceData) {
    if(this.protocolData !== protocolData) {
      this.protocolData = protocolData;
      this._changeFilterDisplay(this.seq_dia__data_protocol, protocolData);
    }
    if(this.protocolInfoData !== protocolInfoData) {
      this.protocolInfoData = protocolInfoData;
      this._changeFilterDisplay(this.seq_dia__data_protocol_info, protocolInfoData);
    }
    if(this.protocolIpData !== protocolIpData) {
      this.protocolIpData = protocolIpData;
      this._changeFilterDisplay(this.seq_dia__data_protocol_ip, protocolIpData);
    }
    if(this.protocolAddressNameData !== protocolAddressNameData) {
      this.protocolAddressNameData = protocolAddressNameData;
      this._changeFilterDisplay(this.seq_dia__data_protocol_address_name, protocolAddressNameData);
    }
    if(this.protocolNameData !== protocolNameData) {
      this.protocolNameData = protocolNameData;
      this._changeFilterDisplay(this.seq_dia__data_protocol_name, protocolNameData);
    }
    if(this.protocolTransportData !== protocolTransportData) {
      this.protocolTransportData = protocolTransportData;
      this._changeFilterDisplay(this.seq_dia__data_protocol_transport, protocolTransportData);
    }
    if(this.protocolInstanceData !== protocolInstanceData) {
      this.protocolInstanceData = protocolInstanceData;
      this._changeFilterDisplay(this.seq_dia__data_protocol_instance, protocolInstanceData);
    }
  }

  setStackData(stackEvents, stackInfoEvents, stackInstanceEvents, stackProtocolEvents) {
    if(this.stackEvents !== stackEvents) {
      this.stackEvents = stackEvents;
      this._changeFilterDisplay(this.seq_dia__data_stack, stackEvents);
    }
    if(this.stackInfoEvents !== stackInfoEvents) {
      this.stackInfoEvents = stackInfoEvents;
      this._changeFilterDisplay(this.seq_dia__data_stack_info, stackInfoEvents);
    }
    if(this.stackInstanceEvents !== stackInstanceEvents) {
      this.stackInstanceEvents = stackInstanceEvents;
      this._changeFilterDisplay(this.seq_dia__data_stack_instance, stackInstanceEvents);
    }
    if(this.stackProtocolEvents !== stackProtocolEvents) {
      this.stackProtocolEvents = stackProtocolEvents;
      this._changeFilterDisplay(this.seq_dia__data_stack_protocol, stackProtocolEvents);
    }
  }
  
  getStackEvents(phaseId) {
    return this.stackEvents && this.phases[phaseId];
  }
    
  setGuiData(guiEvents, guiInfoData, guiInstanceData, guiProtocolData) {
    if(this.guiEvents !== guiEvents) {
      this.guiEvents = guiEvents;
      this._changeFilterDisplay(this.seq_dia__data_gui, guiEvents);
    }
    if(this.guiInfoData !== guiInfoData) {
      this.guiInfoData = guiInfoData;
      this._changeFilterDisplay(this.seq_dia__data_gui_info, guiInfoData);
    }
    if(this.guiInstanceData !== guiInstanceData) {
      this.guiInstanceData = guiInstanceData;
      this._changeFilterDisplay(this.seq_dia__data_gui_instance, guiInstanceData);
    }
    if(this.guiProtocolData !== guiProtocolData) {
      this.guiProtocolData = guiProtocolData;
      this._changeFilterDisplay(this.seq_dia__data_gui_protocol, guiProtocolData);
    }
  }
    
  setGuiObjectEvents(show) {
    this.guiObjectEvents = show;
  }
  
  setGuiActionEvents(show) {
    this.guiActionEvents = show;
  }
  
  setGuiFunctionEvents(show) {
    this.guiFunctionEvents = show;
  }
  
  getGuiEvents(phaseId, subType) {
    switch(subType) {
      case LogDataGuiSubType.SUB_TYPE_OBJECT:
        return this.guiEvents && this.guiObjectEvents && this.phases[phaseId];
      case LogDataGuiSubType.SUB_TYPE_ACTION:
        return this.guiEvents && this.guiActionEvents && this.phases[phaseId];
      case LogDataGuiSubType.SUB_TYPE_FUNCTION:
        return this.guiEvents && this.guiFunctionEvents && this.phases[phaseId];
    }
  }
  
  setMessageEvents(show) {
    
    
    this.messageEvents = show;
  }
  
  getMessageEvents(phaseId) {
    return this.messageEvents && this.phases[phaseId];
  }
  
  setMessageDetailEvents(show) {
    this.messageDetailEvents = show;
    this.showMessageContentEvents(this.filterShowMessageContent);
  }
  
  getMessageDetailEvents(phaseId) {
    return this.messageDetailEvents && this.phases[phaseId];
  }
  
  showMessageContentEvents(show) {
    this.filterShowMessageContent = show;
    const showDataMessage = show && this.messageEvents && this.phases[ActorPhaseConst.DATA];
    const showPreMessage = show && this.messageEvents && this.phases[ActorPhaseConst.PRE];
    const showExecMessage = show && this.messageEvents && this.phases[ActorPhaseConst.EXEC];
    const showPostMessage = show && this.messageEvents && this.phases[ActorPhaseConst.POST];
    const showNoneMessage = show && this.messageEvents && this.phases[ActorPhaseConst.NONE];
    const showDataMessageDetail = show && this.messageDetailEvents && this.phases[ActorPhaseConst.DATA];
    const showPreMessageDetail = show && this.messageDetailEvents && this.phases[ActorPhaseConst.PRE];
    const showExecMessageDetail = show && this.messageDetailEvents && this.phases[ActorPhaseConst.EXEC];
    const showPostMessageDetail = show && this.messageDetailEvents && this.phases[ActorPhaseConst.POST];
    const showNoneMessageDetail = show && this.messageDetailEvents && this.phases[ActorPhaseConst.NONE];
  }
  
  getMessageContentEvents(phaseId, details, open) {
    if(Const.MESSAGE_NORMAL === details) {
      return (this.filterShowMessageContent || open) && this.messageEvents && this.phases[phaseId];
    }
    else { // Const.MESSAGE_DETAIL
      return (this.filterShowMessageContent || open) && this.messageDetailEvents && this.phases[phaseId];
    }
  }
    
  showAddresses(ip) {
    this.filterShowAddresses = ip;
    this._changeFilterDisplay(this.seq_dia_show__display__address_name, !ip);
    this._changeFilterDisplay(this.seq_dia_show__display__ip_address, ip);
  }

  showSentMessageNumbers(show) {
    this.filterShowSentMessageNumbers = show;
  }
  
  showReceivedMessageNumbers(show) {
    this.filterShowReceivedMessageNumbers = show;
  }
  
  _createFilter(name, type, visible) {
    const key = Symbol(name);
    this.filters.set(key, {name: name, type: type, visible: visible, index: -1});
    return key;
  }
  
  _changeFilterDisplay(key, visible) {
    const filter = this.filters.get(key);
    filter.visible = visible;
    this.styleSheet.deleteRule(filter.index);
    const rule = visible ? 'display:block' : 'display:none;'
    this.styleSheet.insertRule(`${filter.type}.${filter.name} { ${rule} }`, filter.index);
  }
}


SeqDiaFilter.seqDiaExecutedStateEvents = 30;
SeqDiaFilter.seqDiaNotExecutedStateEvents = 31;
SeqDiaFilter.seqDiaServerEvents = 32;

SeqDiaFilter.seqDiaPhaseEvent = 0;
SeqDiaFilter.seqDiaEventExecuted = 1;
SeqDiaFilter.seqDiaEventNotExecuted = 2;
SeqDiaFilter.seqDiaConnectionEvent = 3;
SeqDiaFilter.seqDiaMessage = 4;
SeqDiaFilter.seqDiaMessageDetail = 5;
SeqDiaFilter.seqDiaMessageContentMessage = 6;
SeqDiaFilter.seqDiaMessageContentMessageDetail = 7;
SeqDiaFilter.seqDiaGuiEvent = 8;
SeqDiaFilter.seqDiaGuiEventObject = 9;
SeqDiaFilter.seqDiaGuiEventEvent = 10;
SeqDiaFilter.seqDiaGuiEventFunction = 11;
SeqDiaFilter.seqDiaStackEvent = 12;


module.exports = SeqDiaFilter;
