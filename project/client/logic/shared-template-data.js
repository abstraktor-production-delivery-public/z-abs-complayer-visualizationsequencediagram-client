
'use strict';

import Analyze from './analyze';
import DataLogRow from './data-log-row';
import ActorPhaseConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const';


class SharedTemplateData {
  constructor() {
    this.filter = null;
    this.stackStyles = null;
    this.analyze = null;
    this.dataLogRow = null;
    this.nextPhaseId = -1;
  }
  
  init(actors, filter, stackStyles) {
    this.filter = filter;
    this.stackStyles = stackStyles;
    
    this.analyze = new Analyze();
    
    this.analyze.analyzeActors(actors);
    this.dataLogRow = DataLogRow.create(null, 0, 0);
    this.nextPhaseId = ActorPhaseConst.DATA;
  }
  
  stateDone(msg) {
    let phaseId = this.dataLogRow.phaseId;
    const phaseIdNbr = --this.analyze.phaseActors[phaseId];
    if(0 === phaseIdNbr) {
      while(0 === this.analyze.phaseActors[phaseId]) {
        ++phaseId;
      }
    }
    return phaseId;
  }
  
  setPhaseId(phaseId) {
    this.nextPhaseId = phaseId;
  }
  
  setLogRow(msg) {
    let isNewPhase = false;
    let phaseId = this.dataLogRow.phaseId;
    if(-1 !== this.nextPhaseId) {
      phaseId = this.nextPhaseId;
      this.nextPhaseId = -1;
      isNewPhase = true;
    }
    if(msg && msg.data && msg.data.stack) {
      const lastIndex = msg.data.stack.lastIndexOf('-');
      const prefix = msg.data.stack.substring(0, -1 === lastIndex ? undefined : lastIndex);
      const stackStyle = this.stackStyles.get(prefix);
      this.dataLogRow = DataLogRow.create(msg, stackStyle ? stackStyle.index : 0, phaseId);
    }
    else {
      this.dataLogRow = DataLogRow.clone(this.dataLogRow, phaseId);
    }
    return isNewPhase;
  }
}


module.exports = SharedTemplateData;
