
'use strict';

import Const from './const';


class FilterSize {
  static MAX_COLUMNS = 20;
  
  constructor() {
    this.filterRules = new Map();
    
    this.eventMargin = 10;
    this.eventPadding = 80;
    this.eventWidth = 200;
    this.eventWidthHalf = this.eventWidth / 2;
    this.eventWidthQuarter = this.eventWidth / 4;
    this.eventWidthEighth = this.eventWidth / 8;
    this.eventWidthAndHalf = this.eventWidth + this.eventWidthHalf;
    this.event2Width = this.eventWidth + this.eventWidth;
    this.event2WidthAndHalf = this.eventWidth + this.event2Width;

    this.filterCss = document.createElement('style');
    document.head.appendChild(this.filterCss);
    this.styleSheet = this.filterCss.sheet;
  }
  
  update(eventMargin, eventPadding, eventWidth) {
    if(this.eventMargin !== eventMargin || this.eventPadding !== eventPadding || this.eventWidth !== eventWidth) {
      this.eventMargin = eventMargin;
      this.eventPadding = eventPadding;
      this.eventWidth = eventWidth;
      this.eventWidthHalf = this.eventWidth / 2;
      this.eventWidthQuarter = this.eventWidth / 4;
      this.eventWidthEighth = this.eventWidth / 8;
      this.event2Width = this.eventWidth + this.eventWidth;
      this.event2WidthAndHalf = this.eventWidthHalf + this.event2Width;
      
      this._createFilter('seq_dia__event_column_left', 'div', `left:${eventMargin}px;width:${eventPadding}px`, -1);
      this._createFilter('seq__dia_event_actor_line', 'div', `margin:0px ${this.eventWidthHalf - 1}px;`, -1);
      let left = eventMargin + eventPadding;
      for(let i = 0; i < FilterSize.MAX_COLUMNS; ++i) {
        this._createFilter(`seq_dia__event_column_middle_${i}`, 'div', `left:${left}px;width:${eventWidth}px`, -1);  
        this._createFilter(`seq_dia__event_column_right_${i}`, 'div', `left:${left}px;width:${eventPadding}px`, -1);
        left += eventWidth;
      }
      this.seq_dia__event_lrBorder = this._createFilter('seq_dia__event_lrBorder', 'div', `display:grid!important;width:${eventMargin + eventPadding}px`, -1);
      this.seq_dia__event_Width = this._createFilter('seq_dia__event_Width', 'div', `display:grid!important;width:${eventWidth}px`, -1);
      
      const newFilterCss = document.createElement('style');
      document.head.replaceChild(newFilterCss, this.filterCss);
      this.filterCss = newFilterCss;
      this.styleSheet = this.filterCss.sheet;
      
      const cssRules = this.styleSheet.cssRules;
      this.filterRules.forEach((filterRule) => {
        const index = this.styleSheet.insertRule(`${filterRule.type}.${filterRule.name} { ${filterRule.rule} }`, cssRules.length);
        filterRule.index = index;
      });
      return true;
    }
    else {
      return false;
    }
  }
  
  getClassEventBorder() {
    return 'seq_dia__event_lrBorder';
  }
  
  getClassEventWidth() {
    return 'seq_dia__event_Width';
  }
  
  _createFilter(name, type, rule, index) {
    this.filterRules.set(name, {name: name, type: type, rule: rule, index: index});
    return name;
  }
}

module.exports = new FilterSize();
