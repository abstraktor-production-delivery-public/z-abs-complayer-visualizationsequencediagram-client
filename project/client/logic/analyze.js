
'use strict';

import Const from './const';
import ActorPhaseConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const';
import ActorTypeConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const';


class Analyze {
  constructor() {
    this.actorNames = [];
    this.actorPhases = [];
    this.actorParts = [];
    this.phaseActors = [0, 0, 0, 0, -1];
  }
  
  analyzeActors(actors) {
    for(let i = 0; i < actors.length; ++i) {
      this.actorNames.push(actors[i].name);
      this.actorPhases.push(actors[i].phaseId);
      this.actorParts.push(Const.PART_MIDDLE);
      if(ActorTypeConst.REAL_SUT !== actors[i].typeId) {
        if((ActorTypeConst.COND !== actors[i].typeId) || (ActorTypeConst.COND === actors[i].typeId && ActorPhaseConst.PRE === actors[i].phaseId)) {
          ++this.phaseActors[0];
        }
        this.phaseActors[actors[i].phaseId] += 4;
        if(ActorTypeConst.COND === actors[i].typeId) {
          this.phaseActors[ActorPhaseConst.POST] += 4;
        }
      }
    }
  }
}


module.exports = Analyze;
