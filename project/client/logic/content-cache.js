
'use strict';


class ContentCache {
  constructor() {
    this.cache = new Map();
  }
  
  add(name, content) {
    this.cache.set(name, {
      raw: true,
      content: content
    });
  }
  
  clear() {
    this.cache.clear();
  }
}


module.exports = new ContentCache();
