
'use strict';

import Const from './const';


class ConnectionData {
  static NOT_CONNECTED = 0;
  static CONNECTED = 1;
  static DISCONNECTED = 2;
  static CONNECT_STATE = ['NOT_CONNECTED', 'CONNECTED', 'DISCONNECTED'];
  
  constructor(clientActorData, clientConnected, serverActorData, serverConnected, stack, clientStackId, serverStackId, transportLayer, lineEventId, isSut) {
    console.log('ConnectionData.constructor');
    this.clientActorData = clientActorData;
    this.clientConnected = clientConnected;
    this.clientStackId = clientStackId;
    this.sharedClients = 0;
    this.serverActorData = serverActorData;
    this.serverConnected = serverConnected;
    this.serverStackId = serverStackId;
    this.sharedServers = 0;
    this.stack = stack;
    this.transportLayer = transportLayer;
    this.lineEventId = lineEventId;
    this.messagesSent = 0;
    this.messagesReceived = 0;
    this.isSut = isSut;
  }
  
  addMessage(messageType, localType) {
    if(Const.MESSAGE_DIRECTION_SENT === messageType) {
      if('client' === localType) {
        this.messagesSent = ++this.clientActorData.messagesSent;
        this.messagesReceived = ++this.serverActorData.messagesReceived;
      }
      else {
        this.messagesSent = ++this.serverActorData.messagesSent;
        this.messagesReceived = ++this.clientActorData.messagesReceived;
      }
    }
    else if(Const.MESSAGE_DIRECTION_RECEIVED === messageType) {
      if('server' === localType) {
        this.messagesSent = ++this.clientActorData.messagesSent;
        this.messagesReceived = ++this.serverActorData.messagesReceived;
      }
      else {
        this.messagesSent = ++this.serverActorData.messagesSent;
        this.messagesReceived = ++this.clientActorData.messagesReceived;
      }
    }
  }
  
  static cloneShallow(connectionData) {
    return new ConnectionData(connectionData.clientActorData, connectionData.clientConnected, connectionData.serverActorData, connectionData.serverConnected, connectionData.stack, connectionData.clientStackId, connectionData.serverStackId, connectionData.transportLayer, connectionData.lineEventId, connectionData.isSut);
  }
}


module.exports = ConnectionData;
