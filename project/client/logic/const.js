
'use strict';


class Const {
  static RENDER_SINGLE_NODE = 0;
  static RENDER_TWO_NODES = 1;
    
  static PART_LEFT = 0;
  static PART_RIGHT = 1;
  static PART_MIDDLE = 2;
  static PART_MIDDLE_NODE_CONTINUED = 3;
  static PART_MIDDLE_NODE_LAST = 4;
  
  static TRANSPORT_TYPE_TCP = 0;
  static TRANSPORT_TYPE_UDP = 1;
  static TRANSPORT_TYPE_MC = 2;
  static TRANSPORT_TYPE_TLS = 3;
  
  static transportTypeNames = [
    'tcp',
    'udp',
    'mc',
    'tls'
  ];
  
  static TRANSPORT_LINE_CSSES = [
    'seq_dia_protocol_tcp_line',
    'seq_dia_protocol_udp_line',
    'seq_dia_protocol_mc_line',
    'seq_dia_protocol_tls_line'
  ];
  
  static PROTOCOL_LINE_NONE = -1;
  static PROTOCOL_LINE_CONNECTION = 0;
  static PROTOCOL_LINE_MESSAGE = 1;
  static PROTOCOL_LINE_MESSAGE_PART = 2;
  static PROTOCOL_LINE_CSSES = [
    'seq_dia_protocol_connection_line',
    'seq_dia_protocol_message_line',
    'seq_dia_protocol_message_part_line'
  ];
  
  static PART_TYPE_CLIENT = 0;
  static PART_TYPE_SERVER = 1;
  
  static RENDER_POSITION_NORMAL = 0;
  static RENDER_POSITION_BOTTOM = 1;
}

Const.LOCAL_PART = 0;
Const.REMOTE_PART = 1;
Const.EAST_START = 0;
Const.EAST_STOP = 1;
Const.WEST_START = 2;
Const.WEST_STOP = 3;
Const.UNKNOWN_START = 4;
Const.EAST_MIDDLE = 5;
Const.WEST_MIDDLE = 6;

Const.MESSAGE_DIRECTION_RECEIVED = 0;
Const.MESSAGE_DIRECTION_SENT = 1;
Const.MESSAGE_DIRECTION = [
  'seq_dia_msg_nbr_received',
  'seq_dia_msg_nbr_sent'
];
Const.MESSAGE_DIRECTION_FILTER = [
  'seq_dia_show__message_event__received_numbers',
  'seq_dia_show__message_event__sent_numbers'
];

Const.MESSAGE_NORMAL = 0;
Const.MESSAGE_DETAIL = 1;

Const.EVENT_CONNECTING = 0;
Const.EVENT_TCP = 1;
Const.EVENT_UDP = 2;
Const.EVENT_MC = 3;
Const.EVENT_TLS = 4;

Const.events = new Map([['connecting', {
  index: Const.EVENT_CONNECTING,
  classLine: 'seq_dia_protocol_connection_line'
}], ['tcp', {
  index: Const.EVENT_TCP,
  classLine: 'event_tcp_line'
}], ['udp', {
  index: Const.EVENT_UDP,
  classLine: 'event_udp_line'
}], ['mc', {
  index: Const.EVENT_MC,
  classLine: 'event_mc_line'
}], ['tls', {
  index: Const.EVENT_TLS,
  classLine: 'event_tls_line'
}]]);

Const.transportTypes = new Map(
[[0, {
  lineEventId: Const.EVENT_TCP
}], [1, {
  lineEventId: Const.EVENT_UDP
}], [2, {
  lineEventId: Const.EVENT_MC
}], [3, {
  lineEventId: Const.EVENT_TLS
}]]);



Const.HIDE = 0;
Const.SHOW = 1;

Const.EAST = 0;
Const.WEST = 1;
Const.CLOSED = 0;
Const.OPEN = 1;

Const.TEST_CASE_Y_BIAS = 10;

Const.ACTOR_X_MARGIN = 30;
Const.ACTOR_X_PADDING = 100;
Const.ACTOR_STATIC_ACTORS = 10;

Const.ACTOR_Y_BIAS = 10;


Const.ACTOR_EVENT_HEIGHT = 10;
Const.ACTOR_EVENT_HALF_HEIGHT = Const.ACTOR_EVENT_HEIGHT / 2;
Const.ACTOR_EVENT_Y_BIAS = Const.TEST_CASE_Y_BIAS + Const.ACTOR_Y_BIAS + Const.ACTOR_EVENT_HEIGHT;

Const.STATE_EVENT_HEIGHT = 10;
Const.STATE_EVENT_HALF_HEIGHT = Const.STATE_EVENT_HEIGHT / 2;

Const.MESSAGE_EVENT_HEIGHT = 10;
Const.MESSAGE_EVENT_HEIGHT_HALF = Const.MESSAGE_EVENT_HEIGHT / 2;
Const.MESSAGE_EVENT_HEIGHT_DOUBLE = 2 * Const.MESSAGE_EVENT_HEIGHT;
Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF = Const.MESSAGE_EVENT_HEIGHT + Const.MESSAGE_EVENT_HEIGHT_HALF;


Const.PHASE_X_BIAS = 10;
Const.PHASE_HEIGHT = 5;
Const.PHASE_HEIGHT_DOUBLE = 2 * Const.PHASE_HEIGHT;

Const.PHASE_Y_BIAS = 0.5;
Const.PHASE_WIDTH = 40;
Const.PHASE_WIDTH_DOUBLE = 2 * Const.PHASE_WIDTH;

Const.PHASE_BIAS_WIDTH = Const.PHASE_X_BIAS + Const.PHASE_WIDTH;
Const.PHASE_BIAS_WIDTH_DOUBLE = 2 * Const.PHASE_BIAS_WIDTH;

Const.HEADER_Y_HEIGHT = 30;

Const.LINE_WIDTH = 2.0;
Const.LINE_WIDTH_HALF = Const.LINE_WIDTH / 2;

Const.TEXT_BIAS = 17;

Const.phaseClasses = [
  'seq_dia_phase_data',
  'seq_dia_phase_pre',
  'seq_dia_phase_exec',
  'seq_dia_phase_post',
  'seq_dia_phase_none'
];

Const.phaseDarkClasses = [
  'seq_dia_dark_phase_data',
  'seq_dia_dark_phase_pre',
  'seq_dia_dark_phase_exec',
  'seq_dia_dark_phase_post',
  'seq_dia_dark_phase_none'
];

Const.phaseNames = [
  'Data',
  'Precondition',
  'Execution',
  'Postcondition',
  'None'
];

/// NEW ///
Const.phasePrefixClasses = [
  'data__',
  'pre__',
  'exec__',
  'post__',
  'none__',
  'all__'
];


module.exports = Const;
