
'use strict';


class DataLogRow {
  static create(logRow, protocolIndex, phaseId) {
    return {
      logRow: logRow,
      phaseId: phaseId,
      protocolIndex: protocolIndex
    }
  }
  
  static clone(dataLogRow, phaseId) {
    return {
      logRow: dataLogRow.logRow,
      phaseId: phaseId,
      protocolIndex: dataLogRow.protocolIndex
    }
  }
}


module.exports = DataLogRow;
