
'use strict';

import ConnectionData from '../logic/connection-data';
import Const from '../logic/const';
import ContentCache from '../logic/content-cache';
import FilterSize from '../logic/filter-size';
import SharedTemplateData from '../logic/shared-template-data';
import ComponentSeqDiaCaption from '../templates/components/component-seq-dia-caption';
import ComponentSeqDiaEnvelope from '../templates/components/component-seq-dia-envelope'; 
import ComponentSeqDiaEventInfo from '../templates/components/component-seq-dia-event-info';
import ComponentSeqDiaEventRow from '../templates/components/component-seq-dia-event-row';
import ComponentSeqDiaProtocol from '../templates/components/component-seq-dia-protocol'; 
import ComponentSeqDiaShared from '../templates/components/component-seq-dia-shared';
import TemplateSeqDiaTestCaseStart from '../templates/template-seq-dia-test-case-start';
import TemplateSeqDiaTestCaseHeader from '../templates/template-seq-dia-test-case-header';
import TemplateSeqDiaPhaseStart from '../templates/template-seq-dia-phase-start';
import TemplateSeqDiaPhaseEnd from '../templates/template-seq-dia-phase-end';
import TemplateSeqDiaTestCaseEnd from '../templates/template-seq-dia-test-case-end';
import TemplateSeqDiaState from '../templates/template-seq-dia-state';
import TemplateSeqDiaStack from '../templates/template-seq-dia-stack';
import TemplateSeqDiaServerStarting from '../templates/template-seq-dia-server-starting';
import TemplateSeqDiaServerStarted from '../templates/template-seq-dia-server-started';
import TemplateSeqDiaServerAttached from '../templates/template-seq-dia-server-attached';
import TemplateSeqDiaServerStopping from '../templates/template-seq-dia-server-stopping';
import TemplateSeqDiaServerStopped from '../templates/template-seq-dia-server-stopped';
import TemplateSeqDiaServerDetached from '../templates/template-seq-dia-server-detached';
import TemplateSeqDiaServerNotStarted from '../templates/template-seq-dia-server-not-started';
import TemplateSeqDiaServerAccepting from '../templates/template-seq-dia-server-accepting';
import TemplateSeqDiaServerAccepted from '../templates/template-seq-dia-server-accepted';
import TemplateSeqDiaClientConnecting from '../templates/template-seq-dia-client-connecting';
import TemplateSeqDiaClientConnected from '../templates/template-seq-dia-client-connected';
import TemplateSeqDiaClientDnsFailure from '../templates/template-seq-dia-client-dns-failure';
import TemplateSeqDiaClientConnectedNot from '../templates/template-seq-dia-client-connected-not';
import TemplateSeqDiaConnectionUpgrading from '../templates/template-seq-dia-connection-upgrading';
import TemplateSeqDiaConnectionUpgraded from '../templates/template-seq-dia-connection-upgraded';
import TemplateSeqDiaConnectionUpgradedNot from '../templates/template-seq-dia-connection-upgraded-not';
import TemplateSeqDiaConnectionClosing from '../templates/template-seq-dia-connection-closing';
import TemplateSeqDiaConnectionClosed from '../templates/template-seq-dia-connection-closed';
import TemplateSeqDiaMsg from '../templates/template-seq-dia-msg';
import TemplateSeqDiaSend from '../templates/template-seq-dia-send';
import TemplateSeqDiaReceive from '../templates/template-seq-dia-receive';
import TemplateSeqDiaMessage from '../templates/template-seq-dia-message';
import TemplateSeqDiaGui from '../templates/template-seq-dia-gui';
import ScrollListNode from 'z-abs-complayer-bootstrap-client/client/scroll-list-node';
import ScrollList from 'z-abs-complayer-bootstrap-client/client/scroll-list';
import ReactComponentRealtimeRenderer from 'z-abs-corelayer-client/client/react-component/react-component-realtime-renderer';
import AppProtocolConst from 'z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import ActorStateConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-state-const';
import ActorTypeConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const';
import LogDataAction from 'z-abs-funclayer-engine-cs/clientServer/log/log-data-action';
import React from 'react';


export default class SequenceDiagram extends ReactComponentRealtimeRenderer {
  constructor(props) {
    super(props, [props.store], 2);
    this.isActive = false;
    this.logClient = false;
    this.logConsole = false;
    this.refDivSeqDia = React.createRef();
    this.sharedTemplateData = new SharedTemplateData();
    //this.seqDiaMsgTemplate = new TemplateSeqDiaMsg();
    this.seqDiaTestCaseStartTemplate = new TemplateSeqDiaTestCaseStart();
    this.seqDiaTestCaseEndTemplate = new TemplateSeqDiaTestCaseEnd();
    this.seqDiaTestCaseHeaderTemplate = new TemplateSeqDiaTestCaseHeader();
    this.seqDiaPhaseStartTemplate = new TemplateSeqDiaPhaseStart();
    this.seqDiaPhaseEndTemplate = new TemplateSeqDiaPhaseEnd();
    this.seqDiaStateTemplate = new TemplateSeqDiaState();
    this.seqDiaClientDnsFailureTemplate = new TemplateSeqDiaClientDnsFailure();
    this.seqDiaConnectionUpgradingTemplate = new TemplateSeqDiaConnectionUpgrading();
    this.seqDiaConnectionUpgradedTemplate = new TemplateSeqDiaConnectionUpgraded();
    this.seqDiaConnectionUpgradedNotTemplate = new TemplateSeqDiaConnectionUpgradedNot();
    this.scrollList = new ScrollList(props.name, (autoScroll) => {
      if(props.onAutoScroll) {
        props.onAutoScroll(autoScroll);
      }
    }, (data, cb, ...params) => {
      this.renderRealtimeData(data, cb, ...params);
    }, (left) => {});
    this.templates = [
      new TemplateSeqDiaServerStarting(),
      new TemplateSeqDiaServerStarted(),
      new TemplateSeqDiaServerAttached(),
      new TemplateSeqDiaServerStopping(),
      new TemplateSeqDiaServerStopped(),
      new TemplateSeqDiaServerDetached(),
      new TemplateSeqDiaServerNotStarted(),
      null, // notAttached
      new TemplateSeqDiaSend(),
      new TemplateSeqDiaReceive(),
      new TemplateSeqDiaClientConnecting(),
      new TemplateSeqDiaClientConnected(),
      null, // this.seqDiaClientDnsFailureTemplate
      new TemplateSeqDiaClientConnectedNot(),
      null, // this.seqDiaConnectionUpgradingTemplate
      null, // this.seqDiaConnectionUpgradedTemplate
      null, // this.seqDiaConnectionUpgradedNotTemplate
      new TemplateSeqDiaServerAccepting(),
      new TemplateSeqDiaServerAccepted(),
      new TemplateSeqDiaConnectionClosing(),
      new TemplateSeqDiaConnectionClosed(),
      new TemplateSeqDiaGui(),
      new TemplateSeqDiaStack()
    ];
  }
  
  didMount() {
    const storeState = Reflect.get(this.state, this.props.storeName);
    FilterSize.update(Const.ACTOR_X_MARGIN, Const.ACTOR_X_PADDING, storeState.buttons.sequenceDiagram.width);
    this.props.filter.update(storeState.buttons.sequenceDiagram);
    this.initComponents(this.props, false);
    this.scrollList.init(this.refDivSeqDia.current, storeState.buttons.sequenceDiagram.scroll, storeState.buttons.sequenceDiagram.zoom);
  }
  
  willUnmount() {
    this.scrollList.exit();
  }
  
  cbIsRealtime() {
    return true;
  };
  
  realtimeUpdate(nextProps, nextState) {
    const storeState = Reflect.get(this.state, this.props.storeName);
    const nextStoreState = Reflect.get(nextState, nextProps.storeName);
    const changedWidth = FilterSize.update(Const.ACTOR_X_MARGIN, Const.ACTOR_X_PADDING, nextStoreState.buttons.sequenceDiagram.width);
    if((this.props.buttonsLoaded !== nextProps.buttonsLoaded) || changedWidth || !this.shallowCompare(this.props.stackStyles, nextProps.stackStyles)) {
      this.initComponents(nextProps, true);
    }
    if(nextStoreState.buttons.sequenceDiagram !== storeState.buttons.sequenceDiagram) {
      nextProps.filter.update(nextStoreState.buttons.sequenceDiagram);
      this.scrollList.update(nextStoreState.buttons.sequenceDiagram.scroll, nextStoreState.buttons.sequenceDiagram.zoom, storeState.buttons.sequenceDiagram !== nextStoreState.buttons.sequenceDiagram);
    }
  }
  
  active(active) {
    const prevActive = this.isActive;
    this.isActive = active;
    this.scrollList.active(active);
    if(!prevActive && active) {
      this.renderRealtimeData();
    }
  }
  
  onRenderRealtimeFrame(timestamp, clear, force) {
    this.scrollList.renderRealtimeFrame(timestamp, clear, force);
  }
  
  initComponents(props, update) {
    if(props.buttonsLoaded && 0 !== props.stackStyles.size) {
      let pendings = 0;
      const f = !update ? requestIdleCallback : setTimeout;
      const func = (cb) => {
        ++pendings;
        f(() => {
          //const t0 = performance.now();
          cb();
          //const t1 = performance.now();
          //ddb.debug(`requestIdleCallback took ${t1 - t0} milliseconds.`);
          if(0 === --pendings) {
            props.onOnit();
          }
        });
      };
      func(() => {
        ComponentSeqDiaShared.init(props.stackStyles);
      });
      func(() => {
        ComponentSeqDiaCaption.init();
      });
      func(() => {
        ComponentSeqDiaEventInfo.init(props.stackStyles);
      });
      func(() => {
        ComponentSeqDiaEventRow.init();
      });
      func(() => {
        ComponentSeqDiaProtocol.init(props.stackStyles);
      });
      func(() => {
        ComponentSeqDiaEnvelope.init();
      });
      func(() => {
        this.seqDiaTestCaseStartTemplate.init(props.filter);
      });
      func(() => {
        this.seqDiaTestCaseEndTemplate.init(props.filter);
      });
      func(() => {
        this.seqDiaTestCaseHeaderTemplate.init(props.filter);
      });
      func(() => {
        this.seqDiaPhaseStartTemplate.init(props.filter);
      });
      func(() => {
        this.seqDiaPhaseEndTemplate.init(props.filter);
      });
      func(() => {
        this.seqDiaStateTemplate.init(props.filter);
      });
      this.templates.forEach((template) => {
        if(template) {
          func(() => {
            template.init(props.filter, props.stackStyles);
          });
        }
      });
      func(() => {
        this.scrollList.refresh();
      });
    }
  }
  
  onRealtimeMessage(msg) {
    if(AppProtocolConst.LOG === msg.msgId) {
      const logData = msg.data;
      if(logData) {
        if(this.sharedTemplateData.setLogRow(msg)) {
          const buffer = this.seqDiaPhaseStartTemplate.store(msg, this.sharedTemplateData);
          const node = new ScrollListNode(this.seqDiaPhaseStartTemplate, buffer);
          this.scrollList.add(node);
        }
        if(ActorTypeConst.NAME_REAL_SUT === this.sharedTemplateData.dataLogRow.actorType) {
          if(LogDataAction.STARTING <= logData.actionId && LogDataAction.STOPPED >= logData.actionId) {
            return;
          }
        }
        try {
          const template = this.templates[logData.actionId];
          if(template) {
            const buffer = template.store(msg, this.sharedTemplateData);
            const node = new ScrollListNode(template, buffer);
            this.scrollList.add(node);
            this._renderActiveRealtime();
          }
        }
        catch(err) {// TODO: REMOVE
          ddb.error(LogDataAction.LOG[logData.actionId], ' error: ', err);
        }
      }
    }
    if(AppProtocolConst.TEST_CASE_STATE === msg.msgId) {
      if(this.logClient) {
        if(ActorStateConst.DATA === msg.stateIndex && ActorResultConst.NA === msg.stateResultIndex) {
          return;
        }
        if(this.sharedTemplateData.setLogRow(null)) {
          const buffer = this.seqDiaPhaseStartTemplate.store(msg, this.sharedTemplateData);
          const node = new ScrollListNode(this.seqDiaPhaseStartTemplate, buffer);
          this.scrollList.add(node);
        }
        const buffer = this.seqDiaStateTemplate.store(msg, this.sharedTemplateData);
        const node = new ScrollListNode(this.seqDiaStateTemplate, buffer);
        this.scrollList.add(node);
        const currentPhaseId = this.sharedTemplateData.dataLogRow.phaseId;
        const nextPhaseId = this.sharedTemplateData.stateDone(msg);
        if(currentPhaseId !== nextPhaseId) {
          this.sharedTemplateData.setLogRow(null);
          const buffer = this.seqDiaPhaseEndTemplate.store(msg, this.sharedTemplateData);
          const node = new ScrollListNode(this.seqDiaPhaseEndTemplate, buffer);
          this.scrollList.add(node);
          this.sharedTemplateData.setPhaseId(nextPhaseId);
        }
        this._renderActiveRealtime();
      }
    }
    else if(AppProtocolConst.TEST_CASE_STARTED === msg.msgId) {
      if(this.logClient) {
        this.sharedTemplateData.init(msg.actors, this.props.filter, this.props.stackStyles);
        const buffer = this.seqDiaTestCaseStartTemplate.store(msg, this.sharedTemplateData);
        const node = new ScrollListNode(this.seqDiaTestCaseStartTemplate, buffer);
        this.scrollList.add(node);
        if(msg.isExecuted) {
          const buffer = this.seqDiaTestCaseHeaderTemplate.store(msg, this.sharedTemplateData);
          const node = new ScrollListNode(this.seqDiaTestCaseHeaderTemplate, buffer);
          this.scrollList.add(node);
          if(2 <= msg.actors.length) {
            const buffer = this.seqDiaPhaseStartTemplate.store(msg, this.sharedTemplateData);
            const node = new ScrollListNode(this.seqDiaPhaseStartTemplate, buffer);
            this.scrollList.add(node);
          }
        }
        this._renderActiveRealtime();
        this.sharedTemplateData.setLogRow(null);
      }
    }
    else if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId) {
      if(this.logClient) {
        this.sharedTemplateData.setLogRow(null);
        const buffer = this.seqDiaTestCaseEndTemplate.store(msg, this.sharedTemplateData);
        const node = new ScrollListNode(this.seqDiaTestCaseEndTemplate, buffer);
        this.scrollList.add(node);
        this._renderActiveRealtime();
      }
    }
    else if(AppProtocolConst.EXECUTION_STARTED === msg.msgId) {
      this.logClient = !!(1 & msg.chosen);
      this.logConsole = !!(2 & msg.chosen);
    }
    else if(AppProtocolConst.TEST_CASE_CLEAR === msg.msgId && 'TestCase' === this.props.clearMsgPrefix) {
      if('sequence-diagram' === msg.tab) {
        this.renderRealtimeData([true, false], () => {
          ContentCache.clear();
        });
      }
    }
    else if(AppProtocolConst.TEST_SUITE_CLEAR === msg.msgId && 'TestSuite' === this.props.clearMsgPrefix) {
      if('sequence-diagram' === msg.tab) {
        this.renderRealtimeData([true, false], () => {
          ContentCache.clear();
        });
      }
    }
  }
  
  _renderActiveRealtime() {
    if(this.isActive) {
      this.renderRealtimeData();
    }
  }
  
  render() {
    return (
      <div ref={this.refDivSeqDia} className="comp_layer_seq_dia_rows">
      </div>
    );
  }
}
