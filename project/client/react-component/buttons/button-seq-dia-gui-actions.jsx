
'use strict';

import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonSeqDiaGuiActions extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue);
  }
  
  render() {
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} glyphicons={['glyphicon-picture', 'glyphicon-hand-right']} colorMark={['gui_events_heading', 'gui_events_detail']} styles={[{top:0,left:-4,transform:'scale(0.7)'},{top:6.5,left:-5,width:0,fontWeight:'bolder',transform:'scale(0.7)'}]} toolTipContent="GUI Actions" toolTipHeading={['Hide', 'Show']}
        onAction={this.props.onAction}
      />
    );
  }
}
