
'use strict';

import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonSeqDiaProtocolIp extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue);
  }
  
  render() {
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} glyphicons={['glyphicon-transfer', 'TEXT:IP']} colorMark={['button_protocol_data_heading', 'button_protocol_data_detail']} styles={[{top:0,left:-4,transform:'scale(0.7)'},{top:6,left:-5,width:0,fontWeight:'bolder',letterSpacing:-1,transform:'scale(0.7)'}]} toolTipContent="Protocol Ip" toolTipHeading={['Hide', 'Show']}
        onAction={this.props.onAction}
      />
    );
  }
}
