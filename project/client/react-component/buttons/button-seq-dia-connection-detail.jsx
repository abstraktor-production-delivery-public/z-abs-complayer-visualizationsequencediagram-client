
'use strict';

import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonSeqDiaConnectionDetail extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue);
  }
  
  render() {
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} glyphicons={['TEXT:IP', ['glyphicon-plus-sign', 'glyphicon-minus-sign']]} colorMark="connection_events" styles={[{top:-3,left:-4,fontWeight:'bolder',transform:'scale(0.8)'}, {top:6,left:-4,width:0,transform:'scale(0.6)'}]} toolTipContent={['Address Names', 'IP Addresses']} toolTipHeading="Show"
        onAction={this.props.onAction}
      />
    );
  }
}
