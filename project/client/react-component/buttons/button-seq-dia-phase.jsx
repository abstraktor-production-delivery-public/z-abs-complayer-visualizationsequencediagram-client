
'use strict';

import Const from '../../logic/const';
import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonSeqDiaPhase extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue)
      || !this.shallowCompare(this.props.buttonIndex, nextProps.buttonIndex);
  }
  
  render() {
    const buttonIndex = Number.parseInt(this.props.buttonIndex);
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} buttonIndex={this.props.buttonIndex} glyphicons={['glyphicon-stats']} colorMark={Const.phaseDarkClasses[buttonIndex]} styles={[{transform:'rotate(90deg)'}]} toolTipContent={`${this.props.name} Phase Events`} toolTipHeading={['Hide', 'Show']}
        onAction={this.props.onAction}
      />
    );
  }
}
