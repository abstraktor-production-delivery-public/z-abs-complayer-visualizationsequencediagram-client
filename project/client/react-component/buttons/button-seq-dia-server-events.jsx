
'use strict';

import RealtimePressDownButton from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-press-down-button';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ButtonSeqDiaServerEvents extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.buttonValue, nextProps.buttonValue);
  }
  
  render() {
    return (
      <RealtimePressDownButton size="btn-xs" buttonValue={this.props.buttonValue} glyphicons={['glyphicon-cloud']} colorMark="message_events" styles={[{top:2.8}]} toolTipContent="Server Events" toolTipHeading={['Hide', 'Show']}
        onAction={this.props.onAction}
      />
    );
  }
}
