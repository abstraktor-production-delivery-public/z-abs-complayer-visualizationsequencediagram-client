
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaEventRow from './components/component-seq-dia-event-row';
import DataSeqDiaState from '../data/data-seq-dia-state';
import FilterSize from '../logic/filter-size';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import ActorStateConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-state-const';


class TemplateDiaState {
  constructor() {
    this.filter = null;
    this.middleTemplates = null;
  }
  
  init(filter) {
    this.filter = filter;
    
    const stateLineTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    stateLineTemplate.setAttribute('x1', FilterSize.eventWidthHalf + 1.5);
    stateLineTemplate.setAttribute('x2', FilterSize.eventWidthHalf + 8);
    stateLineTemplate.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    stateLineTemplate.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    
    const circleSmallTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circleSmallTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    circleSmallTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    circleSmallTemplate.setAttribute('r', 3);
    
    const stateTextTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    stateTextTemplate.classList.add('seq_dia_state_event_state_text');
    stateTextTemplate.setAttribute('x', FilterSize.eventWidthHalf - 9);
    stateTextTemplate.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 3.5);
    stateTextTemplate.appendChild(document.createTextNode('[state]'));
    
    const resultRectNodeTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    resultRectNodeTemplate.setAttribute('x', FilterSize.eventWidthHalf + 8);
    resultRectNodeTemplate.setAttribute('width', 40);
    resultRectNodeTemplate.setAttribute('rx', 2);
    resultRectNodeTemplate.setAttribute('y', 0.5);
    resultRectNodeTemplate.setAttribute('height', 9);
    resultRectNodeTemplate.setAttribute('ry', 2);
    
    const resultTextNodeTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    resultTextNodeTemplate.classList.add('seq_dia_state_event_result_text');
    resultTextNodeTemplate.setAttribute('x', FilterSize.eventWidthHalf + 9);
    resultTextNodeTemplate.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 3.5);
    resultTextNodeTemplate.appendChild(document.createTextNode('[result]'));
    
    this.middleTemplates = new Array(ActorResultConst.results.length);
    ActorResultConst.results.forEach((result, index) => {
      const fragment = document.createDocumentFragment();
      const stateLine = fragment.appendChild(stateLineTemplate.cloneNode(true));
      stateLine.classList.add(ActorResultConst.resultClasses[index]);
      const circleSmall = fragment.appendChild(circleSmallTemplate.cloneNode(true));
      circleSmall.classList.add(ActorResultConst.resultClasses[index]);
      const stateText = fragment.appendChild(stateTextTemplate.cloneNode(true));
      const resultRectNode = fragment.appendChild(resultRectNodeTemplate.cloneNode(true));
      resultRectNode.classList.add(ActorResultConst.resultClasses[index]);
      resultRectNode.setAttribute('width', ActorResultConst.templateWidths[index]);
      const resultTextNode = fragment.appendChild(resultTextNodeTemplate.cloneNode(true));
      resultTextNode.classList.add(ActorResultConst.resultClasses[index]);
      resultTextNode.firstChild.nodeValue = result;
      this.middleTemplates[index] = fragment;
    });
  }
  
  calculateHeight(buffer) {
    const data = DataSeqDiaState.restoreHeightData(buffer);
    if(ActorResultConst.TYPE_NOT_EXECUTED === data.stateType) {
      return this.filter.getNotExecutedStateEvents(data.phaseId) ? Const.STATE_EVENT_HEIGHT : 0
    }
    else {
      return this.filter.getStateExecutedEvents(data.phaseId) ? Const.STATE_EVENT_HEIGHT : 0
    }
  }
  
  store(msg, sharedTemplateData) {
    const stateType = ActorResultConst.TYPES[msg.stateResultIndex];
    return DataSeqDiaState.store(msg, sharedTemplateData, stateType);
  }
  
  restore(buffer) {
    return DataSeqDiaState.restore(buffer);
  }
  
  create(data) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const phaseId = data.phaseId;
    const node = ComponentSeqDiaEventRow.cloneRow('seq_dia_state');
    const divNodeLeft = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnLeft(phaseId));
    const actorIndex = data.actorIndex;
    for(let i = 0; i < actorPhases.length; ++i) {
      const divNodeMiddle = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnMiddle(i, phaseId, actorParts[i]));
      if(actorIndex === i) {
        const svg = divNodeMiddle.appendChild(ComponentSeqDiaEventRow.cloneSvg(Const.STATE_EVENT_HEIGHT, Const.RENDER_POSITION_NORMAL));
        svg.appendChild(this.middleTemplates[data.stateResultIndex].cloneNode(true));
        svg.childNodes[2].firstChild.nodeValue = ActorStateConst.actorStateNames[data.stateIndex];
      }
    }
    const divNodeRight = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnRight(actorPhases.length, phaseId));
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateDiaState;
