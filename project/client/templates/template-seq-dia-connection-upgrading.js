
'use strict';

import Const from '../logic/const';
import DataSeqDiaConnectionUpgrading from '../data/data-seq-dia-connection-upgrading';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaConnectionUpgrading extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaConnectionUpgrading);
    this.fragment = null;
  }
  
  onInit(appDeserializer) {
    DataSeqDiaConnectionUpgrading.init(appDeserializer);
    const gTop = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTop.setAttribute('transform', `translate(${FilterSize.eventWidthHalf} 0)`);
    
    const connectedCircleSmallTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectedCircleSmallTemplate.classList.add('seq_dia_client_connected');
    connectedCircleSmallTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    connectedCircleSmallTemplate.setAttribute('r', 4);
    connectedCircleSmallTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const gTransform = gTop.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    gTransform.setAttribute('transform', 'translate(-3.2 1.6) scale(0.13 0.13)');
    
    const path = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path.setAttribute('d', 'M32,21.9h0Z');
    path.setAttribute('d', 'M40,18H16V13a7,7,0,0,1,7-7h2a7.1,7.1,0,0,1,5,2.1,2,2,0,0,0,2.2.5h.1a1.9,1.9,0,0,0,.6-3.1A10.9,10.9,0,0,0,25,2H23A11,11,0,0,0,12,13v5H8a2,2,0,0,0-2,2V44a2,2,0,0,0,2,2H40a2,2,0,0,0,2-2V20A2,2,0,0,0,40,18ZM25.9,33.4v2.5a2,2,0,0,1-4,0V33.4a4,4,0,1,1,4,0Z');
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(connectedCircleSmallTemplate.cloneNode(true));
    this.fragment.appendChild(gTop.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [fragment, fragment, fragment, fragment, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaConnectionUpgrading.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaConnectionUpgrading;
