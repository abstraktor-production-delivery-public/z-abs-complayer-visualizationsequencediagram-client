
'use strict';

import Const from '../logic/const';
import DataSeqDiaConnectionUpgrading from '../data/data-seq-dia-connection-upgrading';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaConnectionUpgradedNot extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaConnectionUpgrading);
    this.fragment = null;
  }
  
  onInit(appDeserializer) {
    DataSeqDiaConnectionUpgrading.init(appDeserializer);
    const gTop = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTop.setAttribute('transform', `translate(${FilterSize.eventWidthHalf} 0)`);
    
    const closingLine1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine1.classList.add('seq_dia_event_connection_closing');
    closingLine1.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine1.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine1.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine1.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingLine2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine2.classList.add('seq_dia_event_connection_closing');
    closingLine2.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine2.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine2.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine2.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const connectedCircleOuterTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectedCircleOuterTemplate.classList.add('seq_dia_client_connected');
    connectedCircleOuterTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    //connectedCircleOuterTemplate.setAttribute('r', 4);
    connectedCircleOuterTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const connectedCircleInnerTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectedCircleInnerTemplate.classList.add('seq_dia_client_connected');
    connectedCircleInnerTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    //connectedCircleInnerTemplate.setAttribute('r', 3);
    connectedCircleInnerTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const gTransform = gTop.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    gTransform.setAttribute('transform', 'translate(-2.4 2.4) scale(0.015 0.015)');
    
    const path = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path.setAttribute('d', 'M32,21.9h0Z');
    path.setAttribute('d', 'M65,330h200c8.284,0,15-6.716,15-15V145c0-8.284-6.716-15-15-15h-15V85c0-46.869-38.131-85-85-85 S80,38.131,80,85v45H65c-8.284,0-15,6.716-15,15v170C50,323.284,56.716,330,65,330z M180,234.986V255c0,8.284-6.716,15-15,15 s-15-6.716-15-15v-20.014c-6.068-4.565-10-11.824-10-19.986c0-13.785,11.215-25,25-25s25,11.215,25,25 C190,223.162,186.068,230.421,180,234.986z M110,85c0-30.327,24.673-55,55-55s55,24.673,55,55v45H110V85z');
        
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(closingLine1.cloneNode(true));
    this.fragment.appendChild(closingLine2.cloneNode(true));
    this.fragment.appendChild(connectedCircleOuterTemplate.cloneNode(true));
    this.fragment.appendChild(connectedCircleInnerTemplate.cloneNode(true));
    this.fragment.appendChild(gTop.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
   // fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_small`);
    fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_big_fail_outer`);
    fragment.childNodes[3].classList.add(`seq_dia_protocol_${protocolKey}_big_fail_inner`);
    return [fragment, fragment, fragment, fragment, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaConnectionUpgrading.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaConnectionUpgradedNot;
