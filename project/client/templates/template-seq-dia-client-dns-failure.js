
'use strict';

import Const from '../logic/const';
import DataSeqDiaClientDnsFailure from '../data/data-seq-dia-client-dns-failure';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaClientDnsFailure extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaClientDnsFailure);
    this.fragment = null;
  }
  
  onInit(appDeserializer) {
    const closingLine1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine1.classList.add('seq_dia_event_connection_closing');
    closingLine1.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine1.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine1.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine1.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingLine2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine2.classList.add('seq_dia_event_connection_closing');
    closingLine2.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine2.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine2.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine2.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingCircleOuter = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    closingCircleOuter.classList.add('seq_dia_client_connecting');
    closingCircleOuter.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    closingCircleOuter.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const closingCircleInner = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    closingCircleInner.classList.add('seq_dia_client_connecting');
    closingCircleInner.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    closingCircleInner.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const infoTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    
    const dsrAddressTspanTemplate = infoTemplate.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
    dsrAddressTspanTemplate.classList.add('seq_dia_event_address');
    //addressNameTspanTemplate.classList.add('seq_dia_show__display__address_name');
    dsrAddressTspanTemplate.setAttribute('x', FilterSize.eventWidthHalf + 7.5);
    dsrAddressTspanTemplate.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 2);
    dsrAddressTspanTemplate.appendChild(document.createTextNode('[src:>dst]'));
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(closingLine1.cloneNode(true));
    this.fragment.appendChild(closingLine2.cloneNode(true));
    this.fragment.appendChild(closingCircleOuter.cloneNode(true));
    this.fragment.appendChild(closingCircleInner.cloneNode(true));
    this.fragment.appendChild(infoTemplate.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_small_fail_outer`);
    fragment.childNodes[3].classList.add(`seq_dia_protocol_${protocolKey}_small_fail_inner`);
    return [fragment, fragment, fragment, fragment, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaClientDnsFailure.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
    
  onCreate(node, dataSeqDiaClientDnsFailure) {
    const actorIndex = dataSeqDiaClientDnsFailure.actorIndex;
    node.childNodes[actorIndex + 1].firstChild.childNodes[1].childNodes[4].firstChild.textContent = dataSeqDiaClientDnsFailure.dstUrl;
  }
}


module.exports = TemplateSeqDiaClientDnsFailure;
