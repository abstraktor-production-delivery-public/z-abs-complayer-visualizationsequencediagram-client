
'use strict';

import DataSeqDiaTestCaseStart from '../data/data-seq-dia-test-case-start';
import Const from '../logic/const';


class TemplateSeqDiaTestCaseStart {
  constructor() {
    this.filter = null;
    this.testCaseStart = null;
  }
  
  init(filter) {
    this.filter = filter;
    
    this.testCaseStart = document.createElement('div');
    const testCaseStart = this.testCaseStart;
    testCaseStart.classList.add('seq_dia_test_case_start');
    
    const start = testCaseStart.appendChild(document.createElement('strong'));
    start.appendChild(document.createTextNode('TEST CASE START'));
    
    const name = testCaseStart.appendChild(document.createElement('strong'));
    name.appendChild(document.createTextNode('name:'));
    name.classList.add('seq_dia_test_case_start');
    testCaseStart.appendChild(document.createTextNode('[TC NAME]'));
    
    const date = testCaseStart.appendChild(document.createElement('strong'));
    date.appendChild(document.createTextNode('date:'));
    date.classList.add('seq_dia_test_case_start');
    
    testCaseStart.appendChild(document.createTextNode('[TC DATE]'));
  }
  
  calculateHeight(buffer) {
    return 20;
  }
  
  store(msg, sharedTemplateData) {
    return DataSeqDiaTestCaseStart.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return DataSeqDiaTestCaseStart.restore(buffer);
  }
  
  create(data) {
    const node = this.testCaseStart.cloneNode(true);
    node.replaceChild(document.createTextNode(data.name), node.childNodes[2]);
    node.replaceChild(document.createTextNode(data.date), node.childNodes[4]);
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateSeqDiaTestCaseStart;
