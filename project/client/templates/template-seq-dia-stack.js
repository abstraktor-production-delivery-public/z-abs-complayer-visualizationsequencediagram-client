
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaEventInfo from './components/component-seq-dia-event-info';
import ComponentSeqDiaEventRow from './components/component-seq-dia-event-row';
import DataSeqDiaStack from '../data/data-seq-dia-stack';
import FilterSize from '../logic/filter-size';
import LogDataStackType from 'z-abs-funclayer-engine-cs/clientServer/log/log-data-stack-type';


class TemplateSeqDiaStack {
  constructor() {
    this.filter = null;
    this.stackTemplates = null;
    this.eventInfoIds = null;
  }
  
  init(filter, stackStyles) {
    this.filter = filter;
    
    this.stackTemplates = new Array(5);
    for(let i = 0; i < 5; ++i) {
      this.stackTemplates[i] = new Array(stackStyles.size);
    }
    stackStyles.forEach((protocol, protocolKey) => {
      this.stackTemplates[0][protocol.index] = this._new(protocolKey);
      this.stackTemplates[1][protocol.index] = this._shared(protocolKey);
      this.stackTemplates[2][protocol.index] = this._use(protocolKey);
      this.stackTemplates[3][protocol.index] = this._store(protocolKey);
      this.stackTemplates[4][protocol.index] = this._del(protocolKey);
    });
    this.eventInfoIds = new Array(LogDataStackType.results.length);
    LogDataStackType.results.forEach((result, index) => {
      const leftRow = ComponentSeqDiaEventInfo.createLeftRowDiv('seq_dia__data_stack');
      ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, result, 'seq_dia__data_stack_info', false);
      const rightRow = ComponentSeqDiaEventInfo.createRightRowDiv('seq_dia__data_stack');
      ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, 'instance', 'seq_dia__data_stack_instance', false);
      ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '${prot}', 'seq_dia__data_stack_protocol', true);
      this.eventInfoIds[index] = ComponentSeqDiaEventInfo.createTemplate(leftRow, rightRow);
    });
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaStack.restoreHeightData(buffer);
    return this.filter.getStackEvents(phaseId) ? Const.STATE_EVENT_HEIGHT : 0;
  }
  
  store(msg, sharedTemplateData) {
    return DataSeqDiaStack.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return DataSeqDiaStack.restore(buffer);
  }
  
  create(data) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const actorIndex = data.actorIndex;
    const phaseId = data.phaseId;
    const protocolIndex = data.protocolIndex;
    const type = data.type;
    const stackData = data.stackData;
    const node = ComponentSeqDiaEventRow.cloneRow('seq_dia_stack');
    const divNodeLeft = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnLeft(phaseId));
    for(let i = 0; i < actorPhases.length; ++i) {
      const divNodeMiddle = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnMiddle(i, phaseId, actorParts[i]));
      if(actorIndex === i) {
        const svg = divNodeMiddle.appendChild(ComponentSeqDiaEventRow.cloneSvg(Const.STATE_EVENT_HEIGHT, Const.RENDER_POSITION_NORMAL));
        svg.appendChild(this.stackTemplates[type][protocolIndex].cloneNode(true));
        divNodeMiddle.appendChild(ComponentSeqDiaEventInfo.clone(this.eventInfoIds[type], 0, protocolIndex));
        divNodeMiddle.appendChild(ComponentSeqDiaEventInfo.clone(this.eventInfoIds[type], 1, protocolIndex));
        const stackNode = divNodeMiddle.lastChild.firstChild.firstChild;
        stackNode.textContent = stackData;
      }
    }
    const divNodeRight = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnRight(actorPhases.length, phaseId));
    return node;
  }
  
  destroy(node) {}
  
  _rectBackground() {
    const bgRect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    bgRect.classList.add('stack_seq_dia_bg');
    bgRect.setAttribute('x', FilterSize.eventWidthHalf - 6);
    bgRect.setAttribute('width', 12);
    bgRect.setAttribute('y', 1);
    bgRect.setAttribute('height', 8);
    return bgRect;
  }
  
  _rect(g, protocolKey, x) {
    const connLeftRect = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'rect'));
    connLeftRect.classList.add('stack_seq_dia_conn', `seq_dia_protocol_${protocolKey}_small`);
    connLeftRect.setAttribute('x', FilterSize.eventWidthHalf + x);
    connLeftRect.setAttribute('width', 3);
    connLeftRect.setAttribute('y', 3.5);
    connLeftRect.setAttribute('height', 3);
  }
  
  _cross(g, protocolKey, x) {
    const connLine1 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    connLine1.classList.add('stack_seq_dia_conn');
    connLine1.setAttribute('x1', FilterSize.eventWidthHalf + x);
    connLine1.setAttribute('x2', FilterSize.eventWidthHalf + x + 3);
    connLine1.setAttribute('y1', 3.5);
    connLine1.setAttribute('y2', 6.5);

    const connLine2 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    connLine2.classList.add('stack_seq_dia_conn');
    connLine2.setAttribute('x1', FilterSize.eventWidthHalf + x);
    connLine2.setAttribute('x2', FilterSize.eventWidthHalf + x + 3);
    connLine2.setAttribute('y1', 6.5);
    connLine2.setAttribute('y2', 3.5);
    
    const circle1 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
    circle1.classList.add('stack_seq_dia_conn', `seq_dia_protocol_${protocolKey}_small`);
    circle1.setAttribute('cx', FilterSize.eventWidthHalf + x + 1.5);
    circle1.setAttribute('cy', 5);
    circle1.setAttribute('r', 0.5);
  }
  
  _new(protocolKey) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.appendChild(this._rectBackground());
    const connLine = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    connLine.classList.add('stack_seq_dia_conn');
    connLine.setAttribute('x1', FilterSize.eventWidthHalf - 3.5);
    connLine.setAttribute('x2', FilterSize.eventWidthHalf + 1);
    connLine.setAttribute('y1', 5);
    connLine.setAttribute('y2', 5);
    this._rect(g, protocolKey, -4.5);
    return g;
  }
  
  _shared(protocolKey) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.appendChild(this._rectBackground());
    const connLine = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    connLine.classList.add('stack_seq_dia_conn');
    connLine.setAttribute('x1', FilterSize.eventWidthHalf - 3.5);
    connLine.setAttribute('x2', FilterSize.eventWidthHalf + 1);
    connLine.setAttribute('y1', 5);
    connLine.setAttribute('y2', 5);
    this._rect(g, protocolKey, -4.5);
    return g;
  }
  
  _use(gTop, protocolKey) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.appendChild(this._rectBackground());
    this._rect(g, protocolKey, -4.5);
    this._rect(g, protocolKey, 1.5);
    return g;
  }
  
  _store(protocolKey) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.appendChild(this._rectBackground());
    this._rect(g, protocolKey, -3.5);
    this._rect(g, protocolKey, 0.5); 
    return g;
  }
  
  _del(protocolKey) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.appendChild(this._rectBackground());
    const connLine = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    connLine.classList.add('stack_seq_dia_conn');
    connLine.setAttribute('x1', FilterSize.eventWidthHalf - 3.5);
    connLine.setAttribute('x2', FilterSize.eventWidthHalf + 3.5);
    connLine.setAttribute('y1', 5);
    connLine.setAttribute('y2', 5);
    this._cross(g, protocolKey, -4.5);
    this._cross(g, protocolKey, 1.5);
    return g;
  }
}


module.exports = TemplateSeqDiaStack;
