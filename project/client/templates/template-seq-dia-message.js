
'use strict';

import Const from '../logic/const';
import DataSeqDiaMessage from '../data/data-seq-dia-message';
import SeqDiaFilter from '../logic/seq-dia-filter';
import FilterSize from '../logic/filter-size';


class TemplateSeqDiaMessage {
  constructor(seqDiaMsgTemplate, seqDiaProtocolTemplate, seqDiaEnvelopTemplate) {
    this.seqDiaMsgTemplate = seqDiaMsgTemplate;
    this.seqDiaProtocolTemplate = seqDiaProtocolTemplate;
    this.seqDiaEnvelopTemplate = seqDiaEnvelopTemplate;
    this.filter = null;
    this.seqDiaEventTemplate = null;
    this.seqDiaEventTemplate = null;
    this.divTemplate = null;
    this.msgSvgTemplate = new Array(2);
    this.arrowTemplate = new Array(2);
  }
  
  init(seqDiaEventTemplate, filter, actorPhaseConst) {
    this.seqDiaEventTemplate = seqDiaEventTemplate;
    this.filter = filter;
    this.divTemplate = new Array(actorPhaseConst.namesRunning.length);
    
    const divTemplate = document.createElement('div');
  
    actorPhaseConst.namesRunning.forEach((phase, indexPhase) => {
      const divNode = divTemplate.cloneNode(true);
      
      divNode.classList.add(`${Const.phasePrefixClasses[indexPhase]}message_event`);
      this.divTemplate[indexPhase] = divNode;
      
      const msgEastSvgTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      msgEastSvgTemplate.setAttribute('height', Const.ACTOR_EVENT_HEIGHT  + 4);
      msgEastSvgTemplate.setAttribute('width', FilterSize.event2WidthAndHalf);
      msgEastSvgTemplate.setAttribute('style', `position:relative;left:${FilterSize.eventWidthHalf}px;`);
         
      const msgEastTextTemplate = msgEastSvgTemplate.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
      msgEastTextTemplate.classList.add('seq_dia_msg_caption_text_left_right');
      msgEastTextTemplate.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 5);
      msgEastTextTemplate.appendChild(document.createTextNode('[msg]'));
      msgEastTextTemplate.setAttribute('x', Const.TEXT_BIAS);
      
      const msgWestSvgTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      msgWestSvgTemplate.setAttribute('height', Const.ACTOR_EVENT_HEIGHT + 4);
      msgWestSvgTemplate.setAttribute('width', FilterSize.event2WidthAndHalf);
      msgWestSvgTemplate.setAttribute('style', `position:relative;left:-${FilterSize.event2Width}px;`);
      
      const msgWestTextTemplate = msgWestSvgTemplate.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
      msgWestTextTemplate.classList.add('seq_dia_msg_caption_text_right_left');
      msgWestTextTemplate.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 5);
      msgWestTextTemplate.appendChild(document.createTextNode('[msg]'));
      msgWestTextTemplate.setAttribute('x', FilterSize.event2WidthAndHalf - Const.TEXT_BIAS);
      
      this.msgSvgTemplate[Const.DIRECTION_EAST] = msgEastSvgTemplate;
      this.msgSvgTemplate[Const.DIRECTION_WEST] = msgWestSvgTemplate;
      
      const arrowEastTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
      arrowEastTemplate.setAttribute('points', `${FilterSize.eventWidthHalf - 12},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF - 4} ${FilterSize.eventWidthHalf - 12},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF + 4} ${FilterSize.eventWidthHalf - 1},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF}`);

      const arrowWestTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
      arrowWestTemplate.setAttribute('points', `${FilterSize.eventWidthHalf + 12},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF - 4} ${FilterSize.eventWidthHalf + 12},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF + 4} ${FilterSize.eventWidthHalf + 1},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF}`);
    
      this.arrowTemplate[Const.DIRECTION_EAST] = arrowEastTemplate;
      this.arrowTemplate[Const.DIRECTION_WEST] = arrowWestTemplate;
    });
  }
  
  calculateHeight(dataSeqDiaMessage) {
    return this.filter.getMessageEvents(dataSeqDiaMessage.phaseId) && dataSeqDiaMessage.ready ? Const.MESSAGE_EVENT_HEIGHT_DOUBLE : 0;
  }
  
  create(connectionData, persistData) {
    const dataSeqDiaMessage = new DataSeqDiaMessage(connectionData, persistData);
    return new ScrollListNode(this, dataSeqDiaMessage, null, this.filter, [SeqDiaFilter.seqDiaPhaseEvent, SeqDiaFilter.seqDiaMessage]);
  }
  
  clone(connectionData, persistData, reverseDirection, cbResize) {
    const node = this.divTemplate[persistData.phaseId].cloneNode(true);
    const phaseId = persistData.phaseId;
    const nodeData = new NodeData(node, this, this.std.filter.getMessageEvents(phaseId) ? Const.MESSAGE_EVENT_HEIGHT_DOUBLE : 0, []);
    nodeData.addEventListener('seq_dia_message', function(event) {
      this.height = event.detail.show[phaseId] ? Const.MESSAGE_EVENT_HEIGHT_DOUBLE : 0
    });
    const nodeDataPair = {
      node: nodeData,
      msgNode: null
    };
    const messageNode = this.seqDiaEventTemplate.cloneMessage(Const.PART_LEFT, persistData.phaseId);
    node.appendChild(messageNode);
    const actors = this.std.sortedActorDatas;
    nodeDataPair.node.data.push(null);
    for(let i = 0; i < actors.length; ++i) {
      const middleNodeX = this.seqDiaEventTemplate.cloneMessage(this.std.actorParts[i], persistData.phaseId);
      node.appendChild(middleNodeX);
      const parameters = this.seqDiaProtocolTemplate.createParameters(connectionData, i, persistData.logRow.data.remote.type, reverseDirection);
      nodeDataPair.node.data.push(!parameters ? null : {
        direction: parameters[0],
        part: parameters[1],
        arrow: true
      });
      if(null !== parameters) {
        const middleSvg = this.seqDiaEventTemplate.getMiddleSvg(middleNodeX);
        if(parameters[1] === Const.PART_LEFT) {
          const middleNode = this.seqDiaProtocolTemplate.clone(...parameters, persistData.protocolIndex, connectionData, connectionData.lineEventId);
          middleSvg.appendChild(middleNode);
          if(parameters[0] === Const.DIRECTION_EAST) {
            nodeDataPair.msgNode = this.seqDiaMsgTemplate.clone(i, persistData, Const.MESSAGE_NORMAL, cbResize);
            const envelopeNode = this.seqDiaEnvelopTemplate.clone(connectionData, nodeDataPair.msgNode, persistData.logRow.data.remote.type, reverseDirection)
            middleNode.appendChild(envelopeNode);
            const svgNode = this.msgSvgTemplate[Const.DIRECTION_EAST].cloneNode(true);
            middleNode.parentNode.parentNode.insertBefore(svgNode, middleNode.parentNode.parentNode.firstChild);
            svgNode.lastChild.firstChild.nodeValue = persistData.logRow.data.caption;
            this.seqDiaProtocolTemplate.setMsgNbr(middleSvg, ...parameters, Const.MESSAGE_DIRECTION_SENT, connectionData.messagesSent);
          }
          else {
            const arrowNode = this.arrowTemplate[Const.DIRECTION_WEST].cloneNode(true);
            middleSvg.appendChild(arrowNode);
            this.seqDiaProtocolTemplate.getMiddleProtocolLine(middleSvg).setAttribute('x1', FilterSize.eventWidthHalf + 10);
            this.seqDiaProtocolTemplate.setMsgNbr(middleSvg, ...parameters, Const.MESSAGE_DIRECTION_RECEIVED, connectionData.messagesReceived);
          }
        }
        else if(parameters[1] === Const.PART_MIDDLE) {
          const middleNode = this.seqDiaProtocolTemplate.clone(...parameters, persistData.protocolIndex, connectionData, connectionData.lineEventId)
          middleSvg.appendChild(middleNode);
        }
        else { // Const.PART_RIGHT
          const middleNode = this.seqDiaProtocolTemplate.clone(...parameters, persistData.protocolIndex, connectionData, connectionData.lineEventId);
          middleSvg.appendChild(middleNode);
          if(parameters[0] === Const.DIRECTION_EAST) {
            const arrowNode = this.arrowTemplate[Const.DIRECTION_EAST].cloneNode(true);
            middleSvg.appendChild(arrowNode);
            this.seqDiaProtocolTemplate.getMiddleProtocolLine(middleSvg).setAttribute('x2', FilterSize.eventWidthHalf - 10);
            this.seqDiaProtocolTemplate.setMsgNbr(middleSvg, ...parameters, Const.MESSAGE_DIRECTION_RECEIVED, connectionData.messagesReceived);
          }
          else {
            nodeDataPair.msgNode = this.seqDiaMsgTemplate.clone(i, persistData, Const.MESSAGE_NORMAL, cbResize);
            const envelopeNode = this.seqDiaEnvelopTemplate.clone(connectionData, nodeDataPair.msgNode, persistData.logRow.data.remote.type, reverseDirection);
            middleNode.appendChild(envelopeNode);
            const svgNode = this.msgSvgTemplate[Const.DIRECTION_WEST].cloneNode(true);
            const msgNode = middleNode.parentNode.parentNode.insertBefore(svgNode, middleNode.parentNode.parentNode.firstChild);
            msgNode.lastChild.firstChild.nodeValue = persistData.logRow.data.caption;
            this.seqDiaProtocolTemplate.setMsgNbr(middleSvg, ...parameters, Const.MESSAGE_DIRECTION_SENT, connectionData.messagesSent);
          }
        }
      }
    }
    nodeDataPair.node.data.push(null);
    this.seqDiaMsgTemplate.createInners(nodeDataPair.msgNode, persistData.protocolIndex);
    const eventNode = this.seqDiaEventTemplate.cloneMessage(Const.PART_RIGHT, persistData.phaseId);
    node.appendChild(eventNode);
    return nodeDataPair;
  }
  
  _setSize(stateType) {
    return this.std.filter.filterShowNotExecutedStateChanges ? Const.STATE_EVENT_HEIGHT : 0;
  }
}


module.exports = TemplateSeqDiaMessage;
