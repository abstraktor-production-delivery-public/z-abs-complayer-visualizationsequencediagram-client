
'use strict';

import DataSeqDiaTestCaseEnd from '../data/data-seq-dia-test-case-end';
import Const from '../logic/const';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';


class TemplateSeqDiaTestCaseEnd {
  constructor() {
    this.filter = null;
    this.testCaseEndNodes = [];
  }
  
  init(filter) {
    this.filter = filter;
    
    const testCaseEndNode = document.createElement('div');
    testCaseEndNode.classList.add('seq_dia_test_case_end');
    const textPNode = testCaseEndNode.appendChild(document.createElement('p'));
    textPNode.classList.add('seq_dia_test_case_end_text');
    textPNode.appendChild(document.createTextNode('TEST CASE END'));

    const resultNode = testCaseEndNode.appendChild(document.createElement('div'));
    resultNode.classList.add('seq_dia_test_case_end_result');
    const resultPNode = resultNode.appendChild(document.createElement('p'));
    resultPNode.classList.add('seq_dia_test_case_end_result');
    resultPNode.appendChild(document.createTextNode('[result]'));
    
    const durationPNode = testCaseEndNode.appendChild(document.createElement('p'));
    durationPNode.classList.add('seq_dia_test_case_end_duration');
    durationPNode.appendChild(document.createTextNode('[TC DATE]'));
    
    ActorResultConst.results.forEach((result, index) => {
      const node = testCaseEndNode.cloneNode(true);
      node.childNodes[1].firstChild.firstChild.nodeValue = result;
      node.childNodes[1].classList.add(ActorResultConst.classes[index]);
      this.testCaseEndNodes.push(node);
    });
  }
  
  calculateHeight(buffer) {
    return 20;
  }
    
  store(msg, sharedTemplateData) {
    return DataSeqDiaTestCaseEnd.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return DataSeqDiaTestCaseEnd.restore(buffer);
  }
  
  create(data) {
    const node = this.testCaseEndNodes[data.resultId].cloneNode(true);
    node.childNodes[2].replaceChild(document.createTextNode(data.duration), node.childNodes[2].firstChild);
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateSeqDiaTestCaseEnd;
