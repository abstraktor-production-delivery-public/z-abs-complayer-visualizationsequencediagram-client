
'use strict';

import Const from '../logic/const';
import DataSeqDiaConnectionClosing from '../data/data-seq-dia-connection-closing';
import FilterSize from '../logic/filter-size';
import ComponentSeqDiaShared from './components/component-seq-dia-shared';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaConnectionClosing extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaConnectionClosing, 'seq_dia_connection_closing', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const closingLine1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine1.classList.add('seq_dia_event_connection_closing');
    closingLine1.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine1.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine1.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine1.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingLine2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine2.classList.add('seq_dia_event_connection_closing');
    closingLine2.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine2.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine2.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine2.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingCircle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    closingCircle.classList.add('seq_dia_client_connecting');
    closingCircle.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    closingCircle.setAttribute('r', 2);
    closingCircle.setAttribute('cx', FilterSize.eventWidthHalf);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(closingLine1.cloneNode(true));
    this.fragment.appendChild(closingLine2.cloneNode(true));
    this.fragment.appendChild(closingCircle.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [fragment, null, fragment, null, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaConnectionClosing.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
  
  onCloneToSvg(localNode, remoteNode, data) {
    const reverseDirection = data.reverseDirection ? 1 : 0;
    if(data.localIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
    }
    if(data.extraData.remoteIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection)); 
    }
  }
}


module.exports = TemplateSeqDiaConnectionClosing;
