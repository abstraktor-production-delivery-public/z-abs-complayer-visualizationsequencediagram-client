
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerDetached from '../data/data-seq-dia-server-detached';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerDetached extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerDetached, 'seq_dia_server_detached', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
    
  onInit() {
    const startRect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    startRect.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT - 4);
    startRect.setAttribute('height', 8);
    startRect.setAttribute('x', FilterSize.eventWidthHalf - 4);
    startRect.setAttribute('width', 8);
    
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circle.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    circle.setAttribute('cx', FilterSize.eventWidthHalf - 9);
    circle.setAttribute('r', 4);
    
    const lineHorizontal = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    lineHorizontal.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    lineHorizontal.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    lineHorizontal.setAttribute('x1', FilterSize.eventWidthHalf - 11);
    lineHorizontal.setAttribute('x2', FilterSize.eventWidthHalf - 7);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(startRect);
    this.fragment.appendChild(circle);
    this.fragment.appendChild(lineHorizontal);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    fragment.childNodes[1].classList.add(`seq_dia_protocol_${protocolKey}_small`);
    fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_info`);
    return [null, null, fragment, null, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerDetached.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerDetached;
