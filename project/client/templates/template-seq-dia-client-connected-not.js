
'use strict';

import Const from '../logic/const';
import DataSeqDiaClientConnectedNot from '../data/data-seq-dia-client-connected-not';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaClientConnectedNot extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaClientConnectedNot, 'seq_dia_client_connected_not', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const closingLine1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine1.classList.add('seq_dia_event_connected_not');
    closingLine1.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine1.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine1.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine1.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingLine2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine2.classList.add('seq_dia_event_connected_not');
    closingLine2.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine2.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine2.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine2.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingCircleOuter = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    closingCircleOuter.classList.add('seq_dia_event_connected_not');
    closingCircleOuter.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    closingCircleOuter.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const closingCircleInner = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    closingCircleInner.classList.add('seq_dia_event_connected_not');
    closingCircleInner.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    closingCircleInner.setAttribute('cx', FilterSize.eventWidthHalf);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(closingLine1.cloneNode(true));
    this.fragment.appendChild(closingLine2.cloneNode(true));
    this.fragment.appendChild(closingCircleOuter.cloneNode(true));
    this.fragment.appendChild(closingCircleInner.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_small_fail_outer`);
    fragment.childNodes[3].classList.add(`seq_dia_protocol_${protocolKey}_small_fail_inner`);
    return [fragment, fragment, fragment, fragment, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaClientConnectedNot.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaClientConnectedNot;
