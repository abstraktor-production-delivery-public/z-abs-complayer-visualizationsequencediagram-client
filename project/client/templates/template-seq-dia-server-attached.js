
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerAttached from '../data/data-seq-dia-server-attached';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerAttached extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerAttached, 'seq_dia_server_attached', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const startArrow = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    startArrow.classList.add('seq_dia_arrow');
    const arrowWidth = 4;
    const startArrowX1 = FilterSize.eventWidthHalf - arrowWidth;
    const startArrowX2 = FilterSize.eventWidthHalf + arrowWidth;
    const startArrowY1 = Const.STATE_EVENT_HALF_HEIGHT - arrowWidth;
    const startArrowY2 = Const.STATE_EVENT_HALF_HEIGHT;
    const startArrowY3 = Const.STATE_EVENT_HALF_HEIGHT + arrowWidth;
    startArrow.setAttribute('points', `${startArrowX1},${startArrowY1} ${startArrowX1},${startArrowY3} ${startArrowX2},${startArrowY2}`);
    
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circle.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    circle.setAttribute('cx', FilterSize.eventWidthHalf - 9);
    circle.setAttribute('r', 4);
    
    const lineHorizontal = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    lineHorizontal.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    lineHorizontal.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    lineHorizontal.setAttribute('x1', FilterSize.eventWidthHalf - 11);
    lineHorizontal.setAttribute('x2', FilterSize.eventWidthHalf - 7);
    
    const lineVertical = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    lineVertical.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 2);
    lineVertical.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 2);
    lineVertical.setAttribute('x1', FilterSize.eventWidthHalf - 9);
    lineVertical.setAttribute('x2', FilterSize.eventWidthHalf - 9);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(startArrow);
    this.fragment.appendChild(circle);
    this.fragment.appendChild(lineHorizontal);
    this.fragment.appendChild(lineVertical);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    fragment.childNodes[1].classList.add(`seq_dia_protocol_${protocolKey}_small`);
    fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_info`);
    fragment.childNodes[3].classList.add(`seq_dia_protocol_${protocolKey}_info`);
    return [null, null, fragment, null, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerAttached.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerAttached;
