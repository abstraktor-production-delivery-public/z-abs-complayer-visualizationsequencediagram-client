
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerStarted from '../data/data-seq-dia-server-started';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerStarted extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerStarted, 'seq_dia_server_started', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const startArrow = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    startArrow.classList.add('seq_dia_arrow');
    const arrowWidth = 4;
    const startArrowX1 = FilterSize.eventWidthHalf - arrowWidth;
    const startArrowX2 = FilterSize.eventWidthHalf + arrowWidth;
    const startArrowY1 = Const.STATE_EVENT_HALF_HEIGHT - arrowWidth;
    const startArrowY2 = Const.STATE_EVENT_HALF_HEIGHT;
    const startArrowY3 = Const.STATE_EVENT_HALF_HEIGHT + arrowWidth;
    startArrow.setAttribute('points', `${startArrowX1},${startArrowY1} ${startArrowX1},${startArrowY3} ${startArrowX2},${startArrowY2}`);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(startArrow);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [null, null, fragment, null, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerStarted.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerStarted;
