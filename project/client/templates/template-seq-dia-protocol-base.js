
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaProtocol from './components/component-seq-dia-protocol';


class TemplateSeqDiaProtocolBase {
  constructor(data, className, height) {
    this.data = data;
    this.className = className;
    this.height = height;
    this.filter = null;
    this.stackStyles = null;
    this.svgTemplate = null;
  }
  
  init(filter, stackStyles) {
    this.filter = filter;
    this.stackStyles = stackStyles;
    if(this.onInit) {
      this.onInit();
      this.svgTemplate = new Array(stackStyles.size);
      stackStyles.forEach((protocol, protocolKey) => {
        const fragments = this.onCreateSvgFragment(protocolKey);
        this.svgTemplate[protocol.index] = new Array(7);
        this.svgTemplate[protocol.index][Const.EAST_START] = fragments[Const.EAST_START];
        this.svgTemplate[protocol.index][Const.EAST_STOP] = fragments[Const.EAST_STOP];
        this.svgTemplate[protocol.index][Const.WEST_START] = fragments[Const.WEST_START];
        this.svgTemplate[protocol.index][Const.WEST_STOP] = fragments[Const.WEST_STOP];
        this.svgTemplate[protocol.index][Const.UNKNOWN_START] = fragments[Const.UNKNOWN_START];
        this.svgTemplate[protocol.index][Const.EAST_MIDDLE] = null;
        this.svgTemplate[protocol.index][Const.WEST_MIDDLE] = null;
      });
    }
  }
  
  store(msg, sharedTemplateData) {
    return this.data.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return this.data.restore(buffer);
  }
  
  create(data) {
    const protocolIndex = data.protocolIndex;
    const node = ComponentSeqDiaProtocol.cloneInto(this.className, this.height, data, (fragment) => {
      const template = this.svgTemplate[protocolIndex][fragment];
      if(template) {
        return template.cloneNode(true);
      }
      else {
        return null;
      }
    });
    if(this.onCloneToSvg) {
      const localActorIndex = data.localActorIndex;
      const remoteActorIndex = data.remoteActorIndex;
      node.setAttribute('data-local-actor-index', localActorIndex);
      node.setAttribute('data-remote-actor-index', remoteActorIndex);
      node.setAttribute('data-render-nodes', data.renderNodes);
      this.onCloneToSvg(node.firstChild.childNodes[localActorIndex + 1].childNodes[1].firstChild, Const.RENDER_TWO_NODES === data.renderNodes && remoteActorIndex >= 0 ? node.firstChild.childNodes[remoteActorIndex + 1].childNodes[1].firstChild : null, data);
    }
    if(this.onCloneInfo) {
      this.onCloneInfo(node.firstChild.childNodes[data.localActorIndex + 1], data);
    }
    return node;
  }
  
  destroy(node) {
    if(this.onDestroyToSvg) {
      const localActorIndex = Number.parseInt(node.getAttribute('data-local-actor-index'));
      const remoteActorIndex = Number.parseInt(node.getAttribute('data-remote-actor-index'));
      const renderNodes = Number.parseInt(node.getAttribute('data-render-nodes'));
      this.onDestroyToSvg(node.firstChild.childNodes[localActorIndex + 1].childNodes[1].firstChild, Const.RENDER_TWO_NODES === renderNodes && remoteActorIndex >= 0 ? node.firstChild.childNodes[remoteActorIndex + 1].childNodes[1].firstChild : null);
    }
  }
}


module.exports = TemplateSeqDiaProtocolBase;
