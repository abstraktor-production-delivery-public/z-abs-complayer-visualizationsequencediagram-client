
'use strict';

import ContentDecoder from '../content-decoders/content-decoder';
import Const from '../logic/const';
import LogPartType from 'z-abs-funclayer-engine-cs/clientServer/log/log-part-type';


class TemplateSeqDiaMsg {
  constructor() {
    this.filter = null;
    this.seqDiaEventTemplate = null;
    this.divTemplate = null;
    this.divMsgTemplate = null;
    this.logInner = null;
    this.divInnerTemplate = null;
    this.divViewerTemplate = [new Array(2), new Array(2)];
    this.arrowTemplate = new Array(2);
    this.protocolBufferToolbarClassName = [];
    this.dynamicInner = null;
    this.dynamicInnerText = null;
    this.dynamicInnerTextFirst = null;
    this.dynamicInnerRef = null;
    this.dynamicInnerRefFirst = null;
    //this.dynamicInnerBufferImage = null;
    //this.dynamicInnerBufferImageFirst = null;
    this.NO_INNERS = 0;
    this.INNERS = 1;
    this.LOG_OBJECT = 0;
    this.LOG_BUFFER = 1;
    this.INNER_CAPTION_HEIGHT = 20;
    this.INNER_ROW_HEIGHT = 16;
    this.INNER_BORDER_HEIGHT = 4;
    this.transportNames = [
     'tcp',
     'udp',
     'mc',
     'tls'
    ];
  }
  
  init(seqDiaEventTemplate, filter, stackStyles, actorPhaseConst, logInner) {
    this.seqDiaEventTemplate = seqDiaEventTemplate;
    this.filter = filter;
    this.divTemplate = new Array(actorPhaseConst.namesRunning.length);
    this.divMsgTemplate = new Array(actorPhaseConst.namesRunning.length);
    this.logInner = logInner;
     
    this.divInnerTemplate = new Array(stackStyles.size);
    
    this.arrowTemplate[Const.HIDE] = this._createArrowTemplate(false);
    this.arrowTemplate[Const.SHOW] = this._createArrowTemplate(true);
    
    const divTemplate = document.createElement('div');
    
    actorPhaseConst.namesRunning.forEach((phase, indexPhase) => {
      const divNodeMessage = divTemplate.cloneNode(true);
      divNodeMessage.classList.add(`${Const.phasePrefixClasses[indexPhase]}message_content_event__message`);
      const divNodeMessageDetail = divTemplate.cloneNode(true);
      divNodeMessageDetail.classList.add(`${Const.phasePrefixClasses[indexPhase]}message_content_event__message_detail`);
      this.divTemplate[indexPhase] = [divNodeMessage, divNodeMessageDetail];
      this.divMsgTemplate[indexPhase] = new Array(3);
      for(let i = 0; i < 3; ++i) {
        const middleTemplate = this.seqDiaEventTemplate.cloneMessage(i, indexPhase);
        this.divMsgTemplate[indexPhase][i] = middleTemplate;
      }
    });
    
    this.divViewerTemplate[Const.HIDE][this.NO_INNERS] = new Array(stackStyles.size);
    this.divViewerTemplate[Const.HIDE][this.INNERS] = new Array(stackStyles.size);
    this.divViewerTemplate[Const.SHOW][this.NO_INNERS] = new Array(stackStyles.size);
    this.divViewerTemplate[Const.SHOW][this.INNERS] = new Array(stackStyles.size);
    stackStyles.forEach((protocol, protocolKey) => {
      this.divInnerTemplate[protocol.index] = new Array(2);
      this.protocolBufferToolbarClassName.push(`seq_dia_protocol_${protocolKey}_buffer_toolbar`);
      
      const divOuterLr = document.createElement('div');
      divOuterLr.classList.add('seq_dia_protocol_outer');
      const divInnerLr = divOuterLr.appendChild(document.createElement('div'));
      divInnerLr.classList.add('seq_dia_protocol_inner');
      divInnerLr.classList.add(`seq_dia_protocol_${protocolKey}_inner`);
      const divHeaderLr = divInnerLr.appendChild(document.createElement('div'));
      divHeaderLr.classList.add('seq_dia_protocol_header');
      divHeaderLr.classList.add(`seq_dia_protocol_${protocolKey}_header`);
      const pHeaderStackLr = divHeaderLr.appendChild(document.createElement('p'));
      pHeaderStackLr.classList.add('seq_dia_protocol_header_stack');
      pHeaderStackLr.appendChild(document.createTextNode('[stack]'));
      const pHeaderTransportLr = divHeaderLr.appendChild(document.createElement('p'));
      pHeaderTransportLr.classList.add('seq_dia_protocol_header_transport');
      pHeaderTransportLr.appendChild(document.createTextNode('[transport]'));
      
      const divProtocolLr = divInnerLr.appendChild(document.createElement('div'));
      const divTopLr = divProtocolLr.appendChild(document.createElement('div'));
      divTopLr.classList.add('seq_dia_protocol_inners_outer_first');
      
      this.divInnerTemplate[protocol.index][Const.DIRECTION_EAST] = divOuterLr;
      
      const classLine = `seq_dia_protocol_${protocolKey}`;
      
      this.divViewerTemplate[Const.HIDE][this.NO_INNERS][protocol.index] = [this._createInnerViewerObjectTemplate(false, false), this._createInnerViewerBufferTemplate(false, false, protocol.index), this._createInnerViewerBufferInnerTemplate(false, protocol.index)];
      const hideInnersObject = this._createInnerViewerObjectTemplate(false, true);
      hideInnersObject.firstChild.firstChild.firstChild.firstChild.classList.add(`${classLine}_arrow`);
      const hideInnersBuffer = this._createInnerViewerBufferTemplate(false, true, protocol.index);
      hideInnersBuffer.firstChild.firstChild.firstChild.firstChild.classList.add(`${classLine}_arrow`);
      this.divViewerTemplate[Const.HIDE][this.INNERS][protocol.index] = [hideInnersObject, hideInnersBuffer];
      
      this.divViewerTemplate[Const.SHOW][this.NO_INNERS][protocol.index] = [this._createInnerViewerObjectTemplate(true, false), this._createInnerViewerBufferTemplate(true, false, protocol.index), this._createInnerViewerBufferInnerTemplate(true, protocol.index)];
      const showInnersObject = this._createInnerViewerObjectTemplate(true, true);
      showInnersObject.firstChild.firstChild.firstChild.firstChild.classList.add(`${classLine}_arrow`);
      const showInnersBuffer = this._createInnerViewerBufferTemplate(true, true, protocol.index);
      showInnersBuffer.firstChild.firstChild.firstChild.firstChild.classList.add(`${classLine}_arrow`);
      this.divViewerTemplate[Const.SHOW][this.INNERS][protocol.index] = [showInnersObject, showInnersBuffer];
    });
    
    this.dynamicInner = document.createElement('div');
    this.dynamicInner.classList.add('inner_dynamic_inline_flex');
  
    this.dynamicInnerText = document.createElement('pre');
    this.dynamicInnerText.classList.add('inner');
    this.dynamicInnerText.classList.add('seq_dia_protocol_base');
    this.dynamicInnerText.classList.add('seq_dia_protocol');
    this.dynamicInnerText.appendChild(document.createTextNode(''));
    
    this.dynamicInnerTextFirst = this.dynamicInnerText.cloneNode(true);
    this.dynamicInnerTextFirst.classList.replace('seq_dia_protocol', 'seq_dia_protocol_first');
 
    //this.dynamicInnerBufferText = this.dynamicInnerText.cloneNode(true);
    //this.dynamicInnerBufferText.classList.replace('inner', 'seq_dia_inner_buffer');
 
    this.dynamicInnerRef = document.createElement('a');
    this.dynamicInnerRef.classList.add('inner');
    this.dynamicInnerRef.classList.add('seq_dia_protocol_base');
    this.dynamicInnerRef.classList.add('seq_dia_protocol');
    this.dynamicInnerRef.setAttribute('href', '[[ref]]');
    this.dynamicInnerRef.setAttribute('target', '_blank');
    this.dynamicInnerRef.appendChild(document.createTextNode(''));
    
    this.dynamicInnerRefFirst = this.dynamicInnerRef.cloneNode(true);
    this.dynamicInnerRefFirst.classList.replace('seq_dia_protocol', 'seq_dia_protocol_first');
    
    /*this.dynamicInnerBufferImage = document.createElement('img');
    this.dynamicInnerBufferImage.classList.add('inner');
    this.dynamicInnerBufferImage.classList.add('seq_dia_protocol_base');
    this.dynamicInnerBufferImage.classList.add('seq_dia_protocol');
  
    this.dynamicInnerBufferImageFirst = this.dynamicInnerBufferImage.cloneNode(true);
    this.dynamicInnerBufferImageFirst.classList.replace('seq_dia_protocol', 'seq_dia_protocol_first');*/
  }
  
  clone(actorIndex, persistData, detail, cbResize) {
    const phaseId = persistData.phaseId;
    const inners = persistData.logRow.inners;
    const nodeData = {
      event: this.divTemplate[persistData.phaseId][detail].cloneNode(true),
      updater: this,
      height: 0,
      width: 0,
      startPos: 0,
      stopPos: 0,
      actorIndex: actorIndex,
      open: false,
      inners: inners,
      phaseId: phaseId,
      detail: detail,
      divInnerNode: null,
      cbResize: cbResize
    };
    const node = nodeData.event;
    const messageLeftNode = this.seqDiaEventTemplate.cloneMessage(Const.PART_LEFT, persistData.phaseId);
    node.appendChild(messageLeftNode);
    
    const actors = this.std.sortedActorDatas;
    for(let i = 0; i < actors.length; ++i) {
      if(actorIndex === i) {
        const middleNode = this.divMsgTemplate[persistData.phaseId][this.std.actorParts[i]].cloneNode(true);
        node.appendChild(middleNode);
        nodeData.divInnerNode = this.divInnerTemplate[persistData.protocolIndex][Const.DIRECTION_EAST].cloneNode(true);
        middleNode.appendChild(nodeData.divInnerNode);
        const headerDivNode = nodeData.divInnerNode.firstChild.firstChild;
        headerDivNode.firstChild.firstChild.nodeValue = persistData.logRow.data.stack;
        headerDivNode.lastChild.firstChild.nodeValue = Const.transportTypeNames[persistData.logRow.data.local.transportLayer.transportType];
      }
      else {
        const messageMiddleNode = this.seqDiaEventTemplate.cloneMessage(this.std.actorParts[i], persistData.phaseId);
        node.appendChild(messageMiddleNode);
      }
    }
    const messageRightNode = this.seqDiaEventTemplate.cloneMessage(Const.PART_RIGHT, persistData.phaseId);
    node.appendChild(messageRightNode);
    this.resize(nodeData);
    if(Const.MESSAGE_NORMAL === nodeData.detail) {
      document.addEventListener('seq_dia_message', (event) => {
        this.resize(nodeData);
      });
    }
    else { // Const.MESSAGE_DETAIL
      document.addEventListener('seq_dia_message_detail', (event) => {
        this.resize(nodeData);
      });
    }
    document.addEventListener('seq-dia-message-content-message', (event) => {
      this.resize(nodeData);
    });
  //  document.addEventListener('seq-dia-message-content-message_detail', (event) => {
     // nodeData.height = this.heightChanged(nodeData);
 //   });
    return nodeData;
  }
  
  createInners(nodeData, protocolIndex) {
    this._createInners(nodeData.divInnerNode.firstChild.lastChild.lastChild, nodeData, nodeData.inners, protocolIndex);
  }
  
  _setSvgLeftRightHeight(node, height) {
    const svgNode = node.firstChild;
    svgNode.setAttribute('height', height);
    svgNode.firstChild.setAttribute('height', height);
    svgNode.lastChild.setAttribute('y2', height);
  }
  
  _setSvgMiddleHeight(node, height, width) {
    const svgNode = node.firstChild;
    svgNode.setAttribute('height', height);
    if(0 !== width) {
      svgNode.setAttribute('width', width);
    }
    if(node.lastChild !== svgNode) {
      if(0 === width) {
        node.lastChild.setAttribute('style', `height:${height}px;`);
      }
      else {
        node.lastChild.setAttribute('style', `height:${height}px;width:${width}px;`);
      }
    }
    if('line' === svgNode.lastChild.nodeName) {
      svgNode.lastChild.setAttribute('y2', height);
    }
  }
  
  _calculateInnersHeight(inner) {
    let height = 0;
    inner.inners.forEach((inner) => {
      height += this.INNER_ROW_HEIGHT;
      height += !inner.generatedHeight ? 0 : inner.generatedHeight;
      if(inner.open) {
        height += this._calculateInnersHeight(inner);
      }
    });
    return height;
  }
  
  _maxInnersWidth(inner, width) {
    inner.inners.forEach((inner) => {
      width = Math.max(width, !inner.generatedWidth ? 0 : inner.generatedWidth + 4);
      if(inner.open) {
        width = this._maxInnersWidth(inner, width);
      }
    });
    return width;
  }
  
  open(nodeData) {
    nodeData.open = true;
    this.resize(nodeData);
    nodeData.cbResize();
  }
  
  close(nodeData) {
    nodeData.open = false;
    this.resize(nodeData);
    nodeData.cbResize();
  }
  
  resize(nodeData) {
    let height = 0;
    const show = this.std.filter.getMessageContentEvents(nodeData.phaseId, nodeData.detail, nodeData.open);
    if(show) {
      height += this.INNER_CAPTION_HEIGHT;
      height += this.INNER_BORDER_HEIGHT;
      height += this._calculateInnersHeight(nodeData);
      if(!nodeData.event.classList.contains('show_inner_log')) {
        nodeData.event.classList.add('show_inner_log');
      }
    }
    else {
      if(nodeData.event.classList.contains('show_inner_log')) {
        nodeData.event.classList.remove('show_inner_log');
      } 
    }
    nodeData.height = height;
    const node = nodeData.event;
    this._setSvgLeftRightHeight(node.firstChild, height);
    this._setSvgLeftRightHeight(node.lastChild, height);
    const width = this._maxInnersWidth(nodeData, 0);
    for(let i = 1; i < node.childNodes.length - 1;  ++i) {
      const middleNode = node.childNodes[i];
      if(middleNode.classList.contains('seq_dia_event_inner')) {
        this._setSvgMiddleHeight(middleNode, height, width);
      }
    }
  }
    
  _createInners(divNode, nodeData, inners, protocolIndex) {
    inners.forEach((inner) => {
      const haveInnersIndex = 0 !== inner.inners.length ? this.INNERS : this.NO_INNERS;
      const showIndex = inner.open ? Const.SHOW : Const.HIDE;
      const divInner = this.divViewerTemplate[showIndex][haveInnersIndex][protocolIndex][inner.type].cloneNode(true);
      divNode.appendChild(divInner);
      const div = this.dynamicInner.cloneNode(true)
      divInner.lastChild.previousSibling.appendChild(div);
      if(1 <= inner.logParts.length) {
        const innerPart = inner.logParts[0];
        if(LogPartType.REF === innerPart.type) {
          if(haveInnersIndex) {
            const refNode = this.dynamicInnerRef.cloneNode(true);
            div.appendChild(refNode);
          }
          else {
            const refNode = this.dynamicInnerRefFirst.cloneNode(true);
            div.appendChild(refNode);
          }
          div.lastChild.setAttribute('href', innerPart.ref);
          div.lastChild.firstChild.nodeValue = innerPart.text;
        }
        else if(LogPartType.TEXT === innerPart.type) {
          if(haveInnersIndex) {
            const textNode = this.dynamicInnerText.cloneNode(true);
            div.appendChild(textNode);
          }
          else {
            const textNode = this.dynamicInnerTextFirst.cloneNode(true);
            div.appendChild(textNode);
          }
          div.lastChild.firstChild.nodeValue = innerPart.text;
        }
      }
      for(let i = 1; i < inner.logParts.length; ++i) {
        const innerPart = inner.logParts[i];
        if(LogPartType.REF === innerPart.type) {
          const refNode = this.dynamicInnerRef.cloneNode(true);
          div.appendChild(refNode);
          div.lastChild.setAttribute('href', innerPart.ref);
          div.lastChild.firstChild.nodeValue = innerPart.text;
        }
        else if(LogPartType.TEXT === innerPart.type) {
          const textNode = this.dynamicInnerText.cloneNode(true);
          div.appendChild(textNode);
          div.lastChild.firstChild.nodeValue = innerPart.text;
        }
      }
      divInner.firstChild.firstChild.addEventListener('click', (event) => {
        if(event.currentTarget.parentNode.parentNode.lastChild.classList.toggle('seq_dia_protocol_inners_outer_hide')) {
          event.currentTarget.firstChild.setAttribute('transform', 'rotate(0 6 6)');
          inner.open = false;
          this.resize(nodeData);
          nodeData.cbResize();
        }
        else {
          event.currentTarget.firstChild.setAttribute('transform', 'rotate(45 6 6)');
          inner.open = true;
          if(this.logInner.TYPE_BUFFER === inner.type) {
            if(0 !== inner.inners.length) {
              inner.inners.forEach((innerInner, index) => {
                if(0 !== innerInner.logParts.length) {
                  if(!div.parentNode.nextSibling.childNodes[1].firstChild.firstChild.firstChild) {
                    const part = innerInner.logParts[0];
                    if(undefined !== part && LogPartType.BUFFER === part.type) {
                      const sizeF = (node, width, height) => {
                        innerInner.generatedHeight = height;
                        innerInner.generatedWidth = width;
                        node.parentNode.parentNode.setAttribute('style', `width:${width}px;height:${height}px;padding-left:0px;`);
                        this.resize(nodeData);
                        nodeData.cbResize();
                      };
                      const node = ContentDecoder.decode(part.buffer, div.parentNode.nextSibling.childNodes[0].childNodes, part.contentType, {transferEncoding: part.transferEncoding}, sizeF);
                      if(node) {
                        div.parentNode.nextSibling.childNodes[1].firstChild.firstChild.appendChild(node);
                      }
                      else {
                        this.resize(nodeData);
                        nodeData.cbResize();
                      }
                    }
                  }
                }
              });
            }
          }
        }
        this.resize(nodeData);
        nodeData.cbResize();
      });
      this._createInners(divInner.lastChild, nodeData, inner.inners, protocolIndex);
    });
  }
  
  _createArrowTemplate(show) {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.classList.add('seq_dia_arrow');
    svg.setAttribute('width', 16);
    svg.setAttribute('height', 16);
    const group = svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    if(show) {
      group.setAttribute('transform', 'rotate(45 6 6)'); 
    }
    const arrow = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
    arrow.classList.add('seq_dia_arrow');
    arrow.setAttribute('points', '5,4 5,12 13,8');
    return svg;
  }
    
  _createInnerViewerObjectTemplate(show, haveInner) {
    const divInnerTemplate = document.createElement('div');
    divInnerTemplate.classList.add('seq_dia_protocol_inners_inner');
    const divInnerInnerTemplate = divInnerTemplate.appendChild(document.createElement('div'));
    divInnerInnerTemplate.classList.add('seq_dia_protocol_inner_row');
    divInnerInnerTemplate.classList.add('OBJECT');
    if(haveInner) {
      const arrowNode = this.arrowTemplate[show ? Const.SHOW : Const.HIDE].cloneNode(true);
      divInnerInnerTemplate.appendChild(arrowNode);
    }
    const divTopTemplate = divInnerTemplate.appendChild(document.createElement('div'));
    divTopTemplate.classList.add('seq_dia_protocol_inners_outer');
    divTopTemplate.classList.add('seq_dia_protocol_inners_outer_show');
    if(!show) {
      divTopTemplate.classList.add('seq_dia_protocol_inners_outer_hide');
    }
    return divInnerTemplate;
  }
  
  _createInnerViewButton(protocolIndex, glyphIconId, buttonClassName) {
    const button = document.createElement('button');
    button.classList.add('seq_dia_protocol_inner_toolbar_button');
    button.classList.add(buttonClassName);
    const spanText = button.appendChild(document.createElement('span'));
    spanText.classList.add('glyphicon');
    spanText.classList.add(glyphIconId);
    spanText.classList.add(this.protocolBufferToolbarClassName[protocolIndex]);
    return button;
  }
  
  _createInnerViewerBufferTemplate(show, haveInner, protocolIndex) {
    const divInnerTemplate = document.createElement('div');
    divInnerTemplate.classList.add('seq_dia_protocol_inners_inner');
    const divInnerInnerTemplate = divInnerTemplate.appendChild(document.createElement('div'));
    divInnerInnerTemplate.classList.add('seq_dia_protocol_inner_row');
    divInnerInnerTemplate.classList.add('BUFFER');
    if(haveInner) {
      const arrowNode = this.arrowTemplate[show ? Const.SHOW : Const.HIDE].cloneNode(true);
      divInnerInnerTemplate.appendChild(arrowNode);
    }
    const divTopTemplate = divInnerTemplate.appendChild(document.createElement('div'));
    divTopTemplate.classList.add('seq_dia_protocol_inners_outer_show');
    if(!show) {
      divTopTemplate.classList.add('seq_dia_protocol_inners_outer_hide');
    }
    const divInnerToolbarTemplate = divTopTemplate.appendChild(document.createElement('div'));
    divInnerToolbarTemplate.classList.add('seq_dia_protocol_inner_toolbar');
    
    divInnerToolbarTemplate.appendChild(this._createInnerViewButton(protocolIndex, 'glyphicon-align-justify', 'seq_dia_json'));
    /*const buttonText = divInnerToolbarTemplate.appendChild(document.createElement('button'));
    buttonText.classList.add('seq_dia_protocol_inner_toolbar_button');
    const spanText = buttonText.appendChild(document.createElement('span'));
    spanText.classList.add('glyphicon-text-width');
    spanText.classList.add(this.protocolBufferToolbarClassName[protocolIndex]);*/
/*
    const buttonImage = divInnerToolbarTemplate.appendChild(document.createElement('button'));
    buttonImage.classList.add('seq_dia_protocol_inner_toolbar_button');
    const spanImage = buttonImage.appendChild(document.createElement('span'));
    spanImage.classList.add('glyphicon-picture');
    spanImage.classList.add(this.protocolBufferToolbarClassName[protocolIndex]);
    */
    return divInnerTemplate;
  }
  
  _createInnerViewerBufferInnerTemplate(show, protocolIndex) {
    const divInnerTemplate = document.createElement('div');
    divInnerTemplate.classList.add('seq_dia_protocol_inners_inner');
    const divInnerInnerTemplate = divInnerTemplate.appendChild(document.createElement('div'));
    divInnerInnerTemplate.classList.add('seq_dia_protocol_inner_row_buffer');
    divInnerInnerTemplate.classList.add('BUFFER_INNER');
    const divTopTemplate = divInnerTemplate.appendChild(document.createElement('div'));
    divTopTemplate.classList.add('seq_dia_protocol_inners_outer_show');
    if(!show) {
      divTopTemplate.classList.add('seq_dia_protocol_inners_outer_hide');
    }

    return divInnerTemplate;
  }
}


module.exports = TemplateSeqDiaMsg;
