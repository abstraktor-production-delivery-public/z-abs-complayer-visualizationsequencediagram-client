
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerStopped from '../data/data-seq-dia-server-stopped';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerStopped extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerStopped, 'seq_dia_server_stopped', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
    
  onInit() {
    const startRect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    startRect.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT - 4);
    startRect.setAttribute('height', 8);
    startRect.setAttribute('x', FilterSize.eventWidthHalf - 4);
    startRect.setAttribute('width', 8);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(startRect);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [null, null, fragment, null, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerStopped.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerStopped;
