
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaEventRow from './components/component-seq-dia-event-row';
import DataSeqDiaPhaseEnd from '../data/data-seq-dia-phase-end';
import FilterSize from '../logic/filter-size';


class TemplateSeqDiaPhaseEnd {
  constructor() {
    this.filter = null;
  }
  
  init(filter) {
    this.filter = filter;
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaPhaseEnd.restoreHeightData(buffer);
    return this.filter.getPhase(phaseId) ? Const.PHASE_HEIGHT : 0;
  }
  
  store(msg, sharedTemplateData) {
    return DataSeqDiaPhaseEnd.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return DataSeqDiaPhaseEnd.restore(buffer);
  }
    
  create(data) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const phaseId = data.phaseId;
    const node = ComponentSeqDiaEventRow.cloneRow('seq_dia_phase_end');
    const divNodeLeft = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnLeft(phaseId));
    divNodeLeft.classList.add('seq_dia_phase_end_middle');
    for(let i = 0; i < actorPhases.length; ++i) {
      const divNodeMiddle = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnMiddle(i, phaseId, actorParts[i]));
      divNodeMiddle.classList.add('seq_dia_phase_end_middle');
    }
    const divNodeRight = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnRight(actorPhases.length, phaseId));
    divNodeRight.classList.add('seq_dia_phase_end_middle');
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateSeqDiaPhaseEnd;
