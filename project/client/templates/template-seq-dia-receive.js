
'use strict';

import Const from '../logic/const';
import DataSeqDiaReceive from '../data/data-seq-dia-receive';
import FilterSize from '../logic/filter-size';
import ComponentSeqDiaArrow from './components/component-seq-dia-arrow';
import ComponentSeqDiaCaption from './components/component-seq-dia-caption';
import ComponentSeqDiaEnvelope from './components/component-seq-dia-envelope';
import ComponentSeqDiaShared from './components/component-seq-dia-shared';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaReceive extends TemplateSeqDiaProtocolBase {
  static ARROW_HEIGHT = 3;
  
  constructor() {
    super(DataSeqDiaReceive, 'seq_dia_receive', 20);
    this.fragmentEast = null;
    this.fragmentWest = null;
  }
  
  onInit() {
    this.fragmentEast = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragmentWest = this.fragmentEast.cloneNode(true);
    
    const eastLine = this.fragmentEast.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    eastLine.setAttribute('x1', Const.LINE_WIDTH_HALF + FilterSize.eventWidthHalf + 7);
    eastLine.setAttribute('x2', FilterSize.eventWidth);
    eastLine.setAttribute('y1', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    eastLine.setAttribute('y2', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    
    this.fragmentEast.appendChild(ComponentSeqDiaArrow.createWest(FilterSize.eventWidthHalf));
    this.fragmentEast.appendChild(ComponentSeqDiaEnvelope.clone(Const.EAST));
    
    const westLine = this.fragmentWest.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    westLine.setAttribute('x1', FilterSize.eventWidthHalf - Const.LINE_WIDTH_HALF - 7);
    westLine.setAttribute('x2', 0);
    westLine.setAttribute('y1', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    westLine.setAttribute('y2', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    
    
    this.fragmentWest.appendChild(ComponentSeqDiaArrow.createEast(FilterSize.eventWidthHalf));
    this.fragmentWest.appendChild(ComponentSeqDiaEnvelope.clone(Const.WEST));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragmentEastStart = this.fragmentEast.cloneNode(true);
    fragmentEastStart.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_msg`);
    const fragmentWestStart = this.fragmentWest.cloneNode(true);
    fragmentWestStart.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_msg`);
    return [fragmentEastStart, null, fragmentWestStart, null, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaReceive.restoreHeightData(buffer);
    return this.filter.getMessageDetailEvents(phaseId) ? this.height : 0;
  }
  
  onCloneToSvg(localNode, remoteNode, data) {
   ComponentSeqDiaEnvelope.addEventListener(localNode.lastChild);
    const reverseDirection = data.reverseDirection ? 1 : 0;
    if(data.localIsShared) {
      const shared = ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection);
      shared.setAttribute('transform', 'translate(0, 10)');
      localNode.appendChild(shared);
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      }
    }
    if(data.remoteIsShared) {
      const shared = ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection);
      shared.setAttribute('transform', 'translate(0, 10)');
      localNode.appendChild(shared); 
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection));
      }
    }
  }
    
  onDestroyToSvg(localNode, remoteNode) {
   ComponentSeqDiaEnvelope.removeEventListener(localNode.lastChild);
  }
  
  onCloneInfo(node, data) {
    const dataNode = ComponentSeqDiaCaption.clone(data.reverseDirection, data.extraData.caption);
    node.appendChild(dataNode);
  }
}


module.exports = TemplateSeqDiaReceive;
