
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerNotStarted from '../data/data-seq-dia-server-not-started';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerNotStarted extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerNotStarted, 'seq_dia_server_not_started', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const closingLine1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine1.classList.add('seq_dia_event_connected_not');
    closingLine1.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine1.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine1.setAttribute('x1', FilterSize.eventWidthHalf - 6);
    closingLine1.setAttribute('x2', FilterSize.eventWidthHalf + 4);
    
    const closingLine2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine2.classList.add('seq_dia_event_connected_not');
    closingLine2.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine2.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine2.setAttribute('x1', FilterSize.eventWidthHalf - 6);
    closingLine2.setAttribute('x2', FilterSize.eventWidthHalf + 4);
    
    const startArrowOuter = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    startArrowOuter.classList.add('seq_dia_arrow');
    const arrowOuterWidth = 4;
    const startArrowOuterX1 = FilterSize.eventWidthHalf - arrowOuterWidth;
    const startArrowOuterX2 = FilterSize.eventWidthHalf + arrowOuterWidth;
    const startArrowOuterY1 = Const.STATE_EVENT_HALF_HEIGHT - arrowOuterWidth;
    const startArrowOuterY2 = Const.STATE_EVENT_HALF_HEIGHT;
    const startArrowOuterY3 = Const.STATE_EVENT_HALF_HEIGHT + arrowOuterWidth;
    startArrowOuter.setAttribute('points', `${startArrowOuterX1},${startArrowOuterY1} ${startArrowOuterX1},${startArrowOuterY3} ${startArrowOuterX2},${startArrowOuterY2}`);
    
    const startArrowInner = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    startArrowInner.classList.add('seq_dia_arrow');
    const arrowInnerWidth = 2.2;
    const startArrowInnerX1 = FilterSize.eventWidthHalf - arrowInnerWidth - 0.6;
    const startArrowInnerX2 = FilterSize.eventWidthHalf + arrowInnerWidth - 1;
    const startArrowInnerY1 = Const.STATE_EVENT_HALF_HEIGHT - arrowInnerWidth + 0.2;
    const startArrowInnerY2 = Const.STATE_EVENT_HALF_HEIGHT;
    const startArrowInnerY3 = Const.STATE_EVENT_HALF_HEIGHT + arrowInnerWidth - 0.2;
    startArrowInner.setAttribute('points', `${startArrowInnerX1},${startArrowInnerY1} ${startArrowInnerX1},${startArrowInnerY3} ${startArrowInnerX2},${startArrowInnerY2}`);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(closingLine1);
    this.fragment.appendChild(closingLine2);
    this.fragment.appendChild(startArrowOuter);
    this.fragment.appendChild(startArrowInner);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.childNodes[2].classList.add(`seq_dia_protocol_${protocolKey}_small_fail_outer`);
    fragment.childNodes[3].classList.add(`seq_dia_protocol_${protocolKey}_small_fail_inner`);
    return [null, null, fragment, null, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerNotStarted.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerNotStarted;
