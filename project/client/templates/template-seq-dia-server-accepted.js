
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerAccepted from '../data/data-seq-dia-server-accepted';
import FilterSize from '../logic/filter-size';
import ComponentSeqDiaShared from './components/component-seq-dia-shared';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerAccepted extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerAccepted, 'seq_dia_server_accepted', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const connectedCircleSmallTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectedCircleSmallTemplate.classList.add('seq_dia_client_connected');
    connectedCircleSmallTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    connectedCircleSmallTemplate.setAttribute('r', 3.5);
    connectedCircleSmallTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(connectedCircleSmallTemplate);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [fragment, fragment, fragment, fragment, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerAccepted.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
  
  onCloneToSvg(localNode, remoteNode, data) {
    const reverseDirection = data.reverseDirection ? 1 : 0;
    if(data.localIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      }
    }
    if(data.remoteIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection)); 
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection));
      }
    }
  }
}


module.exports = TemplateSeqDiaServerAccepted;
