
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerAccepting from '../data/data-seq-dia-server-accepting';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerAccepting extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerAccepting, 'seq_dia_server_accepting', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const connectingCircleTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectingCircleTemplate.classList.add('seq_dia_client_connecting');
    connectingCircleTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    connectingCircleTemplate.setAttribute('r', 2);
    connectingCircleTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(connectingCircleTemplate);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [null, null, fragment, null, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerAccepting.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerAccepting;
