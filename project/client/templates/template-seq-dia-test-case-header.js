
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaEventRow from './components/component-seq-dia-event-row';
import DataSeqDiaTestCaseHeader from '../data/data-seq-dia-test-case-header';
import FilterSize from '../logic/filter-size';


class TemplateSeqDiaTestCaseHeader {
  constructor() {
    this.filter = null;
    this.divTemplate = null;
  }
  
  init(filter) {
    this.filter = filter;
    
    this.divTemplate = document.createDocumentFragment();
    const divText = this.divTemplate.appendChild(document.createElement('div'));
    divText.classList.add('test_case_header_text');
    divText.appendChild(document.createTextNode('[actor]'));
    
    const divActorLine = this.divTemplate.appendChild(document.createElement('div'));
    divActorLine.classList.add('test_case_header_line');
  }
  
  calculateHeight(buffer) {
    return Const.HEADER_Y_HEIGHT;
  }
    
  store(msg, sharedTemplateData) {
    return DataSeqDiaTestCaseHeader.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return DataSeqDiaTestCaseHeader.restore(buffer);
  }
  
  create(data) {
    const actorPhases = data.actorPhases;
    const actorNames = data.actorNames;
    const node = ComponentSeqDiaEventRow.cloneRowHeader('seq_dia_test_case_header');
    node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnHeaderLeft());
    for(let i = 0; i < actorPhases.length; ++i) {
      const innerDivNode = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnHeader(i));
      innerDivNode.appendChild(this.divTemplate.cloneNode(true));
      innerDivNode.firstChild.firstChild.nodeValue = actorNames[i];
    }
    node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnHeaderRight(actorPhases.length));
    return node;
  }
  
  destroy(node) {}
}


module.exports = TemplateSeqDiaTestCaseHeader;
