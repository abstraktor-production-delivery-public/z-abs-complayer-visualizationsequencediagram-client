
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaEventInfo from './components/component-seq-dia-event-info';
import ComponentSeqDiaEventRow from './components/component-seq-dia-event-row';
import DataSeqDiaGui from '../data/data-seq-dia-gui';
import FilterSize from '../logic/filter-size';
import ActorPhaseConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const';
import LogDataGuiSubType from 'z-abs-funclayer-engine-cs/clientServer/log/log-data-gui-sub-type';
import LogDataGuiType from 'z-abs-funclayer-engine-cs/clientServer/log/log-data-gui-type';


class TemplateSeqDiaGui {
  constructor() {
    this.filter = null;
    this.eventInfoIds = null;
    this.template = null;
    this.eventInfoId = -1;
  }
  
  init(filter) {
    this.filter = filter;
    const length = LogDataGuiType.results.length;
    this.template = new Array(length);

    for(let i = 0; i < length; ++i) {
      const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
      this.template[i] = g
      switch(i) {
        case LogDataGuiType.NEW_BROWSER:
        case LogDataGuiType.USE_BROWSER:
          g.appendChild(this._browser(i));
          break;
        case LogDataGuiType.NEW_PAGE:
        case LogDataGuiType.USE_PAGE:
          g.appendChild(this._newPage(i));
          break;
        case LogDataGuiType.ACTION_CLICK:
          g.appendChild(this._click());
          break;
        case LogDataGuiType.ACTION_TYPE:
          g.appendChild(this._type());
          break;
        case LogDataGuiType.FUNCTION_CALL:
          g.appendChild(this._functionCall());
          break;
      }
    }
    
    const leftRow = ComponentSeqDiaEventInfo.createLeftRowDiv('seq_dia__data_gui');
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, '', 'seq_dia__data_gui_info', false);
    const rightRow = ComponentSeqDiaEventInfo.createRightRowDiv('seq_dia__data_gui');
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '', 'seq_dia__data_gui_instance', false);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '${prot}', 'seq_dia__data_gui_protocol', true);
    
    this.eventInfoId = ComponentSeqDiaEventInfo.createTemplate(leftRow, rightRow);
    
    /*
    this.divTemplate = new Array(ActorPhaseConst.namesRunning.length);
    this.middleTemplates = new Array(ActorPhaseConst.namesRunning.length);
    
    const divTemplate = document.createElement('div');
    
    const gTop = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTop.setAttribute('transform', `translate(${FilterSize.eventWidthHalf} 0)`);
    const gInfo = gTop.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    gInfo.classList.add('seq_dia_show__gui_event_info');
    
    const stateLine = gInfo.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    stateLine.classList.add('gui_seq_dia_svg');
    stateLine.setAttribute('x1', -1.5);
    stateLine.setAttribute('x2', -8);
    stateLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    stateLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    
    const resultRectNode = gInfo.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'rect'));
    resultRectNode.classList.add('gui_seq_dia_svg');
    resultRectNode.setAttribute('x', 0);
    resultRectNode.setAttribute('width', 40);
    resultRectNode.setAttribute('rx', 2);
    resultRectNode.setAttribute('y', 0.5);
    resultRectNode.setAttribute('height', 9);
    resultRectNode.setAttribute('ry', 2);
    
    const resultTextNode = gInfo.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
    resultTextNode.classList.add('gui_seq_dia_svg');
    resultTextNode.setAttribute('x', -9);
    resultTextNode.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 3.5);
    resultTextNode.appendChild(document.createTextNode('[result]'));
    
    const gData = gTop.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    gData.classList.add('seq_dia_show__gui_event_data');
    
    const dataLine = gData.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    dataLine.classList.add('gui_seq_dia_svg');
    dataLine.setAttribute('x1', 1.5);
    dataLine.setAttribute('x2', 8);
    dataLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    dataLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    
    const dataRect = gData.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'rect'));
    dataRect.classList.add('gui_seq_dia_svg');
    dataRect.setAttribute('x', 8);
    dataRect.setAttribute('width', 40);
    dataRect.setAttribute('rx', 2);
    dataRect.setAttribute('y', 0.5);
    dataRect.setAttribute('height', 9);
    dataRect.setAttribute('ry', 2);
    
    const dataText = gData.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
    dataText.classList.add('gui_seq_dia_data_svg');
    dataText.setAttribute('x', 9);
    dataText.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 3.5);
    dataText.appendChild(document.createTextNode('[result]'));
    
    ActorPhaseConst.namesRunning.forEach((phase, indexPhase) => {
      this.divTemplate[indexPhase] = new Array(LogDataGuiType.results.length);
      this.middleTemplates[indexPhase] = new Array(LogDataGuiType.results.length);
      LogDataGuiType.results.forEach((result, index) => {
        const divNode = divTemplate.cloneNode(true);
        divNode.classList.add('seq_dia_event');
        divNode.classList.add(`${Const.phasePrefixClasses[indexPhase]}gui_event_${LogDataGuiType.subTypeNames[index]}`);
        this.divTemplate[indexPhase][index] = divNode;
        this.middleTemplates[indexPhase][index] = new Array(3);
        for(let i = 0; i < 3; ++i) {
          const middleNode = this.seqDiaEventTemplate.cloneStatement(i, indexPhase);
          const middleSvg = this.seqDiaEventTemplate.getMiddleSvg(middleNode);
          const clonedGTop = middleSvg.appendChild(gTop.cloneNode(true));
          switch(index) {
            case LogDataGuiType.NEW_BROWSER:
            case LogDataGuiType.USE_BROWSER:
              middleSvg.childNodes[1].appendChild(this._browser(index));
              break;
            case LogDataGuiType.USE_PAGE:
              middleSvg.childNodes[1].appendChild(this._newPage());
              break;
            case LogDataGuiType.NEW_PAGE:
              middleSvg.childNodes[1].appendChild(this._newPage());
              break;
            case LogDataGuiType.CLICK:
              middleSvg.childNodes[1].appendChild(this._click());
              break;
            case LogDataGuiType.TYPE:
              middleSvg.childNodes[1].appendChild(this._type());
              const rLine = clonedGTop.firstChild.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
              rLine.classList.add('gui_seq_dia_svg');
              rLine.setAttribute('x1', -1.5);
              rLine.setAttribute('x2', -8);
              rLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
              rLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
              break;
            case LogDataGuiType.NOT_IMPL:
              middleSvg.childNodes[1].appendChild(this._notImplemented());
              break;
          }
          const infoG = middleSvg.lastChild.firstChild;
          infoG.firstChild.setAttribute('x1', -1.5 - LogDataGuiType.templateWidths[index][1]);
          infoG.firstChild.setAttribute('x2', -8 - LogDataGuiType.templateWidths[index][1]);
          infoG.childNodes[1].setAttribute('width', LogDataGuiType.templateWidths[index][0]);
          infoG.childNodes[1].setAttribute('x', -8 - LogDataGuiType.templateWidths[index][0] - LogDataGuiType.templateWidths[index][1]);
          infoG.childNodes[2].setAttribute('x', -9 - LogDataGuiType.templateWidths[index][1]);
          infoG.childNodes[2].firstChild.nodeValue = result;
          this.middleTemplates[indexPhase][index][i] = middleNode;
        }
      });
    });*/
  }
  
  calculateHeight(buffer) {
    const data = DataSeqDiaGui.restoreHeightData(buffer);
    return this.filter.getGuiEvents(data.phaseId, LogDataGuiSubType.subTypes[data.type]) ? Const.STATE_EVENT_HEIGHT : 0;
  }
  
  store(msg, sharedTemplateData) {
    return DataSeqDiaGui.store(msg, sharedTemplateData);
  }
  
  restore(buffer) {
    return DataSeqDiaGui.restore(buffer);
  }
  
  create(data) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const actorIndex = data.actorIndex;
    const phaseId = data.phaseId;
    const protocolIndex = data.protocolIndex;
    const type = data.type;
    const stackData = data.stackData;
    const node = ComponentSeqDiaEventRow.cloneRow('seq_dia_stack');
    const divNodeLeft = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnLeft(phaseId));
    for(let i = 0; i < actorPhases.length; ++i) {
      const divNodeMiddle = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnMiddle(i, phaseId, actorParts[i]));
      if(actorIndex === i) {
        const svg = divNodeMiddle.appendChild(ComponentSeqDiaEventRow.cloneSvg(Const.STATE_EVENT_HEIGHT, Const.RENDER_POSITION_NORMAL));
        svg.appendChild(this.template[type].cloneNode(true));
      }
    }
    const divNodeRight = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnRight(actorPhases.length, phaseId));
    
    const leftNode = ComponentSeqDiaEventInfo.clone(this.eventInfoId, Const.PART_LEFT, protocolIndex);
    const parentGuiOutputNode = leftNode.childNodes[0].firstChild;
    parentGuiOutputNode.replaceChild(document.createTextNode(data.data.name), parentGuiOutputNode.firstChild);
    node.firstChild.childNodes[data.actorIndex + 1].appendChild(leftNode);
    
    const rightNode = ComponentSeqDiaEventInfo.clone(this.eventInfoId, Const.PART_RIGHT, protocolIndex);
    const parentGuiInstanceNode = rightNode.childNodes[0].firstChild;
    parentGuiInstanceNode.replaceChild(document.createTextNode(stackData), parentGuiInstanceNode.firstChild);
    node.firstChild.childNodes[data.actorIndex + 1].appendChild(rightNode);
    
    return node;
  }
  
  destroy(node) {}
  
  /*create(dataSeqDiaGui) {
    const actorPhases = dataSeqDiaGui.dataTestCase.actorPhases;
    const actorParts = dataSeqDiaGui.dataTestCase.actorParts;
    const actorIndex = dataSeqDiaGui.dataLogRow.actorIndex;
    const phaseId = dataSeqDiaGui.dataLogRow.phaseId;
    const dataType = dataSeqDiaGui.dataType;
    const node = this.divTemplate[phaseId][dataType].cloneNode(true);
    const statementLeftNode = this.seqDiaEventTemplate.cloneStatement(Const.PART_LEFT, phaseId);
    node.appendChild(statementLeftNode);
    for(let i = 0; i < actorPhases.length; ++i) {
      if(actorIndex === i) {
        const middleNode = node.appendChild(this.middleTemplates[phaseId][dataType][actorParts[i]].cloneNode(true));
        const middleSvg = this.seqDiaEventTemplate.getMiddleSvg(middleNode);
        const gTopNode = middleSvg.lastChild;
        const gInfoNode = gTopNode.firstChild;
        const gDataNode = middleSvg.childNodes[1].childNodes[1];
        dataSeqDiaGui.calcParams.push(dataType, LogDataGuiType.subTypes[dataType], gInfoNode, gDataNode);
        if(LogDataGuiType.NEW_BROWSER === dataType || LogDataGuiType.USE_BROWSER === dataType) {
          gDataNode.childNodes[2].innerHTML = dataSeqDiaGui.dataBrowser;
        }
        else if(LogDataGuiType.NEW_PAGE === dataType || LogDataGuiType.USE_PAGE === dataType) {
          gDataNode.childNodes[2].innerHTML = dataSeqDiaGui.page;
        }
        else if(LogDataGuiType.CLICK === dataType) {
          gDataNode.childNodes[2].innerHTML = dataSeqDiaGui.dataArgs;
        }
        else if(LogDataGuiType.TYPE === dataType) {
          gDataNode.childNodes[2].innerHTML = dataSeqDiaGui.dataArgs;
        }
        else if(LogDataGuiType.NOT_IMPL === dataType) {
          gInfoNode.childNodes[2].innerHTML = dataSeqDiaGui.data.name;
          gDataNode.parentNode.removeChild(gDataNode);
        }
      }
      else {
        const statementMiddleNode = this.seqDiaEventTemplate.cloneStatement(actorParts[i], phaseId);
        node.appendChild(statementMiddleNode);
      }
    }
    const statementRightNode = this.seqDiaEventTemplate.cloneStatement(Const.PART_RIGHT, phaseId);
    node.appendChild(statementRightNode);
    return node;
  }*/
      
  _browser(type) {
    const gTransform = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTransform.setAttribute('transform', `translate(${FilterSize.eventWidthHalf - 5}) scale(0.1 0.1)${LogDataGuiType.NEW_BROWSER === type ? '' : ' rotate(45 38 46)'}`);
    const path = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path.classList.add('gui_seq_dia_browser_svg');
    path.setAttribute('d', 'M 84.24 34.662 c 5.993 -13.379 7.386 -25.143 3.819 -32.273 c -0.097 -0.193 -0.254 -0.35 -0.447 -0.447 C 80.481 -1.626 68.717 -0.234 55.337 5.76 c -0.018 0.008 -0.033 0.021 -0.05 0.03 c -6.605 2.964 -13.045 6.812 -18.59 11.048 l -12.576 0.306 c -1.434 0.035 -2.838 0.449 -4.061 1.197 L 2.531 29.06 c -1.463 0.894 -2.359 2.452 -2.397 4.167 c -0.038 1.715 0.79 3.311 2.212 4.268 l 13.132 8.839 l -2.22 10.701 c -0.068 0.33 0.034 0.672 0.272 0.91 L 32.056 76.47 c 0.189 0.189 0.444 0.293 0.707 0.293 c 0.067 0 0.136 -0.007 0.203 -0.021 l 10.693 -2.218 l 8.817 13.101 c 0.937 1.392 2.484 2.213 4.157 2.213 c 0.036 0 0.074 0 0.111 -0.001 c 1.714 -0.037 3.273 -0.934 4.168 -2.397 l 10.717 -17.53 c 0.748 -1.221 1.161 -2.625 1.197 -4.061 l 0.305 -12.506 z');
    
    const circle = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
    circle.setAttribute('cx', 49);
    circle.setAttribute('cy', 40);
    circle.setAttribute('r', 15);
    circle.setAttribute('style', 'fill:White;');
   
    const line1 = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    line1.classList.add('gui_seq_dia_browser_svg_fuel');
    line1.setAttribute('x1', 20);
    line1.setAttribute('x2', 0);
    line1.setAttribute('y1', 58);
    line1.setAttribute('y2', 78);
    
    const line2 = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    line2.classList.add('gui_seq_dia_browser_svg_fuel');
    line2.setAttribute('x1', 26);
    line2.setAttribute('x2', 0);
    line2.setAttribute('y1', 64);
    line2.setAttribute('y2', 90);
    
    const line3 = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    line3.classList.add('gui_seq_dia_browser_svg_fuel');
    line3.setAttribute('x1', 32);
    line3.setAttribute('x2', 12);
    line3.setAttribute('y1', 70);
    line3.setAttribute('y2', 90);
    return gTransform;
  }
  
  _newPage(type) {
    const gTransform = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTransform.setAttribute('transform', `translate(${FilterSize.eventWidthHalf - 4}) scale(0.018 0.018)`);
    
    const path1 = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path1.classList.add('gui_seq_dia_page');
    path1.setAttribute('d', 'm285.37 85.136 1.3221-76.683 101.47 101.8-80.98-1.3221z');
    path1.setAttribute('style', 'fill:#8fbcef;');
    
    const path2 = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path2.setAttribute('d', 'm5.742 26.302 18.84-16.527 262.11-1.3221-1.3221 76.683 21.815 23.798 80.98 1.3221-0.99159 368.21-17.518 18.675-338.79 4.1316-23.137-20.823z');
    path2.setAttribute('style', 'fill:White');
    
    const path3 = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path3.classList.add('gui_seq_dia_page');
    path3.setAttribute('d', 'm398.96 110.75c-0.018-0.185-0.05-0.365-0.081-0.545-0.011-0.06-0.016-0.122-0.028-0.182-0.043-0.215-0.098-0.425-0.159-0.632-7e-3 -0.025-0.012-0.052-0.02-0.077-0.065-0.213-0.141-0.421-0.224-0.625-8e-3 -0.021-0.015-0.043-0.023-0.064-0.081-0.195-0.173-0.384-0.269-0.57-0.016-0.031-0.029-0.063-0.045-0.094-0.093-0.173-0.196-0.339-0.301-0.504-0.027-0.042-0.05-0.086-0.077-0.127-0.103-0.154-0.216-0.3-0.33-0.446-0.037-0.048-0.07-0.098-0.109-0.145-0.142-0.173-0.294-0.338-0.45-0.498-0.015-0.015-0.027-0.031-0.042-0.046l-104-104c-0.018-0.018-0.038-0.033-0.057-0.051-0.156-0.153-0.317-0.301-0.486-0.44-0.055-0.045-0.113-0.083-0.169-0.126-0.138-0.107-0.275-0.214-0.42-0.311-0.051-0.034-0.105-0.062-0.156-0.095-0.156-0.099-0.312-0.197-0.475-0.284-0.036-0.019-0.074-0.035-0.111-0.053-0.181-0.093-0.365-0.183-0.554-0.262-0.024-0.01-0.049-0.017-0.074-0.027-0.202-0.081-0.406-0.157-0.616-0.221-0.027-8e-3 -0.054-0.013-0.081-0.021-0.206-0.06-0.415-0.115-0.628-0.158-0.063-0.013-0.128-0.018-0.192-0.029-0.177-0.031-0.354-0.062-0.536-0.08-0.248-0.025-0.498-0.038-0.749-0.038h-248c-21.78 0-39.5 17.72-39.5 39.5v432c0 21.78 17.72 39.5 39.5 39.5h320c21.78 0 39.5-17.72 39.5-39.5v-360c0-0.251-0.013-0.501-0.038-0.749zm-103.96-85.145 78.394 78.394h-53.894c-13.509 0-24.5-10.99-24.5-24.5zm64.5 470.39h-320c-13.509 0-24.5-10.99-24.5-24.5v-432c0-13.51 10.991-24.5 24.5-24.5h240.5v64.5c0 21.78 17.72 39.5 39.5 39.5h64.5v352.5c0 13.51-10.991 24.5-24.5 24.5z');
    path3.setAttribute('style', 'fill:#227ce0;');
    
    const g = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    g.setAttribute('transform', 'translate(-55.998 .001)');
    
    const pathG1 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    pathG1.setAttribute('d', 'm210.79 328.21v33.164l-117.89-51.094v-28.359l117.89-51.094v33.398l-82.148 31.641z');
    
    const pathG2 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    pathG2.setAttribute('d', 'm283.68 230.12-32.812 135.12q-1.6406 6.6797-2.9297 10.547-1.1719 3.8672-3.75 5.9766-2.4609 2.2266-7.2656 2.2266-11.953 0-11.953-10.312 0-2.6953 2.3438-13.477l32.695-135.12q2.5781-10.781 4.9219-14.766 2.3438-3.9844 9.1406-3.9844 5.8594 0 8.9062 2.8125 3.1641 2.8125 3.1641 7.7344 0 3.6328-2.4609 13.242z');
    
    const pathG3 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    pathG3.setAttribute('d', 'm418.1 310.28-117.89 51.328v-33.164l82.383-32.344-82.383-31.875v-32.93l117.89 50.859z');
    
    return gTransform;
  }
  
  _click() {
    const gTransform = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTransform.setAttribute('transform', `translate(${FilterSize.eventWidthHalf - 11.2} 1.0) scale(0.14 0.12)`);
    
    const circle = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
    circle.setAttribute('cx', 80);
    circle.setAttribute('cy', 20);
    circle.setAttribute('r', 15);
    circle.setAttribute('style', 'fill:forestgreen;');
    
    const g = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    g.setAttribute('transform', 'translate(35 -35)');
    
    const path = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path.setAttribute('d', 'M 66.437 24.873 c 0.382 -1.923 -0.02 -3.815 -1.129 -5.329 c -1.108 -1.514 -2.791 -2.467 -4.739 -2.685 c -1.918 -0.215 -3.853 0.314 -5.455 1.487 l -32.62 23.888 l 0.161 -8.456 c 0.036 -1.889 -0.682 -3.638 -2.023 -4.924 c -1.388 -1.332 -3.25 -1.999 -5.258 -1.878 c -4.003 0.242 -7.319 3.62 -7.394 7.529 L 7.399 65.128 C 7.276 70.599 8.867 75.753 12 80.031 c 4.799 6.553 12.408 9.97 20.359 9.969 c 5.88 0 11.948 -1.868 17.205 -5.718 l 13.652 -9.998 c 3.344 -2.45 4.212 -6.972 1.935 -10.081 c -1.108 -1.513 -2.791 -2.467 -4.739 -2.684 c -0.013 -0.001 -0.025 -0.001 -0.038 -0.002 l 0.359 -0.263 c 1.601 -1.173 2.69 -2.86 3.064 -4.751 c 0.382 -1.922 -0.02 -3.816 -1.129 -5.329 c -1.108 -1.513 -2.791 -2.467 -4.739 -2.685 c -0.351 -0.04 -0.702 -0.046 -1.052 -0.036 c 0.446 -0.746 0.771 -1.56 0.942 -2.42 c 0.381 -1.922 -0.021 -3.815 -1.129 -5.329 c -1.154 -1.576 -2.899 -2.488 -4.774 -2.689 l 11.458 -8.391 C 64.974 28.451 66.063 26.764 66.437 24.873');
    path.setAttribute('style', 'stroke:Black;stroke-width:4;stroke-dasharray:none;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;fill:forestgreen;fill-rule:nonzero;opacity:1;stroke-linecap:round;transform:rotate(45deg)');
    
    return gTransform;
  }
  
  _type() {
    const gTransform = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTransform.setAttribute('transform', `translate(${FilterSize.eventWidthHalf - 14.2} -1) scale(0.023 0.023)`);
    
    const background = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'rect'));
    background.classList.add('gui_seq_dia_type');
    background.setAttribute('x', 0);
    background.setAttribute('width', 480);
    background.setAttribute('rx', 2);
    background.setAttribute('y', 200);
    background.setAttribute('height', 200);
    background.setAttribute('ry', 2);
   
    const path = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path.setAttribute('d', 'M251.2,193.5v-53.7c0-5.8,4.7-10.5,10.5-10.5h119.4c21,0,38.1-17.1,38.1-38.1s-17.1-38.1-38.1-38.1H129.5 c-5.4,0-10.1,4.3-10.1,10.1c0,5.8,4.3,10.1,10.1,10.1h251.6c10.1,0,17.9,8.2,17.9,17.9c0,10.1-8.2,17.9-17.9,17.9H261.7 c-16.7,0-30.3,13.6-30.3,30.3v53.3H0v244.2h490V193.5H251.2z M232.2,221.5h15.6c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1 h-15.6c-5.4,0-10.1-4.3-10.1-10.1C222.1,225.8,226.7,221.5,232.2,221.5z M203.4,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C213.5,321,208.8,325.7,203.4,325.7z M213.5,352.9 c0,5.4-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6C208.8,342.8,213.5,347.5,213.5,352.9z M203.4,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C213.5,283.7,208.8,288,203.4,288z M186.3,221.5h15.6c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1h-15.6 c-5.4,0-10.1-4.3-10.1-10.1S180.8,221.5,186.3,221.5z M140.4,221.5H156c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1h-15.6 c-5.4,0-10.1-4.3-10.1-10.1C130.3,225.8,134.9,221.5,140.4,221.5z M138.8,268.1h15.6c5.4,0,10.1,4.3,10.1,10.1 c0,5.8-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1C128.7,272.4,133.4,268.1,138.8,268.1z M138.8,305.5h15.6 c5.4,0,10.1,4.3,10.1,10.1c0,5.4-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1C128.7,310.1,133.4,305.5,138.8,305.5z M138.8,342.8h15.6c5.4,0,10.1,4.3,10.1,10.1c0,5.4-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1 C128.7,347.5,133.4,342.8,138.8,342.8z M94.5,221.5h15.6c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1H94.5 c-5.4,0-10.1-4.3-10.1-10.1S89.1,221.5,94.5,221.5z M89.4,268.1H105c5.4,0,10.1,4.3,10.1,10.1c0,5.8-4.3,10.1-10.1,10.1H89.4 c-5.4,0-10.1-4.3-10.1-10.1C79.3,272.4,84,268.1,89.4,268.1z M89.4,305.5H105c5.4,0,10.1,4.3,10.1,10.1c0,5.4-4.3,10.1-10.1,10.1 H89.4c-5.4,0-10.1-4.3-10.1-10.1C79.7,310.1,84,305.5,89.4,305.5z M89.4,342.8H105c5.4,0,10.1,4.3,10.1,10.1 c0,5.4-4.3,10.1-10.1,10.1H89.4c-5.4,0-10.1-4.3-10.1-10.1C79.7,347.5,84,342.8,89.4,342.8z M56,400.4H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C65.7,395.7,61.4,400.4,56,400.4z M56,363H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C65.7,358.4,61.4,363,56,363z M56,325.7H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C65.7,321,61.4,325.7,56,325.7z M56,288H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C66.1,283.7,61.4,288,56,288z M56,241.3H40.4 c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1S61.4,241.3,56,241.3z M252.8,400.4H89.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h163.3c5.4,0,10.1,4.3,10.1,10.1C262.9,395.7,258.2,400.4,252.8,400.4z M252.8,363h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C262.9,358.4,258.2,363,252.8,363z M252.8,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C262.9,321,258.2,325.7,252.8,325.7z M252.8,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C262.9,283.7,258.2,288,252.8,288z M302.2,400.4h-15.6 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C311.9,395.7,307.6,400.4,302.2,400.4z M302.2,363h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C311.9,358.4,307.6,363,302.2,363z M302.2,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C311.9,321,307.6,325.7,302.2,325.7z M302.2,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C312.3,283.7,307.6,288,302.2,288z M312.3,241.3h-15.6 c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1S317.7,241.3,312.3,241.3z M351.2,400.4h-15.6 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C361.3,395.7,356.6,400.4,351.2,400.4z M351.2,363h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C361.3,358.4,356.6,363,351.2,363z M351.2,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C361.3,321,356.6,325.7,351.2,325.7z M351.2,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C361.3,283.7,356.6,288,351.2,288z M357.8,241.3h-15.6 c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1S363.6,241.3,357.8,241.3z M400.6,400.4H385 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C410.3,395.7,406,400.4,400.6,400.4z M400.6,363H385c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C410.3,358.4,406,363,400.6,363z M400.6,325.7H385c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C410.3,321,406,325.7,400.6,325.7z M400.6,288H385c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1 h15.6c5.4,0,10.1,4.3,10.1,10.1C410.7,283.7,406,288,400.6,288z M403.7,241.3h-15.6c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1 h15.6c5.4,0,10.1,4.3,10.1,10.1C413.8,237,409.5,241.3,403.7,241.3z M449.6,400.4H434c-5.4,0-10.1-4.3-10.1-10.1 c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C459.7,395.7,455,400.4,449.6,400.4z M449.6,363H434 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C459.7,358.4,455,363,449.6,363z M449.6,325.7 H434c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C459.7,321,455,325.7,449.6,325.7z M449.6,288H434c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C459.7,283.7,455,288,449.6,288z M449.6,241.3H434c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 S455,241.3,449.6,241.3z');
    
    /*const rLine = clonedGTop.firstChild.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
          rLine.classList.add('gui_seq_dia_svg');
          rLine.setAttribute('x1', -1.5);
          rLine.setAttribute('x2', -8);
          rLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
          rLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);*/
    return gTransform;
  }
  
  _functionCall() {
    const gTransform = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTransform.setAttribute('transform', `translate(${FilterSize.eventWidthHalf})`);
    
    const rect = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'rect'));
    rect.classList.add('gui_seq_dia_function');
    rect.setAttribute('y', 1);
    rect.setAttribute('height', Const.STATE_EVENT_HEIGHT - 2);
    rect.setAttribute('x', -5);
    rect.setAttribute('width', 9);
    
    const text = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'text'));
    text.classList.add('gui_seq_dia_function');
    text.setAttribute('x', 4);
    text.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT + 2.5);
    text.appendChild(document.createTextNode('f()'));
    
    return gTransform;
  }
}


module.exports = TemplateSeqDiaGui;

