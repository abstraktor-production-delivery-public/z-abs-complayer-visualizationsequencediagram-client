
'use strict';

import Const from '../logic/const';
import DataSeqDiaConnectionClosed from '../data/data-seq-dia-connection-closed';
import FilterSize from '../logic/filter-size';
import ComponentSeqDiaShared from './components/component-seq-dia-shared';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaConnectionClosed extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaConnectionClosed, 'seq_dia_connection_closed', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const closingLine1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine1.classList.add('seq_dia_event_connection_closed');
    closingLine1.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine1.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine1.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine1.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    const closingLine2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    closingLine2.classList.add('seq_dia_event_connection_closed');
    closingLine2.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT + 3);
    closingLine2.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT - 3);
    closingLine2.setAttribute('x1', FilterSize.eventWidthHalf - 3);
    closingLine2.setAttribute('x2', FilterSize.eventWidthHalf + 3);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(closingLine1.cloneNode(true));
    this.fragment.appendChild(closingLine2.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    return [fragment, fragment, fragment, fragment, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaConnectionClosed.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
  
  onCloneToSvg(localNode, remoteNode, data) {
    const reverseDirection = data.reverseDirection ? 1 : 0;
    if(data.localIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      }
    }
    if(data.remoteIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection)); 
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection));
      }
    }
  }
}


module.exports = TemplateSeqDiaConnectionClosed;
