
'use strict';

import Const from '../logic/const';
import ComponentSeqDiaEventInfo from './components/component-seq-dia-event-info';
import DataSeqDiaClientConnecting from '../data/data-seq-dia-client-connecting';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaClientConnecting extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaClientConnecting, 'seq_dia_server_connecting', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
    this.eventInfoId = -1;
  }
  
  onInit() {
    const connectingCircleTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectingCircleTemplate.classList.add('seq_dia_client_connecting');
    connectingCircleTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    connectingCircleTemplate.setAttribute('r', 2);
    connectingCircleTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(connectingCircleTemplate);
    
    const leftRow = ComponentSeqDiaEventInfo.createLeftRowDiv('seq_dia__data_protocol');
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, '', 'seq_dia__data_protocol_ip', false);
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, '', 'seq_dia__data_protocol_address_name', false);
    
    const rightRow = ComponentSeqDiaEventInfo.createRightRowDiv('seq_dia__data_protocol');
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '', 'seq_dia__data_protocol_ip', false);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '', 'seq_dia__data_protocol_address_name', false);
    
    this.eventInfoId = ComponentSeqDiaEventInfo.createTemplate(leftRow, rightRow);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [fragment, null, fragment, null, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaClientConnecting.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
  
  onCloneInfo(node, data) {
    const dataNode = ComponentSeqDiaEventInfo.clone(this.eventInfoId, Const.PART_RIGHT, data.protocolIndex);
    const parentIpNode = dataNode.childNodes[0].firstChild;
    parentIpNode.replaceChild(document.createTextNode(data.extraData.remoteIp), parentIpNode.firstChild);
    const parentNameNode = dataNode.childNodes[1].firstChild;
    parentNameNode.replaceChild(document.createTextNode(data.extraData.remoteName), parentNameNode.firstChild);
    node.appendChild(dataNode);
  }
}


module.exports = TemplateSeqDiaClientConnecting;
