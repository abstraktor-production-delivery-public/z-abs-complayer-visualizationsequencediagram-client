
'use strict';

import Const from '../logic/const';
import DataSeqDiaClientConnected from '../data/data-seq-dia-client-connected';
import FilterSize from '../logic/filter-size';
import ComponentSeqDiaShared from './components/component-seq-dia-shared';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaClientConnected extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaClientConnected, 'seq_dia_client_connected', Const.STATE_EVENT_HEIGHT);
    this.fragmentEast = null;
    this.fragmentWest = null;
  }
  
  onInit() {
    const connectedCircleSmallTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectedCircleSmallTemplate.classList.add('seq_dia_client_connected');
    connectedCircleSmallTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    connectedCircleSmallTemplate.setAttribute('r', 3.5);
    connectedCircleSmallTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    this.fragmentEast = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragmentEast.appendChild(connectedCircleSmallTemplate);
    
    this.fragmentWest = this.fragmentEast.cloneNode(true);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragmentEast = this.fragmentEast.cloneNode(true);
    fragmentEast.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    const fragmentWest = this.fragmentWest.cloneNode(true);
    fragmentWest.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [fragmentEast, fragmentWest, fragmentWest, fragmentEast, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaClientConnected.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
  
  onCloneToSvg(localNode, remoteNode, data) {
    const reverseDirection = data.reverseDirection ? 1 : 0;
    if(data.localIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      }
    }
    if(data.remoteIsShared) {
      localNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection)); 
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection));
      }
    }
  }
}


module.exports = TemplateSeqDiaClientConnected;
