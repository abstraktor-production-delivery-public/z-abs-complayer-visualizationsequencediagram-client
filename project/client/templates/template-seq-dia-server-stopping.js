
'use strict';

import Const from '../logic/const';
import DataSeqDiaServerStopping from '../data/data-seq-dia-server-stopping';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaServerStopping extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaServerStopping, 'seq_dia_server_stopping', Const.STATE_EVENT_HEIGHT);
    this.fragment = null;
  }
  
  onInit() {
    const startRect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    startRect.setAttribute('y', Const.STATE_EVENT_HALF_HEIGHT - 3);
    startRect.setAttribute('height', 6);
    startRect.setAttribute('x', FilterSize.eventWidthHalf - 3);
    startRect.setAttribute('width', 6);
    
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(startRect);
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.lastChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [null, null, fragment, null, null];
  }
    
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaServerStopping.restoreHeightData(buffer);
    return this.filter.getServerEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaServerStopping;
