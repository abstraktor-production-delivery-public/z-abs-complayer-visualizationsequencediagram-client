
'use strict';

import Const from '../logic/const';
import DataSeqDiaConnectionUpgrading from '../data/data-seq-dia-connection-upgrading';
import FilterSize from '../logic/filter-size';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaConnectionUpgraded extends TemplateSeqDiaProtocolBase {
  constructor() {
    super(DataSeqDiaConnectionUpgrading);
    this.fragment = null;
  }
  
  onInit(appDeserializer) {
    DataSeqDiaConnectionUpgrading.init(appDeserializer);
    const gTop = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    gTop.setAttribute('transform', `translate(${FilterSize.eventWidthHalf} 0)`);
    
    const connectedCircleSmallTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    connectedCircleSmallTemplate.classList.add('seq_dia_client_connected');
    connectedCircleSmallTemplate.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT);
    connectedCircleSmallTemplate.setAttribute('r', 4);
    connectedCircleSmallTemplate.setAttribute('cx', FilterSize.eventWidthHalf);
    
    const gTransform = gTop.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    gTransform.setAttribute('transform', 'translate(-3.2 1.6) scale(0.019 0.019)');
    
    const path = gTransform.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'path'));
    path.setAttribute('d', 'M32,21.9h0Z');
    path.setAttribute('d', 'M65,330h200c8.284,0,15-6.716,15-15V145c0-8.284-6.716-15-15-15h-15V85c0-46.869-38.131-85-85-85 S80,38.131,80,85v45H65c-8.284,0-15,6.716-15,15v170C50,323.284,56.716,330,65,330z M180,234.986V255c0,8.284-6.716,15-15,15 s-15-6.716-15-15v-20.014c-6.068-4.565-10-11.824-10-19.986c0-13.785,11.215-25,25-25s25,11.215,25,25 C190,223.162,186.068,230.421,180,234.986z M110,85c0-30.327,24.673-55,55-55s55,24.673,55,55v45H110V85z');
        
    this.fragment = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragment.appendChild(connectedCircleSmallTemplate.cloneNode(true));
    this.fragment.appendChild(gTop.cloneNode(true));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragment = this.fragment.cloneNode(true);
    fragment.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
    return [fragment, fragment, fragment, fragment, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaConnectionUpgrading.restoreHeightData(buffer);
    return this.filter.getConnectionEvents(phaseId) ? this.height : 0;
  }
}


module.exports = TemplateSeqDiaConnectionUpgraded;
