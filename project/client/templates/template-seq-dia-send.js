
'use strict';

import Const from '../logic/const';
import DataSeqDiaSend from '../data/data-seq-dia-send';
import FilterSize from '../logic/filter-size';
import ComponentSeqDiaArrow from './components/component-seq-dia-arrow';
import ComponentSeqDiaCaption from './components/component-seq-dia-caption';
import ComponentSeqDiaEnvelope from './components/component-seq-dia-envelope';
import ComponentSeqDiaShared from './components/component-seq-dia-shared';
import TemplateSeqDiaProtocolBase from './template-seq-dia-protocol-base';


class TemplateSeqDiaSend extends TemplateSeqDiaProtocolBase {
  static ARROW_HEIGHT = 3;
  
  constructor() {
    super(DataSeqDiaSend, 'seq_dia_send', 20);
    this.fragmentEast = null;
    this.fragmentWest = null;
    this.eventInfoId = -1;
  }
  
  onInit() {
    this.fragmentEast = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.fragmentWest = this.fragmentEast.cloneNode(true);
    
    const eastLine = this.fragmentEast.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    eastLine.setAttribute('x1', Const.LINE_WIDTH_HALF + FilterSize.eventWidthHalf);
    eastLine.setAttribute('x2', FilterSize.eventWidth - 7);
    eastLine.setAttribute('y1', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    eastLine.setAttribute('y2', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    
    this.fragmentEast.appendChild(ComponentSeqDiaArrow.createEast(FilterSize.eventWidth));
    this.fragmentEast.appendChild(ComponentSeqDiaEnvelope.clone(Const.EAST));
    
    const westLine = this.fragmentWest.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    westLine.setAttribute('x1', FilterSize.eventWidthHalf - Const.LINE_WIDTH_HALF);
    westLine.setAttribute('x2', 7);
    westLine.setAttribute('y1', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    westLine.setAttribute('y2', Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF);
    
    this.fragmentWest.appendChild(ComponentSeqDiaArrow.createWest(0));
    this.fragmentWest.appendChild(ComponentSeqDiaEnvelope.clone(Const.WEST));
  }
  
  onCreateSvgFragment(protocolKey) {
    const fragmentEastStart = this.fragmentEast.cloneNode(true);
    fragmentEastStart.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_msg`);
    const fragmentWestStart = this.fragmentWest.cloneNode(true);
    fragmentWestStart.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_msg`);
    return [fragmentEastStart, null, fragmentWestStart, null, null];
  }
  
  calculateHeight(buffer) {
    const phaseId = DataSeqDiaSend.restoreHeightData(buffer);
    return this.filter.getMessageDetailEvents(phaseId) ? this.height : 0;
  }
  
  onCloneToSvg(localNode, remoteNode, data) {
    ComponentSeqDiaEnvelope.addEventListener(localNode.lastChild);
    const reverseDirection = data.reverseDirection ? 1 : 0;
    if(data.localIsShared) {
      const shared = ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection);
      shared.setAttribute('transform', 'translate(0, 10)');
      localNode.appendChild(shared);
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.LOCAL_PART, reverseDirection));
      }
    }
    if(data.remoteIsShared) {
      const shared = ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection);
      shared.setAttribute('transform', 'translate(0, 10)');
      localNode.appendChild(shared); 
      if(remoteNode) {
        remoteNode.appendChild(ComponentSeqDiaShared.clone(data.protocolIndex, Const.REMOTE_PART, reverseDirection));
      }
    }
  }
    
  onDestroyToSvg(localNode, remoteNode) {
    ComponentSeqDiaEnvelope.removeEventListener(localNode.lastChild);
  }
  
  onCloneInfo(node, data) {
    const dataNode = ComponentSeqDiaCaption.clone(data.reverseDirection, data.extraData.caption);
    node.appendChild(dataNode);
  }
}


module.exports = TemplateSeqDiaSend;
