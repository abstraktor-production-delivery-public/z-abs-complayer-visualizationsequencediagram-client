
'use strict';

import Const from '../../logic/const';


class ComponentSeqDiaEventInfo {
  static stackStyles = null;
  static templateData = [];
  
  static init(stackStyles) {
    ComponentSeqDiaEventInfo.stackStyles = stackStyles;
  }
  
  static createLeftRowDiv(className) {
    const divLeft = document.createElement('div');
    divLeft.classList.add(className);
    divLeft.classList.add('seq_dia_protocol_left');
    return {
      div: divLeft,
      inners: []
    };
  }
  
  static createRightRowDiv(className) {
    const divRight = document.createElement('div');
    divRight.classList.add(className);
    divRight.classList.add('seq_dia_protocol_right');
    return {
      div: divRight,
      inners: []
    };
  }
  
  static _createInfoDiv(leftRow, templateText, className, useStackStyles, pos) {
    const div = leftRow.div.appendChild(document.createElement('div'));
    div.classList.add(className);
    div.classList.add(pos);
    if(!useStackStyles) {
      div.classList.add('seq_dia_no_protocol_data');
    }
    const p = div.appendChild(document.createElement('p'));
    p.classList.add('seq_dia_protocol_data');
    p.appendChild(document.createTextNode(templateText));
    leftRow.inners.push({
      div: div, 
      useStackStyles: useStackStyles,
      templateText: templateText
    });
  }
  
  static createLeftInfoDiv(leftRow, templateText, className, useStackStyles) {
    return ComponentSeqDiaEventInfo._createInfoDiv(leftRow, templateText, className, useStackStyles, 'seq_dia_protocol_data_left');
  }
  
  static createRightInfoDiv(rightRow, templateText, className, useStackStyles) {
    return ComponentSeqDiaEventInfo._createInfoDiv(rightRow, templateText, className, useStackStyles, 'seq_dia_protocol_data_right');
  }
  
  static __createTemplate(row, id, pos) {
    const styleSheet = document.styleSheets[0];
    ComponentSeqDiaEventInfo.stackStyles.forEach((protocol, protocolKey) => {
      const classLine = `seq_dia_protocol_${protocolKey}`;
      const eventData = row.div.cloneNode(true);
      row.inners.forEach((inner, index) => {
        if(inner.useStackStyles) {
          eventData.childNodes[index].classList.add(`${classLine}_dimond`);
        }
        if('${prot}' === inner.templateText) {
          const protocolChildLeft = eventData.childNodes[index].firstChild;
          protocolChildLeft.replaceChild(document.createTextNode(protocolKey), protocolChildLeft.firstChild);
        }
      });
      ComponentSeqDiaEventInfo.templateData[id][pos][protocol.index] = eventData;
    });
  }
  
  static createTemplate(leftRow, rightRow) {
    const id = ComponentSeqDiaEventInfo.templateData.length;
    ComponentSeqDiaEventInfo.templateData.push([leftRow ? new Array(ComponentSeqDiaEventInfo.stackStyles.size) : null, rightRow ? new Array(ComponentSeqDiaEventInfo.stackStyles.size) : null]);
    if(leftRow) {
      ComponentSeqDiaEventInfo.__createTemplate(leftRow, id, Const.PART_LEFT);
    }
    if(rightRow) {
      ComponentSeqDiaEventInfo.__createTemplate(rightRow, id, Const.PART_RIGHT);
    }
    return id;
  }
  
  static clone(id, part, protocolIndex, renderPosition) {
    if(part <= Const.PART_RIGHT) {
      const infoNode = ComponentSeqDiaEventInfo.templateData[id][part][protocolIndex].cloneNode(true);
      if(Const.RENDER_POSITION_BOTTOM === renderPosition) {
        infoNode.setAttribute('style', 'top:10px');
      }
      return infoNode;
    }
    else {
      return null;
    }
  }
}


module.exports = ComponentSeqDiaEventInfo;
