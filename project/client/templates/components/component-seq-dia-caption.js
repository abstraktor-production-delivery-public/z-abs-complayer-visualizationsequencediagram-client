
'use strict';

import Const from '../../logic/const';


class ComponentSeqDiaCaption {
  static template = new Array(2);
  
  static init() {
    const divLeft = document.createElement('div');
    divLeft.classList.add('seq_dia__caption');
    divLeft.classList.add('seq_dia_protocol_left');
    const pLeft = divLeft.appendChild(document.createElement('p'));
    pLeft.classList.add('seq_dia_protocol_caption_west');
    pLeft.appendChild(document.createTextNode('left'));
    
    const divRight = document.createElement('div');
    divRight.classList.add('seq_dia__caption');
    divRight.classList.add('seq_dia_protocol_right');
    const pRight = divRight.appendChild(document.createElement('p'));
    pRight.classList.add('seq_dia_protocol_caption_east');
    pRight.appendChild(document.createTextNode('right'));
    
    ComponentSeqDiaCaption.template[0] = divRight;
    ComponentSeqDiaCaption.template[1] = divLeft;
  }
  
  static clone(reverseDirection, text) {
    const captionNode = ComponentSeqDiaCaption.template[reverseDirection ? 1 : 0].cloneNode(true);
    captionNode.firstChild.replaceChild(document.createTextNode(text), captionNode.firstChild.firstChild);
    return captionNode;
  }
}


module.exports = ComponentSeqDiaCaption;
