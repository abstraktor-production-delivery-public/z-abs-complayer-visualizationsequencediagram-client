
'use strict';

import Const from '../../logic/const';
import FilterSize from '../../logic/filter-size';


class ComponentSeqDiaShared {
  static template = null;
  
  static init(stackStyles) {
    ComponentSeqDiaShared.template = new Array(stackStyles.size);
    const rightTemplate = ComponentSeqDiaShared.create();
    const leftTemplate = rightTemplate.cloneNode(true);
    leftTemplate.firstChild.setAttribute('cx', FilterSize.eventWidthHalf - 5);
    leftTemplate.childNodes[1].setAttribute('cx', FilterSize.eventWidthHalf - 5);
    stackStyles.forEach((protocol, protocolKey) => {
      this.template[protocol.index] = new Array(new Array(2), new Array(2));
      const left = leftTemplate.cloneNode(true);
      left.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
      left.childNodes[1].classList.add(`seq_dia_protocol_${protocolKey}_small`);
      const right = rightTemplate.cloneNode(true);
      right.firstChild.classList.add(`seq_dia_protocol_${protocolKey}_small`);
      right.childNodes[1].classList.add(`seq_dia_protocol_${protocolKey}_small`);
      this.template[protocol.index][Const.LOCAL_PART][0] = left.cloneNode(true);
      this.template[protocol.index][Const.LOCAL_PART][1] = right.cloneNode(true);
      this.template[protocol.index][Const.REMOTE_PART][0] = right.cloneNode(true);
      this.template[protocol.index][Const.REMOTE_PART][1] = left.cloneNode(true);
    });
  }
  
  static create() {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    const circle1 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
    circle1.classList.add('seq_dia_shared');
    circle1.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT - 2);
    circle1.setAttribute('r', 1.5);
    circle1.setAttribute('cx', FilterSize.eventWidthHalf + 5);
    const circle2 = g.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
    circle2.classList.add('seq_dia_shared');
    circle2.setAttribute('cy', Const.STATE_EVENT_HALF_HEIGHT + 2);
    circle2.setAttribute('r', 1.5);
    circle2.setAttribute('cx', FilterSize.eventWidthHalf + 5);
    return g;
  }
  
  static clone(protocolIndex, part, reverseDirection) {
    return ComponentSeqDiaShared.template[protocolIndex][part][reverseDirection].cloneNode(true);
  }
}


module.exports = ComponentSeqDiaShared;
