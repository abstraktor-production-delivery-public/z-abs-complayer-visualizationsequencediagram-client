
'use strict';

import Const from '../../logic/const';
import ComponentSeqDiaEventInfo from './component-seq-dia-event-info';
import ComponentSeqDiaEventRow from './component-seq-dia-event-row';
import FilterSize from '../../logic/filter-size';


class ComponentSeqDiaProtocol {
  static FRAGMENT = 0;
  static PART = 1;
  
  static DIRECTION_EAST = [
    [[Const.EAST_START, Const.WEST_STOP], Const.PART_LEFT],
    [[Const.EAST_STOP, Const.WEST_START], Const.PART_RIGHT],
    [[Const.EAST_MIDDLE, Const.WEST_MIDDLE], Const.PART_MIDDLE],
    [[Const.EAST_MIDDLE, Const.WEST_MIDDLE], Const.PART_MIDDLE_NODE_CONTINUED],
    [[Const.EAST_MIDDLE, Const.WEST_MIDDLE], Const.PART_MIDDLE_NODE_LAST]
  ];

  static DIRECTION_WEST = [
    [[Const.WEST_STOP, Const.EAST_START], Const.PART_LEFT],
    [[Const.WEST_START, Const.EAST_STOP], Const.PART_RIGHT],
    [[Const.WEST_MIDDLE, Const.EAST_MIDDLE], Const.PART_MIDDLE],
    [[Const.WEST_MIDDLE, Const.EAST_MIDDLE], Const.PART_MIDDLE_NODE_CONTINUED],
    [[Const.WEST_MIDDLE, Const.EAST_MIDDLE], Const.PART_MIDDLE_NODE_LAST]
  ];

  static lineTemplate = new Array(5);
  static eventInfoId = -1;
  
  static init(stackStyles) {
    const leftRow = ComponentSeqDiaEventInfo.createLeftRowDiv('seq_dia__data_protocol');
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, 'inst', 'seq_dia__data_protocol_instance', true);
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, 'tran', 'seq_dia__data_protocol_transport', true);
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, '${prot}', 'seq_dia__data_protocol_name', true);
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, '127.0.0.1:80', 'seq_dia__data_protocol_ip', false);
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, 'srcA', 'seq_dia__data_protocol_address_name', false);
    ComponentSeqDiaEventInfo.createLeftInfoDiv(leftRow, 'info.......', 'seq_dia__data_protocol_info', false);
    
    const rightRow = ComponentSeqDiaEventInfo.createRightRowDiv('seq_dia__data_protocol');
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, 'inst', 'seq_dia__data_protocol_instance', true);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, 'tran', 'seq_dia__data_protocol_transport', true);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '${prot}', 'seq_dia__data_protocol_name', true);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, '127.0.0.1:80', 'seq_dia__data_protocol_ip', false);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, 'srcA', 'seq_dia__data_protocol_address_name', false);
    ComponentSeqDiaEventInfo.createRightInfoDiv(rightRow, 'info.......', 'seq_dia__data_protocol_info', false);
    
    ComponentSeqDiaProtocol.eventInfoId = ComponentSeqDiaEventInfo.createTemplate(leftRow, rightRow);
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    
    const leftLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    leftLine.setAttribute('x1', FilterSize.eventWidthHalf);
    leftLine.setAttribute('x2', FilterSize.eventWidth);
    leftLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    leftLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    
    const rightLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    rightLine.setAttribute('x1', 0);
    rightLine.setAttribute('x2', FilterSize.eventWidthHalf);
    rightLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    rightLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    
    const middleLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    middleLine.setAttribute('x1', 0);
    middleLine.setAttribute('x2', FilterSize.eventWidth);
    middleLine.setAttribute('y1', Const.STATE_EVENT_HALF_HEIGHT);
    middleLine.setAttribute('y2', Const.STATE_EVENT_HALF_HEIGHT);
    
    const middleNodeContinuedLine = middleLine.cloneNode(true);
    const middleNodeLastLine = middleLine.cloneNode(true);
    
    ComponentSeqDiaProtocol.lineTemplate[Const.PART_LEFT] = new Array(stackStyles.size);
    ComponentSeqDiaProtocol.lineTemplate[Const.PART_RIGHT] = new Array(stackStyles.size);
    ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE] = new Array(stackStyles.size);
    ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_CONTINUED] = new Array(stackStyles.size);
    ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_LAST] = new Array(stackStyles.size);
    stackStyles.forEach((protocol, protocolKey) => {
      const leftLineProtocol = leftLine.cloneNode(true);
      leftLineProtocol.classList.add(`seq_dia_protocol_${protocolKey}`);
      const rightLineProtocol = rightLine.cloneNode(true);
      rightLineProtocol.classList.add(`seq_dia_protocol_${protocolKey}`);
      const middleLineProtocol = middleLine.cloneNode(true);
      middleLineProtocol.classList.add(`seq_dia_protocol_${protocolKey}`);
      const middleNodeContinuedLineProtocol = middleNodeContinuedLine.cloneNode(true);
      middleNodeContinuedLineProtocol.classList.add(`seq_dia_protocol_${protocolKey}`);
      const middleNodeLastLineProtocol = middleNodeLastLine.cloneNode(true);
      middleNodeLastLineProtocol.classList.add(`seq_dia_protocol_${protocolKey}`);
      ComponentSeqDiaProtocol.lineTemplate[Const.PART_LEFT][protocol.index] = new Array(Const.TRANSPORT_LINE_CSSES.length);
      ComponentSeqDiaProtocol.lineTemplate[Const.PART_RIGHT][protocol.index] = new Array(Const.TRANSPORT_LINE_CSSES.length);
      ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE][protocol.index] = new Array(Const.TRANSPORT_LINE_CSSES.length);
      ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_CONTINUED][protocol.index] = new Array(Const.TRANSPORT_LINE_CSSES.length);
      ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_LAST][protocol.index] = new Array(Const.TRANSPORT_LINE_CSSES.length);
      Const.TRANSPORT_LINE_CSSES.forEach((css, index) => {
        const leftLineProtocolTransport = leftLineProtocol.cloneNode(true);
        leftLineProtocolTransport.classList.add(css);
        const rightLineProtocolTransport = rightLineProtocol.cloneNode(true);
        rightLineProtocolTransport.classList.add(css);
        const middleLineProtocolTransport = middleLineProtocol.cloneNode(true);
        middleLineProtocolTransport.classList.add(css);
        const middleNodeContinuedLineProtocolTransport = middleNodeContinuedLineProtocol.cloneNode(true);
        middleNodeContinuedLineProtocolTransport.classList.add(css);
        const middleNodeLastLineProtocolTransport = middleNodeLastLineProtocol.cloneNode(true);
        middleNodeLastLineProtocolTransport.classList.add(css);
        ComponentSeqDiaProtocol.lineTemplate[Const.PART_LEFT][protocol.index][index] = new Array(Const.PROTOCOL_LINE_CSSES.length);
        ComponentSeqDiaProtocol.lineTemplate[Const.PART_RIGHT][protocol.index][index] = new Array(Const.PROTOCOL_LINE_CSSES.length);
        ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE][protocol.index][index] = new Array(Const.PROTOCOL_LINE_CSSES.length);
        ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_CONTINUED][protocol.index][index] = new Array(Const.PROTOCOL_LINE_CSSES.length);
        ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_LAST][protocol.index][index] = new Array(Const.PROTOCOL_LINE_CSSES.length);
        Const.PROTOCOL_LINE_CSSES.forEach((lineCss, lineIndex) => {
          const leftLineProtocolTransportLine = leftLineProtocolTransport.cloneNode(true);
          leftLineProtocolTransportLine.classList.add(lineCss);
          const rightLineProtocolTransportLine = rightLineProtocolTransport.cloneNode(true);
          rightLineProtocolTransportLine.classList.add(lineCss);
          const middleLineProtocolTransportLine = middleLineProtocolTransport.cloneNode(true);
          middleLineProtocolTransportLine.classList.add(lineCss);
          const middleNodeContinuedLineProtocolTransportLine = middleNodeContinuedLineProtocolTransport.cloneNode(true);
          middleNodeContinuedLineProtocolTransportLine.classList.add(lineCss);
          const middleNodeLastLineProtocolTransportLine = middleNodeLastLineProtocolTransport.cloneNode(true);
          middleNodeLastLineProtocolTransportLine.classList.add(lineCss);
          
          const leftG = g.cloneNode();
          leftG.appendChild(leftLineProtocolTransportLine);
          const rightG = g.cloneNode();
          rightG.appendChild(rightLineProtocolTransportLine);
          const middleG = g.cloneNode();
          middleG.appendChild(middleLineProtocolTransportLine);
          const continuedG = g.cloneNode();
          continuedG.appendChild(middleNodeContinuedLineProtocolTransportLine);
          const lastG = g.cloneNode();
          lastG.appendChild(middleNodeLastLineProtocolTransportLine);
          ComponentSeqDiaProtocol.lineTemplate[Const.PART_LEFT][protocol.index][index][lineIndex] = leftG;
          ComponentSeqDiaProtocol.lineTemplate[Const.PART_RIGHT][protocol.index][index][lineIndex] = rightG;
          ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE][protocol.index][index][lineIndex] = middleG;
          ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_CONTINUED][protocol.index][index][lineIndex] = continuedG;
          ComponentSeqDiaProtocol.lineTemplate[Const.PART_MIDDLE_NODE_LAST][protocol.index][index][lineIndex] = lastG;
        });
      });
    });
  }
  
  static setMsgNbr(middleSvg, direction, part, messageDirection, nbrOfMessages) {
    if(Const.DIRECTION_EAST === direction) {
      if(part === Const.PART_LEFT) {
        const group = middleSvg.childNodes[1].childNodes[3];
        group.classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.classList.add(Const.MESSAGE_DIRECTION_FILTER[messageDirection]);
        group.childNodes[1].classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.childNodes[2].replaceChild(document.createTextNode(nbrOfMessages), group.childNodes[2].firstChild);
      }
      else if(part === Const.PART_RIGHT) {
        const group = middleSvg.childNodes[1].childNodes[1];
        group.classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.classList.add(Const.MESSAGE_DIRECTION_FILTER[messageDirection]);
        group.childNodes[1].classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.childNodes[2].replaceChild(document.createTextNode(nbrOfMessages), group.childNodes[2].firstChild);
      }
    }
    else if(Const.DIRECTION_WEST === direction) {
      if(part === Const.PART_LEFT) {
        const group = middleSvg.childNodes[1].childNodes[1];
        group.classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.classList.add(Const.MESSAGE_DIRECTION_FILTER[messageDirection]);
        group.childNodes[1].classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.childNodes[2].replaceChild(document.createTextNode(nbrOfMessages), group.childNodes[2].firstChild);
      }
      else if(part === Const.PART_RIGHT) {
        const group = middleSvg.childNodes[1].childNodes[3];
        group.classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.classList.add(Const.MESSAGE_DIRECTION_FILTER[messageDirection]);
        group.childNodes[1].classList.add(Const.MESSAGE_DIRECTION[messageDirection]);
        group.childNodes[2].replaceChild(document.createTextNode(nbrOfMessages), group.childNodes[2].firstChild);
      }
    }
  }
  
  static cloneInto(className, height, data, funcClone) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const phaseId = data.phaseId;
    const node = ComponentSeqDiaEventRow.cloneRow(className);
    const divNodeLeft = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnLeft(phaseId));
    switch(data.renderNodes) {
      case Const.RENDER_SINGLE_NODE: 
        ComponentSeqDiaProtocol._cloneSingleNode(node, height, data, funcClone);
        break;
      case Const.RENDER_TWO_NODES: 
        ComponentSeqDiaProtocol._cloneTwoNodes(node, height, data, funcClone);
        break;
    }
    node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnRight(actorPhases.length, phaseId));
    return node;
  }
  
  static _cloneSingleNode(node, height, data, funcClone) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const phaseId = data.phaseId;
    const protocolIndex = data.protocolIndex;
    const localActorIndex = data.localActorIndex;
    for(let i = 0; i < actorPhases.length; ++i) {
      const divNodeMiddle = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnMiddle(i, phaseId, actorParts[i]));
      if(localActorIndex === i) {
        const svg = divNodeMiddle.appendChild(ComponentSeqDiaEventRow.cloneSvg(height, Const.RENDER_POSITION_NORMAL));
        const currentNode = funcClone(data.reverseDirection ? Const.WEST_START : Const.EAST_START);
        svg.appendChild(currentNode);
        const dataNode = ComponentSeqDiaEventInfo.clone(ComponentSeqDiaProtocol.eventInfoId, data.reverseDirection ? Const.PART_RIGHT : Const.PART_LEFT, protocolIndex, data.renderPosition);
        ComponentSeqDiaProtocol._setLocal(dataNode, data, false);
        divNodeMiddle.appendChild(dataNode);
      }
    }
  }
  
  static _cloneTwoNodes(node, height, data, funcClone) {
    const actorPhases = data.actorPhases;
    const actorParts = data.actorParts;
    const phaseId = data.phaseId;
    const protocolIndex = data.protocolIndex;
    const localType = data.localType;
    const localActorIndex = data.localActorIndex;
    const remoteActorIndex = data.remoteActorIndex;
    for(let i = 0; i < actorPhases.length; ++i) {
      const divNodeMiddle = node.firstChild.appendChild(ComponentSeqDiaEventRow.cloneColumnMiddle(i, phaseId, actorParts[i]));
      const parameters = ComponentSeqDiaProtocol.createParameters(localActorIndex, remoteActorIndex, i, localType);
      if(parameters) {
        const fragment = parameters[ComponentSeqDiaProtocol.FRAGMENT][data.reverseDirection ? 1 : 0];
        const part = parameters[ComponentSeqDiaProtocol.PART];
        const svg = divNodeMiddle.appendChild(ComponentSeqDiaEventRow.cloneSvg(height, data.renderPosition));
        const currentNode = funcClone(fragment);
        const lineG = ComponentSeqDiaProtocol.lineTemplate[part][data.protocolIndex][data.transportType][data.protocolLine].cloneNode(true);
        if(Const.RENDER_POSITION_BOTTOM === data.renderPosition) {
          lineG.setAttribute('transform', 'translate(0, 10)');
        }
        if(currentNode) {
          svg.appendChild(currentNode);
          svg.firstChild.insertBefore(lineG, svg.firstChild.firstChild);
          const dataNode = ComponentSeqDiaEventInfo.clone(ComponentSeqDiaProtocol.eventInfoId, part, protocolIndex, data.renderPosition);
          if(Const.EAST_START === fragment || Const.WEST_START === fragment) {
            ComponentSeqDiaProtocol._setLocal(dataNode, data, true);
          }
          else if(Const.EAST_STOP === fragment || Const.WEST_STOP === fragment) {
            ComponentSeqDiaProtocol._setRemote(dataNode, data, true);
          }
          divNodeMiddle.appendChild(dataNode);
        }
        else {
          svg.appendChild(lineG);
        }
      }
    }
  }
  
  static _setLocal(dataNode, data, isTwoNodes) {
    const instanceNode = dataNode.firstChild.firstChild;
    const id = isTwoNodes || !!(data.extraData?.isConnection) ? `${data.localOwnerId}, ${data.localId}` : data.localOwnerId;
    instanceNode.replaceChild(document.createTextNode(id), instanceNode.firstChild);
    const parentTransportNode = dataNode.childNodes[1].firstChild;
    parentTransportNode.replaceChild(document.createTextNode(Const.transportTypeNames[data.transportType]), parentTransportNode.firstChild);
    const parentIpNode = dataNode.childNodes[3].firstChild;
    parentIpNode.replaceChild(document.createTextNode(data.localIp), parentIpNode.firstChild);
    const parentNameNode = dataNode.childNodes[4].firstChild;
    parentNameNode.replaceChild(document.createTextNode(data.localName), parentNameNode.firstChild);
    const parentInfoNode = dataNode.lastChild.firstChild;
    parentInfoNode.replaceChild(document.createTextNode(data.info), parentInfoNode.firstChild);
  }
  
  static _setRemote(dataNode, data) {
    const instanceNode = dataNode.firstChild.firstChild;
    instanceNode.replaceChild(document.createTextNode(`${data.remoteOwnerId}, ${data.remoteId}`), instanceNode.firstChild);
    const parentTransportNode = dataNode.childNodes[1].firstChild;
    parentTransportNode.replaceChild(document.createTextNode(Const.transportTypeNames[data.transportType]), parentTransportNode.firstChild);
    const parentIpNode = dataNode.childNodes[3].firstChild;
    parentIpNode.replaceChild(document.createTextNode(data.remoteIp), parentIpNode.firstChild);
    const parentNameNode = dataNode.childNodes[4].firstChild;
    parentNameNode.replaceChild(document.createTextNode(data.remoteName), parentNameNode.firstChild);
    const parentInfoNode = dataNode.lastChild.firstChild;
    parentInfoNode.replaceChild(document.createTextNode(data.info), parentInfoNode.firstChild);
  }
  
  static createParameters(localActorIndex, remoteActorIndex, index, type) {
    const srcIndex = Const.PART_TYPE_CLIENT === type ? localActorIndex : remoteActorIndex;
    const dstIndex = Const.PART_TYPE_CLIENT === type ? remoteActorIndex : localActorIndex;
    if(srcIndex < dstIndex) {
      if(index > srcIndex && index < dstIndex) {
        return ComponentSeqDiaProtocol.DIRECTION_EAST[Const.PART_MIDDLE];
      }
      else if(index === srcIndex) {
        return ComponentSeqDiaProtocol.DIRECTION_EAST[Const.PART_LEFT];
      }
      else if(index === dstIndex) {
        return ComponentSeqDiaProtocol.DIRECTION_EAST[Const.PART_RIGHT];
      }
    }
    else {
      if(index > dstIndex && index < srcIndex) {
        return ComponentSeqDiaProtocol.DIRECTION_WEST[Const.PART_MIDDLE];
      }
      else if(index === dstIndex) {
        return ComponentSeqDiaProtocol.DIRECTION_WEST[Const.PART_LEFT];
      }
      else if(index === srcIndex) {
        return ComponentSeqDiaProtocol.DIRECTION_WEST[Const.PART_RIGHT];
      }
    }
    return null;
  }
}


module.exports = ComponentSeqDiaProtocol;
