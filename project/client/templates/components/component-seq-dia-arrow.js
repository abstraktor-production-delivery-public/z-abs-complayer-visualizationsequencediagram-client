
'use strict';

import Const from '../../logic/const';


class ComponentSeqDiaArrow {
  static ARROW_WIDTH = 11;
  static ARROW_HEIGHT = 3;
  static ARROW_WIDTH_MIDDLE = 6;

  static createEast(x) {
    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    arrow.setAttribute('points', `${x - ComponentSeqDiaArrow.ARROW_WIDTH},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF - ComponentSeqDiaArrow.ARROW_HEIGHT} ${x - ComponentSeqDiaArrow.ARROW_WIDTH},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF + ComponentSeqDiaArrow.ARROW_HEIGHT} ${x},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF}`);
    return arrow;
  }
  
  static createWest(x) {
    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    arrow.setAttribute('points', `${x + ComponentSeqDiaArrow.ARROW_WIDTH},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF - ComponentSeqDiaArrow.ARROW_HEIGHT} ${x + ComponentSeqDiaArrow.ARROW_WIDTH},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF + ComponentSeqDiaArrow.ARROW_HEIGHT} ${x},${Const.MESSAGE_EVENT_HEIGHT_ONE_AND_HALF}`);
    return arrow;
  }
}


module.exports = ComponentSeqDiaArrow;
