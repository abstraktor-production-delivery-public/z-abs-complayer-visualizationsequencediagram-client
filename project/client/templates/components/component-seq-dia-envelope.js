
'use strict';

import Const from '../../logic/const';
import FilterSize from '../../logic/filter-size';


class ComponentSeqDiaEnvelope {
  static TOP_POINT = 2;
  static MIDDLE_TOP_POINT = ComponentSeqDiaEnvelope.TOP_POINT + 2.5;
  static MIDDLE_BOTTOM_POINT = ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT + 2.5;
  static BOTTOM_POINT = ComponentSeqDiaEnvelope.MIDDLE_BOTTOM_POINT + 2.5;
  static WIDTH = 5;
  static LEFT_POINT = 0;
  static MIDDLE_POINT = ComponentSeqDiaEnvelope.LEFT_POINT + ComponentSeqDiaEnvelope.WIDTH;
  static RIGHT_POINT = ComponentSeqDiaEnvelope.MIDDLE_POINT + ComponentSeqDiaEnvelope.WIDTH;
  
  static envelopeTemplate = new Array(2);
  static stateTemplate = new Array(2);
  static __clickListener = this._clickListener.bind(this);
  
  static init() {
    const closedGroup = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    closedGroup.setAttribute('data-id', 0);
    
    const bottomNode = closedGroup.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
    bottomNode.setAttribute('points', `${ComponentSeqDiaEnvelope.LEFT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.MIDDLE_POINT},${ComponentSeqDiaEnvelope.MIDDLE_BOTTOM_POINT} ${ComponentSeqDiaEnvelope.RIGHT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.RIGHT_POINT},${ComponentSeqDiaEnvelope.BOTTOM_POINT} ${ComponentSeqDiaEnvelope.LEFT_POINT},${ComponentSeqDiaEnvelope.BOTTOM_POINT} ${ComponentSeqDiaEnvelope.LEFT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.MIDDLE_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.RIGHT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT}`);
    
    const rectTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    rectTemplate.setAttribute('y', ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT);
    rectTemplate.setAttribute('height', 5);
    rectTemplate.setAttribute('style', 'fill:white;cursor:pointer;opacity:0;');
    rectTemplate.setAttribute('x', 0);
    rectTemplate.setAttribute('width', 10);
    
    const openGroup = closedGroup.cloneNode(true);
          
    ComponentSeqDiaEnvelope.stateTemplate[1] = closedGroup.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
    ComponentSeqDiaEnvelope.stateTemplate[1].setAttribute('points', `${ComponentSeqDiaEnvelope.LEFT_POINT + 2},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.MIDDLE_POINT},${ComponentSeqDiaEnvelope.MIDDLE_BOTTOM_POINT - 1} ${ComponentSeqDiaEnvelope.RIGHT_POINT - 2},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT}`);
    
    const eastClosed = ComponentSeqDiaEnvelope.envelopeTemplate[Const.EAST] = closedGroup.cloneNode(true);
    const eastClosedEventObject = eastClosed.appendChild(rectTemplate.cloneNode(true));
    eastClosed.setAttribute('style', `transform:translate(${FilterSize.eventWidthHalf + 4}px)`);
    ComponentSeqDiaEnvelope.envelopeTemplate[Const.EAST].setAttribute('data-state', 0);
    
    const westClosed = ComponentSeqDiaEnvelope.envelopeTemplate[Const.WEST] = closedGroup.cloneNode(true);
    const westClosedEventObject = westClosed.appendChild(rectTemplate.cloneNode(true));
    westClosed.setAttribute('style', `transform:translate(${FilterSize.eventWidthHalf - 4 - 10}px)`);
    ComponentSeqDiaEnvelope.envelopeTemplate[Const.WEST].setAttribute('data-state', 0);
    
    ComponentSeqDiaEnvelope.stateTemplate[0] = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    ComponentSeqDiaEnvelope.stateTemplate[0].setAttribute('points', `${ComponentSeqDiaEnvelope.LEFT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.MIDDLE_POINT},${ComponentSeqDiaEnvelope.MIDDLE_BOTTOM_POINT} ${ComponentSeqDiaEnvelope.RIGHT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.RIGHT_POINT},${ComponentSeqDiaEnvelope.BOTTOM_POINT} ${ComponentSeqDiaEnvelope.LEFT_POINT},${ComponentSeqDiaEnvelope.BOTTOM_POINT} ${ComponentSeqDiaEnvelope.LEFT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT} ${ComponentSeqDiaEnvelope.MIDDLE_POINT},${ComponentSeqDiaEnvelope.TOP_POINT} ${ComponentSeqDiaEnvelope.RIGHT_POINT},${ComponentSeqDiaEnvelope.MIDDLE_TOP_POINT}`);
  }
  
  static clone(direction) {
    const node = ComponentSeqDiaEnvelope.envelopeTemplate[direction].cloneNode(true);
    return node;
  }
  
  static _clickListener(event) {
    const state = Number.parseInt(event.srcElement.parentNode.getAttribute('data-state'));
    event.srcElement.parentNode.setAttribute('data-state', state ? 0 : 1);
    event.srcElement.parentNode.replaceChild(ComponentSeqDiaEnvelope.stateTemplate[state].cloneNode(), event.srcElement.parentNode.childNodes[1]);
    event.preventDefault();
  }
  
  static addEventListener(envelopeNode) {
    if(envelopeNode.lastChild) {
      envelopeNode.lastChild.addEventListener('click', ComponentSeqDiaEnvelope.__clickListener);
    }
  }
  
  static removeEventListener(envelopeNode) {
    if(envelopeNode.lastChild) {
      envelopeNode.lastChild.removeEventListener('click', ComponentSeqDiaEnvelope.__clickListener);
    }
  }
  
  /*static _openEnvelope(envelopePartNode, msgNode, direction) {
    ComponentSeqDiaEnvelope.seqDiaMsgTemplate.open(msgNode);
    const envelopeNode = ComponentSeqDiaEnvelope.envelopeTemplate[Const.OPEN][direction].cloneNode(true);
    envelopePartNode.parentNode.parentNode.replaceChild(envelopeNode, envelopePartNode.parentNode);
    return envelopeNode;
  }
  
  static _openEnvelopeListeners(envelopeNode, msgNode, direction) {
    envelopeNode.lastChild.addEventListener('click', (event) => {
      const nextEnvelopeNode = ComponentSeqDiaEnvelope._closeEnvelope(event.currentTarget, msgNode, direction);
      ComponentSeqDiaEnvelope._closeEnvelopeListeners(nextEnvelopeNode, msgNode, direction);
    }, false);
  }
  
  static _closeEnvelope(envelopePartNode, msgNode, direction) {
    ComponentSeqDiaEnvelope.seqDiaMsgTemplate.close(msgNode);
    const envelopeNode = ComponentSeqDiaEnvelope.envelopeTemplate[Const.CLOSED][direction].cloneNode(true);
    envelopePartNode.parentNode.parentNode.replaceChild(envelopeNode, envelopePartNode.parentNode);
    return envelopeNode;
  }
  
  static _closeEnvelopeListeners(envelopeNode, msgNode, direction) {
    envelopeNode.lastChild.addEventListener('click', (event) => {
      const nextEnvelopeNode = ComponentSeqDiaEnvelope._openEnvelope(event.currentTarget, msgNode, direction);
      ComponentSeqDiaEnvelope._openEnvelopeListeners(nextEnvelopeNode, msgNode, direction);
    }, false);
  }*/
}


module.exports = ComponentSeqDiaEnvelope;
