
'use strict';

import Const from '../../logic/const';
import FilterSize from '../../logic/filter-size';
import ActorPhaseConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const';


class ComponentSeqDiaEventRow {
  static divRowHeader = null;
  static divColumnHeaderLeft = null;
  static divColumnHeader = null;
  static divColumnHeaderRight = null;
  static divRow = null;
  static divColumnLeft = null;
  static divColumn = null;
  static divColumnRight = null;
  static svgTemplate = null;
  
  static init() {
    ComponentSeqDiaEventRow.divRowHeader = document.createElement('div');
    const divRowHeader = ComponentSeqDiaEventRow.divRowHeader.appendChild(document.createElement('div'));
    divRowHeader.classList.add('seq_dia_event_row');
    
    const divColumnHeaderTemplate = document.createElement('div');
        
    ComponentSeqDiaEventRow.divColumnHeaderLeft = divColumnHeaderTemplate.cloneNode(true);
    ComponentSeqDiaEventRow.divColumnHeaderLeft.classList.add('seq_dia_event_column_header_left');
    ComponentSeqDiaEventRow.divColumnHeaderLeft.classList.add('seq_dia__event_column_left');
    ComponentSeqDiaEventRow.divColumnHeader = new Array(Const.ACTOR_STATIC_ACTORS);
    ComponentSeqDiaEventRow.divColumnHeaderRight = new Array(Const.ACTOR_STATIC_ACTORS);
    
    for(let i = 0; i < Const.ACTOR_STATIC_ACTORS; ++i) {
      const divColumnHeader = divColumnHeaderTemplate.cloneNode(true);
      divColumnHeader.classList.add('seq_dia_event_column_header_middle');
      divColumnHeader.classList.add(`seq_dia__event_column_middle_${i}`);
      ComponentSeqDiaEventRow.divColumnHeader[i] = divColumnHeader;
      const divColumnHeaderRight = divColumnHeaderTemplate.cloneNode(true);
      divColumnHeaderRight.classList.add('seq_dia_event_column_header_right');
      divColumnHeaderRight.classList.add(`seq_dia__event_column_right_${i}`);
      ComponentSeqDiaEventRow.divColumnHeaderRight[i] = divColumnHeaderRight;
    }
        
    ComponentSeqDiaEventRow.divRow = document.createElement('div');
    const divRow = ComponentSeqDiaEventRow.divRow.appendChild(document.createElement('div'));
    divRow.classList.add('seq_dia_event_row');
    const divColumnTemplateLR = document.createElement('div');
    const divColumnTemplate = document.createElement('div');
    const actorLine = divColumnTemplate.appendChild(document.createElement('div'));
    actorLine.classList.add('seq_dia_event_actor_line');
    actorLine.classList.add('seq__dia_event_actor_line');
    
    ComponentSeqDiaEventRow.divColumnLeft = new Array(ActorPhaseConst.namesRunning.length);
    ActorPhaseConst.namesRunning.forEach((phase, indexPhase) => {
      const divColumnLeft = divColumnTemplateLR.cloneNode(true);
      divColumnLeft.classList.add('seq_dia_event_column_left');
      divColumnLeft.classList.add(`seq_dia_phase_${phase}`);
      divColumnLeft.classList.add('seq_dia__event_column_left');
      ComponentSeqDiaEventRow.divColumnLeft[indexPhase] = divColumnLeft;
    });
    ComponentSeqDiaEventRow.divColumn = new Array(Const.ACTOR_STATIC_ACTORS);
    ComponentSeqDiaEventRow.divColumnRight = new Array(Const.ACTOR_STATIC_ACTORS);
    
    for(let i = 0; i < Const.ACTOR_STATIC_ACTORS; ++i) {
      ComponentSeqDiaEventRow.divColumn[i] = new Array(ActorPhaseConst.namesRunning.length);
      ComponentSeqDiaEventRow.divColumnRight[i] = new Array(ActorPhaseConst.namesRunning.length);
      ActorPhaseConst.namesRunning.forEach((phase, indexPhase) => {
        const divColumn = divColumnTemplate.cloneNode(true);
        divColumn.classList.add('seq_dia_event_column_middle');
        divColumn.classList.add(`seq_dia_phase_${phase}`);
        divColumn.classList.add(`seq_dia__event_column_middle_${i}`);
        ComponentSeqDiaEventRow.divColumn[i][indexPhase] = divColumn;
        const divColumnRight = divColumnTemplateLR.cloneNode(true);
        divColumnRight.classList.add('seq_dia_event_column_right');
        divColumnRight.classList.add(`seq_dia_phase_${phase}`);
        divColumnRight.classList.add(`seq_dia__event_column_right_${i}`);
        ComponentSeqDiaEventRow.divColumnRight[i][indexPhase] = divColumnRight;
      });
    }
    
    const svgTemplate = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgTemplate.classList.add('actor_svg');
    svgTemplate.setAttribute('height', 10);
    svgTemplate.setAttribute('width', FilterSize.eventWidth);
    this.svgTemplate = svgTemplate;
  }
  
  static cloneRowHeader(className) {
    const divRowHeader = ComponentSeqDiaEventRow.divRowHeader.cloneNode(true);
    divRowHeader.classList.add(className);
    return divRowHeader;
  }
  
  static cloneColumnHeaderLeft() {
    return ComponentSeqDiaEventRow.divColumnHeaderLeft.cloneNode(true);
  }
  
  static cloneColumnHeader(actorIndex) {
    return ComponentSeqDiaEventRow.divColumnHeader[actorIndex].cloneNode(true);
  }
  
  static cloneColumnHeaderRight(actorIndex) {
    return ComponentSeqDiaEventRow.divColumnHeaderRight[actorIndex].cloneNode(true);
  }
  
  static cloneRow(className) {
    const divRow = ComponentSeqDiaEventRow.divRow.cloneNode(true);
    divRow.classList.add(className);
    return divRow;
  }
  
  static cloneColumnLeft(phaseId) {
    return ComponentSeqDiaEventRow.divColumnLeft[phaseId].cloneNode(true);
  }
  
  static cloneColumnMiddle(actorIndex, phaseId, actorPart) {
    return ComponentSeqDiaEventRow.divColumn[actorIndex][phaseId].cloneNode(true);
  }
  
  static cloneColumnRight(actorIndex, phaseId) {
    return ComponentSeqDiaEventRow.divColumnRight[actorIndex][phaseId].cloneNode(true);
  }
  
  static cloneSvg(height, renderPosition) {
    const svg = this.svgTemplate.cloneNode(true);
    if(10 !== height) {
      svg.setAttribute('height', height);
    } 
    return svg;
  }
}


module.exports = ComponentSeqDiaEventRow;
