
'use strict';

import DataSeqDiaProtocolSingleNode from '../data/data-seq-dia-protocol-single-node';
import DataSeqDiaProtocolSingleNodeResult from '../data/data-seq-dia-protocol-single-node-result';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaClientConnecting {
  static size = Encoder.CtSize + Encoder.Uint16Size + Encoder.CtSize + Encoder.Uint16Size;
  static InfoConnecting = 'Connecting';
  static InfoAddMembership = 'Add membership';
    
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaClientConnecting.size + DataSeqDiaProtocolSingleNode.getSize(sharedTemplateData));
    DataSeqDiaProtocolSingleNode.store(encoder, msg, sharedTemplateData, Const.PART_LEFT);
    const remote = msg.data.remote;
    encoder.setCtStringInternal(remote.host);
    encoder.setUint16(remote.port);
    encoder.setCtStringInternal(remote.name);
    encoder.setUint16(remote.actorIndex);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolSingleNode.restoreWhitChoise(decoder, (transportType) => {
      if(Const.TRANSPORT_TYPE_MC === transportType) {
        return DataSeqDiaClientConnecting.InfoAddMembership;
      }
      else {
        return DataSeqDiaClientConnecting.InfoConnecting;
      }
    }, DataSeqDiaProtocolSingleNodeResult);
    const host = decoder.getCtString();
    const port = decoder.getUint16();
    const remoteName = decoder.getCtString();
    const remoteActorIndex = decoder.getUint16();
    const remoteIp = `${host}:${port}`;
    result.extraData = {
      remoteName,
      remoteIp
    };
    result.reverseDirection = remoteActorIndex < result.localActorIndex;
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaClientConnecting;
