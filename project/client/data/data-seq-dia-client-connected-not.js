
'use strict';

import DataSeqDiaProtocolTwoNodes from '../data/data-seq-dia-protocol-two-nodes';
import DataSeqDiaProtocolSingleNodeResult from '../data/data-seq-dia-protocol-single-node-result';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaClientConnectedNot {
  static size = Encoder.Uint8Size + Encoder.Uint16Size;
  static InfoNotConnected = 'Not Connected';
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaClientConnectedNot.size + DataSeqDiaProtocolTwoNodes.getSize(sharedTemplateData));
    DataSeqDiaProtocolTwoNodes.store(encoder, msg, sharedTemplateData, Const.PART_LEFT);
    encoder.setUint2_1(msg.data.local.actionId, true);
    const remote = msg.data.remote;
    encoder.setUint16(remote.actorIndex);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolTwoNodes.restore(decoder, DataSeqDiaClientConnectedNot.InfoNotConnected);
    const actionId = decoder.getUint2_1(true);
    const remoteActorIndex = decoder.getUint16();
    result.renderNodes = Const.RENDER_TWO_NODES;
    result.protocolLine = Const.PROTOCOL_LINE_CONNECTION;
    result.reverseDirection = remoteActorIndex < result.localActorIndex;
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaClientConnectedNot;
