
'use strict';

import DataSeqDiaProtocolSingleNode from '../data/data-seq-dia-protocol-single-node';
import DataSeqDiaProtocolSingleNodeResult from '../data/data-seq-dia-protocol-single-node-result';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaServerAttached {
  static InfoAttached = 'Attached';
    
  static store(msg, sharedTemplateData, type) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaProtocolSingleNode.getSize(sharedTemplateData));
    DataSeqDiaProtocolSingleNode.store(encoder, msg, sharedTemplateData, Const.PART_RIGHT);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolSingleNode.restore(decoder, DataSeqDiaServerAttached.InfoAttached, DataSeqDiaProtocolSingleNodeResult);
    result.reverseDirection = true;
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaServerAttached;
