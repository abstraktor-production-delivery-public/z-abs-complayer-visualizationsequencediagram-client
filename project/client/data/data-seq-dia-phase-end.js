
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaPhaseEnd {
  static size = Encoder.Uint8Size + Encoder.Uint8ArraySize + Encoder.Uint8ArraySize;
  
  static store(msg, sharedTemplateData) {
    const actorPhases = sharedTemplateData.analyze.actorPhases;
    const actorParts = sharedTemplateData.analyze.actorParts;
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaPhaseEnd.size + actorPhases.length + actorParts.length);
    encoder.setUint8(sharedTemplateData.dataLogRow.phaseId);
    encoder.setUint8Array(actorPhases);
    encoder.setUint8Array(actorParts);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const phaseId = decoder.getUint8();
    const actorPhases = decoder.getUint8Array();
    const actorParts = decoder.getUint8Array();
    return {
      phaseId,
      actorPhases,
      actorParts
    };
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaPhaseEnd;
