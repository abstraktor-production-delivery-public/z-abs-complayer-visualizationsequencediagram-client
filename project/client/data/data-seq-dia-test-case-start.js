
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import HighResolutionDate from 'z-abs-corelayer-cs/clientServer/time/high-resolution-date';


class DataSeqDiaTestCaseStart {
  static size = Encoder.CtSize + Encoder.Uint64Size;
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaTestCaseStart.size);
    encoder.setCtStringInternal(msg.name);
    encoder.setBigUint64(msg.timestamp);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const name = decoder.getCtString();
    const date = new HighResolutionDate(decoder.getBigUint64()).getDateMilliSeconds();
    return {
      name,
      date
    };
  }
}


module.exports = DataSeqDiaTestCaseStart;
