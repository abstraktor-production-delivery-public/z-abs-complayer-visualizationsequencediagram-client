
'use strict';

import DataSeqDiaProtocolSingleNode from '../data/data-seq-dia-protocol-single-node';
import DataSeqDiaProtocolSingleNodeResult from '../data/data-seq-dia-protocol-single-node-result';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaConnectionClosing {
  static size = Encoder.Uint16Size + Encoder.Uint8Size;
  static InfoClosing = 'Closing';
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaConnectionClosing.size + DataSeqDiaProtocolSingleNode.getSize(sharedTemplateData));
    DataSeqDiaProtocolSingleNode.store(encoder, msg, sharedTemplateData, Const.PART_LEFT);
    const remote = msg.data.remote;
    encoder.setUint16(remote.actorIndex);
    encoder.setUint1_0(remote.isShared ? 1 : 0, true);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolSingleNode.restore(decoder, DataSeqDiaConnectionClosing.InfoClosing, DataSeqDiaProtocolSingleNodeResult);
    const remoteActorIndex = decoder.getUint16();
    const remoteIsShared = !!decoder.getUint1_0(true);
    result.reverseDirection = remoteActorIndex < result.localActorIndex;
    result.extraData = {
      remoteIsShared,
      isConnection: true
    };
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaConnectionClosing;
