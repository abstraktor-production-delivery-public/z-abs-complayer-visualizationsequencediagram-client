
'use strict';

import Const from '../logic/const';


class DataSeqDiaProtocolSingleNodeResult {
  constructor(info, phaseId, protocolIndex, transportType, localActorIndex, localName, localType, localIp, localOwnerId, localId, localIsShared, actorPhases, actorParts) {
    this.renderNodes = Const.RENDER_SINGLE_NODE;
    this.reverseDirection = false;
    this.phaseId = phaseId;
    this.protocolIndex = protocolIndex;
    this.transportType = transportType;
    this.localActorIndex = localActorIndex;
    this.localIp = localIp;
    this.localName = localName;
    this.localType = localType;
    this.localOwnerId = localOwnerId;
    this.localId = localId;
    this.localIsShared = localIsShared;
    this.info = info;
    this.actorPhases = actorPhases;
    this.actorParts = actorParts;
    this.extraData = null;
  }
}


module.exports = DataSeqDiaProtocolSingleNodeResult;
