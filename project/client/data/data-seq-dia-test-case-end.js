
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import HighResolutionDuration from 'z-abs-corelayer-cs/clientServer/time/high-resolution-duration';


class DataSeqDiaTestCaseEnd {
  static size = Encoder.Uint8Size + Encoder.Uint64Size;
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaTestCaseEnd.size);
    encoder.setUint8(msg.resultId);
    encoder.setBigUint64(msg.duration);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const resultId = decoder.getUint8();
    const duration = new HighResolutionDuration(decoder.getBigUint64()).getDuration();
    return {
      resultId,
      duration
    };
  }
}


module.exports = DataSeqDiaTestCaseEnd;
