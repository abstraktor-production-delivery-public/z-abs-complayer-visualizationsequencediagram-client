
'use strict';


class DataSeqDiaConnectionUpgrading {
  static appDeserializer = null;
  
  static init(appDeserializer) {
    DataSeqDiaConnectionUpgrading.appDeserializer = appDeserializer;
  }
  
  constructor(sharedTemplateData, dataTestCase, dataConnection, upgraded) {
    
    const data = sharedTemplateData.dataLogRow.logRow.data;
    this.upgraded = upgraded;
    
    this.localType = data.local.type;
    this.connectionId = `${dataConnection.clientStackId},${dataConnection.serverStackId}`;
    this.both = true;
    this.showConnection = true;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaConnectionUpgrading;
