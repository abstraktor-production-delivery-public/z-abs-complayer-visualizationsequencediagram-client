
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';

class DataSeqDiaGui {
  static size = Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint16Size + Encoder.Uint16Size + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint8ArraySize + Encoder.Uint8ArraySize + Encoder.CtSize;
  static stackType = ['client', 'server'];
  
  static store(msg, sharedTemplateData) {
    const dataLogRow = sharedTemplateData.dataLogRow;
    const data = msg.data;
    const actorPhases = sharedTemplateData.analyze.actorPhases;
    const actorParts = sharedTemplateData.analyze.actorParts;
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaGui.size + actorPhases.length + actorParts.length + encoder.getStringBytes(data.data.args));
    encoder.setUint8(dataLogRow.phaseId);
    encoder.setUint16(dataLogRow.protocolIndex);
    encoder.setUint8(data.type);
    encoder.setUint16(data.actorIndex);
    encoder.setUint8(data.stackId);
    encoder.setUint8(data.stackType);
    encoder.setUint8Array(actorPhases);
    encoder.setUint8Array(actorParts);
    encoder.setCtString(data.data.name);
    encoder.setString(data.data.args);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const phaseId = decoder.getUint8();
    const protocolIndex = decoder.getUint16();
    const type = decoder.getUint8();
    const actorIndex = decoder.getUint16();
    const stackId = decoder.getUint8();
    const stackType = decoder.getUint8();
    const actorPhases = decoder.getUint8Array();
    const actorParts = decoder.getUint8Array();
    const dataName = decoder.getCtString();
    const dataArgs = decoder.getString();
    return {
      phaseId,
      protocolIndex,
      actorIndex,
      actorPhases,
      actorParts,
      stackData: `${DataSeqDiaGui.stackType[stackType]} ${stackId}`,
      type: type,
      data: {
        name: dataName,
        args: dataArgs
      },
      dataBrowser: 'browser',
      page: 'page'
    };
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    const phaseId = decoder.getUint8();
    decoder.inc(Encoder.Uint16Size);
    const type = decoder.getUint8();
    return {
      phaseId,
      type
    };
  }
}


module.exports = DataSeqDiaGui;

/*this.data = sharedTemplateData.dataLogRow.logRow.data;
    this.dataType = sharedTemplateData.dataLogRow.logRow.data.type;
    this.dataArgs = sharedTemplateData.dataLogRow.logRow.data.data.args;
    this.dataName = sharedTemplateData.dataLogRow.logRow.data.data.name;
    this.dataBrowser = srcAddress ? (srcAddress.addressName + (srcAddress.incognitoBrowser ? (':' + srcAddress.incognitoBrowser) : '')) : '';
    this.page = srcAddress ? srcAddress.page : '';*/