
'use strict';

import Const from '../logic/const';


class DataSeqDiaProtocolTwoNodesResult {
  constructor(info, phaseId, protocolIndex, transportType, localActorIndex, localName, localType, localIp, localOwnerId, localId, localIsShared, actorPhases, actorParts) {
    this.renderNodes = Const.RENDER_SINGLE_NODE;
    this.renderPosition = Const.RENDER_POSITION_NORMAL;
    this.reverseDirection = false;
    this.phaseId = phaseId;
    this.protocolIndex = protocolIndex;
    this.transportType = transportType;
    this.localActorIndex = localActorIndex;
    this.localName = localName;
    this.localType = localType;
    this.localIp = localIp;
    this.localOwnerId = localOwnerId;
    this.localId = localId;
    this.localIsShared = localIsShared;
    this.remoteActorIndex = 0;
    this.remoteIp = null;
    this.remoteName = null;
    this.remoteType = 0;
    this.remoteOwnerId = 0;
    this.remoteId = 0;
    this.remoteIsShared = false;
    this.info = info;
    this.actorPhases = actorPhases;
    this.actorParts = actorParts;
    this.showConnection = false;
    this.protocolLine = Const.PROTOCOL_LINE_NONE;
  }
  
  setRemote(remoteActorIndex, remoteName, remoteType, remoteIp, remoteOwnerId, remoteId, remoteIsShared) {
    this.remoteActorIndex = remoteActorIndex;
    this.remoteName = remoteName;
    this.remoteType = remoteType;
    this.remoteIp = remoteIp;
    this.remoteOwnerId = remoteOwnerId;
    this.remoteId = remoteId;
    this.remoteIsShared = remoteIsShared;
  }
}


module.exports = DataSeqDiaProtocolTwoNodesResult;
