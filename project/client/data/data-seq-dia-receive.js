
'use strict';

import DataSeqDiaProtocolTwoNodes from '../data/data-seq-dia-protocol-two-nodes';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaReceive {
  static size = Encoder.CtSize;
  static InfoReceive = 'Receive';
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaReceive.size + DataSeqDiaProtocolTwoNodes.getSize(sharedTemplateData));
    DataSeqDiaProtocolTwoNodes.store(encoder, msg, sharedTemplateData, Const.PART_LEFT);
    encoder.setCtStringInternal(msg.data.caption);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolTwoNodes.restore(decoder, DataSeqDiaReceive.InfoReceive);
    const caption = decoder.getCtString();;
    result.extraData = {
      caption
    };
    result.renderNodes = Const.RENDER_TWO_NODES;
    result.renderPosition = Const.RENDER_POSITION_BOTTOM;
    result.protocolLine = Const.PROTOCOL_LINE_MESSAGE_PART;
    result.reverseDirection = result.remoteActorIndex < result.localActorIndex;
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaReceive;

