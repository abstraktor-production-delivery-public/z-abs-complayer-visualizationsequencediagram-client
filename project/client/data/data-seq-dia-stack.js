
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaStack {
  static size = Encoder.Uint8Size + Encoder.Uint16Size + Encoder.Uint16Size + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint8ArraySize + Encoder.Uint8ArraySize;
  static stackType = ['client', 'server'];
    
  static store(msg, sharedTemplateData) {
    const actorPhases = sharedTemplateData.analyze.actorPhases;
    const actorParts = sharedTemplateData.analyze.actorParts;
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaStack.size + actorPhases.length + actorParts.length);
    
    const dataLogRow = sharedTemplateData.dataLogRow;
    encoder.setUint8(dataLogRow.phaseId);
    encoder.setUint16(dataLogRow.protocolIndex);
    const data = msg.data;
    encoder.setUint16(data.actorIndex);
    encoder.setUint8(data.stackId);
    encoder.setUint8(data.stackType);
    encoder.setUint8(data.type);
    encoder.setUint8Array(actorPhases);
    encoder.setUint8Array(actorParts);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const phaseId = decoder.getUint8();
    const protocolIndex = decoder.getUint16();
    const actorIndex = decoder.getUint16();
    const stackId = decoder.getUint8();
    const stackType = decoder.getUint8();
    const type = decoder.getUint8();
    const actorPhases = decoder.getUint8Array();
    const actorParts = decoder.getUint8Array();
    return {
      phaseId,
      protocolIndex,
      actorIndex,
      stackData: `${DataSeqDiaStack.stackType[stackType]} ${stackId}`,
      type,
      actorPhases,
      actorParts
    };
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaStack;
