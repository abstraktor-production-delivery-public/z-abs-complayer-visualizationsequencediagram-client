
'use strict';

import Const from '../logic/const';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';


class DataSeqDiaProtocolSingleNode {
  static size = Encoder.Uint8Size + Encoder.Uint16Size + Encoder.Uint16Size + Encoder.CtSize + Encoder.Uint16Size + Encoder.CtSize + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint32Size + Encoder.Uint32Size + Encoder.Uint8ArraySize + Encoder.Uint8ArraySize;
  
  static getSize(sharedTemplateData) {
    return DataSeqDiaProtocolSingleNode.size + sharedTemplateData.analyze.actorPhases.length + sharedTemplateData.analyze.actorParts.length;
  }
  
  static store(encoder, msg, sharedTemplateData) {
    const dataLogRow = sharedTemplateData.dataLogRow;
    encoder.setUint8(dataLogRow.phaseId);
    encoder.setUint16(dataLogRow.protocolIndex);
    const local = msg.data.local;
    encoder.setUint16(local.actorIndex);
    encoder.setCtStringInternal(local.host);
    encoder.setUint16(local.port);
    encoder.setCtStringInternal(local.name);
    encoder.setUint1_0('client' === local.type ? Const.PART_TYPE_CLIENT : Const.PART_TYPE_SERVER, false);
    encoder.setUint1_1(local.isShared ? 1 : 0, true);
    encoder.setUint8(local.transportLayer.transportType);
    encoder.setUint32(local.ownerId);
    encoder.setUint32(local.id);
    const actorPhases = sharedTemplateData.analyze.actorPhases;
    const actorParts = sharedTemplateData.analyze.actorParts;
    encoder.setUint8Array(actorPhases);
    encoder.setUint8Array(actorParts);
  }

  static restore(decoder, info, resultFactory) {
    const phaseId = decoder.getUint8();
    const protocolIndex = decoder.getUint16();
    const localActorIndex = decoder.getUint16();
    const host = decoder.getCtString();
    const port = decoder.getUint16();
    const localName = decoder.getCtString();
    const localType = decoder.getUint1_0(false);
    const localIsShared = !!decoder.getUint1_1(true);
    const transportType = decoder.getUint8();
    const localOwnerId = decoder.getUint32();
    const localId = decoder.getUint32();
    const localIp = `${host}:${port}`;
    const actorPhases = decoder.getUint8Array();
    const actorParts = decoder.getUint8Array();
    return new resultFactory(info, phaseId, protocolIndex, transportType, localActorIndex, localName, localType, localIp, localOwnerId, localId, localIsShared, actorPhases, actorParts);
  }
  
  static restoreWhitChoise(decoder, infoFunc, resultFactory) {
    const phaseId = decoder.getUint8();
    const protocolIndex = decoder.getUint16();
    const localActorIndex = decoder.getUint16();
    const host = decoder.getCtString();
    const port = decoder.getUint16();
    const localName = decoder.getCtString();
    const localType = decoder.getUint1_0(false);
    const localIsShared = !!decoder.getUint1_1(true);
    const transportType = decoder.getUint8();
    const localOwnerId = decoder.getUint32();
    const localId = decoder.getUint32();
    const localIp = `${host}:${port}`;
    const actorPhases = decoder.getUint8Array();
    const actorParts = decoder.getUint8Array();
    const info = infoFunc(transportType);
    return new resultFactory(info, phaseId, protocolIndex, transportType, localActorIndex, localName, localType, localIp, localOwnerId, localId, localIsShared, actorPhases, actorParts);
  }
}


module.exports = DataSeqDiaProtocolSingleNode;
