
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaTestCaseHeader {
  static size = Encoder.Uint8ArraySize + Encoder.CtSize;
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const actorPhases = sharedTemplateData.analyze.actorPhases;
    const actorNames = sharedTemplateData.analyze.actorNames;
    const buffer = encoder.createBuffer(DataSeqDiaTestCaseHeader.size + actorPhases.length + Encoder.CtSize * actorNames.length);
    encoder.setUint8Array(actorPhases);
    encoder.setCtStringArrayInternal(actorNames);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const actorPhases = decoder.getUint8Array();
    const actorNames = decoder.getCtStringArray();
    return {
      actorPhases,
      actorNames
    };
  }
}


module.exports = DataSeqDiaTestCaseHeader;
