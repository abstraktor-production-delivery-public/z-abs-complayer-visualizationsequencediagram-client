
'use strict';

import DataSeqDiaProtocolTwoNodes from '../data/data-seq-dia-protocol-two-nodes';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaClientConnected {
  static size = Encoder.Uint8Size;
  static InfoConnected = 'Connected';
  static InfoMembershipAdded = 'Membership added';
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaClientConnected.size + DataSeqDiaProtocolTwoNodes.getSize(sharedTemplateData));
    DataSeqDiaProtocolTwoNodes.store(encoder, msg, sharedTemplateData, Const.PART_LEFT);
    encoder.setUint2_0(msg.data.local.actionId, true);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolTwoNodes.restoreWhitChoise(decoder, (transportType) => {
      if(Const.TRANSPORT_TYPE_MC === transportType) {
        return DataSeqDiaClientConnected.InfoMembershipAdded;
      }
      else {
        return DataSeqDiaClientConnected.InfoConnected;
      }
    });
    const actionId = decoder.getUint2_0(true);
    if(1 === actionId) {
      result.renderNodes = Const.RENDER_TWO_NODES;
      result.protocolLine = Const.PROTOCOL_LINE_CONNECTION;
    }
    result.reverseDirection = result.remoteActorIndex < result.localActorIndex;
    result.extraData = {
      isConnection: true
    };
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaClientConnected;
