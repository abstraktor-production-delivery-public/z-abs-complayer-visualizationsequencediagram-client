
'use strict';

import DataSeqDiaProtocolTwoNodes from '../data/data-seq-dia-protocol-two-nodes';
import Const from '../logic/const';
import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaServerAccepted {
  static size = Encoder.Uint8Size;
  static InfoAccepted = 'Accepted';
  
  static store(msg, sharedTemplateData) {
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaServerAccepted.size + DataSeqDiaProtocolTwoNodes.getSize(sharedTemplateData));
    DataSeqDiaProtocolTwoNodes.store(encoder, msg, sharedTemplateData);
    encoder.setUint2_1(msg.data.local.actionId, true);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const result = DataSeqDiaProtocolTwoNodes.restore(decoder, DataSeqDiaServerAccepted.InfoAccepted);
    const actionId = decoder.getUint2_1(true);
    if(1 === actionId) {
      result.renderNodes = Const.RENDER_TWO_NODES;
      result.protocolLine = Const.PROTOCOL_LINE_CONNECTION;
    }
    result.reverseDirection = result.remoteActorIndex < result.localActorIndex;
    result.extraData = {
      isConnection: true
    };
    #BUILD_DEBUG_START
    Object.preventExtensions(result);
    #BUILD_DEBUG_STOP
    return result;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaServerAccepted;
