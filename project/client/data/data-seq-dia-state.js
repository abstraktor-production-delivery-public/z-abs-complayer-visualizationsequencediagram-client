
'use strict';

import Decoder from 'z-abs-corelayer-client/client/communication/core-protocol/decoder';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class DataSeqDiaState {
  static size = Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint8Size + Encoder.Uint16Size + Encoder.Uint8ArraySize + Encoder.Uint8ArraySize;
  static inc = Encoder.Uint8Size + Encoder.Uint8Size;
  
  static store(msg, sharedTemplateData, stateType) {
    const actorPhases = sharedTemplateData.analyze.actorPhases;
    const actorParts = sharedTemplateData.analyze.actorParts;
    const encoder = new Encoder(StoreBaseRealtime.textCache);
    const buffer = encoder.createBuffer(DataSeqDiaState.size + actorPhases.length + actorParts.length);
    encoder.setUint8(sharedTemplateData.dataLogRow.phaseId);
    encoder.setUint8(msg.stateResultIndex);
    encoder.setUint8(msg.stateIndex);
    encoder.setUint8(stateType);
    encoder.setUint16(msg.actorIndex);
    encoder.setUint8Array(actorPhases);
    encoder.setUint8Array(actorParts);
    return buffer;
  }
  
  static restore(buffer) {
    const decoder = new Decoder(StoreBaseRealtime.textCache, buffer);
    const phaseId = decoder.getUint8();
    const stateResultIndex = decoder.getUint8();
    const stateIndex = decoder.getUint8();
    const stateType = decoder.getUint8();
    const actorIndex = decoder.getUint16();
    const actorPhases = decoder.getUint8Array();
    const actorParts = decoder.getUint8Array();
    return {
      phaseId,
      stateResultIndex,
      stateIndex,
      stateType,
      actorIndex,
      actorPhases,
      actorParts
    };
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    const phaseId = decoder.getUint8();
    decoder.inc(DataSeqDiaState.inc);
    const stateType = decoder.getUint8();
    return {
      phaseId,
      stateType
    }
  }
}


module.exports = DataSeqDiaState;
