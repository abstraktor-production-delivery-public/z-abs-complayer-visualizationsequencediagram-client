
'use strict';


class DataSeqDiaClientDnsFailure {
  static appDeserializer = null;
  
  static init(appDeserializer) {
    DataSeqDiaClientDnsFailure.appDeserializer = appDeserializer;
  }
  
  constructor(sharedTemplateData, dataTestCase) {

    const data = sharedTemplateData.dataLogRow.logRow.data;
    
    this.actorIndex = sharedTemplateData.dataLogRow.actorIndex;
    this.dataTestCase = dataTestCase;
    this.dstUrl = data.dstUrl;
  }
  
  static restoreHeightData(buffer) {
    const decoder = new Decoder(null, buffer);
    return decoder.getUint8();
  }
}


module.exports = DataSeqDiaClientDnsFailure;
