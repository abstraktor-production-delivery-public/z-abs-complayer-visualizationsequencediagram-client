
'use strict';

import Const from '../logic/const';
import DataSeqDiaProtocolSingleNode from '../data/data-seq-dia-protocol-single-node';
import DataSeqDiaProtocolTwoNodesResult from '../data/data-seq-dia-protocol-two-nodes-result';
import Encoder from 'z-abs-corelayer-client/client/communication/core-protocol/encoder';


class DataSeqDiaProtocolTwoNodes {
  static size = Encoder.CtSize + Encoder.Uint16Size + Encoder.CtSize + Encoder.Uint16Size + Encoder.Uint32Size + Encoder.Uint32Size + Encoder.Uint8Size;
  
  static getSize(sharedTemplateData) {
    const size = DataSeqDiaProtocolSingleNode.getSize(sharedTemplateData);
    return DataSeqDiaProtocolTwoNodes.size + size;
  }
  
  static store(encoder, msg, sharedTemplateData, part) {
    DataSeqDiaProtocolSingleNode.store(encoder, msg, sharedTemplateData, part);
    const remote = msg.data.remote;
    encoder.setCtStringInternal(remote.host);
    encoder.setUint16(remote.port);
    encoder.setCtStringInternal(remote.name);
    encoder.setUint16(remote.actorIndex);
    encoder.setUint32(remote.ownerId);
    encoder.setUint32(remote.id);
    encoder.setUint1_0('client' === remote.type ? Const.PART_TYPE_CLIENT : Const.PART_TYPE_SERVER, false);
    encoder.setUint1_1(remote.isShared ? 1 : 0, true);
  }
  
  static restore(decoder, info) {
    const result = DataSeqDiaProtocolSingleNode.restore(decoder, info, DataSeqDiaProtocolTwoNodesResult);
    const host = decoder.getCtString();
    const port = decoder.getUint16();
    const remoteName = decoder.getCtString();
    const remoteIp = `${host}:${port}`;
    const remoteActorIndex = decoder.getUint16();
    const remoteOwnerId = decoder.getUint32();
    const remoteId = decoder.getUint32();
    const remoteType = decoder.getUint1_0(false);
    const remoteIsShared = !!decoder.getUint1_1(true);
    result.setRemote(remoteActorIndex, remoteName, remoteType, remoteIp, remoteOwnerId, remoteId, remoteIsShared);
    return result;
  }
  
  static restoreWhitChoise(decoder, infoFunc) {
    const result = DataSeqDiaProtocolSingleNode.restoreWhitChoise(decoder, infoFunc, DataSeqDiaProtocolTwoNodesResult);
    const host = decoder.getCtString();
    const port = decoder.getUint16();
    const remoteName = decoder.getCtString();
    const remoteIp = `${host}:${port}`;
    const remoteActorIndex = decoder.getUint16();
    const remoteOwnerId = decoder.getUint32();
    const remoteId = decoder.getUint32();
    const remoteType = decoder.getUint1_0(false);
    const remoteIsShared = !!decoder.getUint1_1(true);
    result.setRemote(remoteActorIndex, remoteName, remoteType, remoteIp, remoteOwnerId, remoteId, remoteIsShared);
    return result;
  }
}


module.exports = DataSeqDiaProtocolTwoNodes;
