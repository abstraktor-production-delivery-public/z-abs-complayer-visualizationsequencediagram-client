
'use strict';

import ImageDecoder from './image-decoder';
import TextDecoder from './text-decoder';


class ContentDecoder {

  static decode(buffer, toolbarNodes, contentType, options, sizeF) {
    if(contentType.startsWith('text/') || contentType.startsWith('application/json') || contentType.startsWith('application/javascript') || contentType.startsWith('application/x-www-form-urlencoded')) {
      return TextDecoder.decode(buffer, toolbarNodes, contentType, options, sizeF);
    }
    else if(contentType.startsWith('image/')) {
      return ImageDecoder.decode(buffer, toolbarNodes, contentType, options, sizeF);
    }
    else {
      return null;
    }
  }
}

module.exports = ContentDecoder;
