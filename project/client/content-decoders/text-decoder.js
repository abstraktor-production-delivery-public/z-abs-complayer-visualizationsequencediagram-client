
'use strict';


class TextDecoder {
  constructor() {
    this.dynamicInnerText = document.createElement('pre');
    this.dynamicInnerText.classList.add('inner');
    this.dynamicInnerText.classList.add('seq_dia_protocol_base');
    this.dynamicInnerText.classList.add('seq_dia_protocol');
    this.dynamicInnerText.appendChild(document.createTextNode(''));
    
    this.dynamicInnerBufferText = this.dynamicInnerText.cloneNode(true);
    this.dynamicInnerBufferText.classList.replace('inner', 'seq_dia_inner_buffer');
  }

  decode(buffer, toolbarNodes, contentType, options, sizeF) {
    const textNode = this.dynamicInnerBufferText.cloneNode(true);
    const decoder = new window.TextDecoder("utf-8");
    const dataBuffer = undefined !== buffer.data ? decoder.decode(Uint8Array.from(buffer.data)) : buffer;
    if(contentType.startsWith('application/json')) {
      textNode.firstChild.nodeValue = this._createInnerJson(textNode, toolbarNodes, dataBuffer, options, sizeF);
    }
    else {
      textNode.firstChild.nodeValue = dataBuffer;
    }
    if(sizeF) {
      setTimeout(() => {
        sizeF(textNode, textNode.clientWidth, textNode.clientHeight);
      });
    }
    return textNode;
  }
  
  _parseJson(buffer, space) {
    try {
      return JSON.stringify(JSON.parse(buffer), null, space);
    }
    catch(err) {
      return buffer;
    }
  }
  
  _createInnerJson(node, toolbarNodes, buffer, options, cbSize) {
    const valueOpen = this._parseJson(buffer, 2);
    const valueClosed = this._parseJson(buffer);
    // display text toolbar
    toolbarNodes.forEach((toolbarNode) => {
      if(toolbarNode.classList.contains('seq_dia_json')) {
        let open = true;
        toolbarNode.addEventListener('click', (event) => {
          open = !open;
          node.firstChild.nodeValue = open ? valueOpen : valueClosed;
          if(cbSize) {
            cbSize(node, node.scrollWidth, node.scrollHeight);
          }
        });
      }
    });
    return valueOpen;
  }
}

module.exports = new TextDecoder();
