
'use strict';


class ImageDecoder {
  constructor() {
    this.dynamicInnerBufferImage = document.createElement('img');
    this.dynamicInnerBufferImage.classList.add('inner');
    this.dynamicInnerBufferImage.classList.add('seq_dia_protocol_base');
    this.dynamicInnerBufferImage.classList.add('seq_dia_protocol');
  
    this.dynamicInnerBufferImageFirst = this.dynamicInnerBufferImage.cloneNode(true);
    this.dynamicInnerBufferImageFirst.classList.replace('seq_dia_protocol', 'seq_dia_protocol_first');  
  }
  
  decode(buffer, toolbarNodes, contentType, options, sizeF) {
    const imageNode =  this.dynamicInnerBufferImageFirst.cloneNode(true);
    const blob = new Blob([this._getBuffer(buffer, options)], {type: contentType});
    const imageUrl = URL.createObjectURL(blob);
    imageNode.onload = () => {
      if(sizeF) {
        sizeF(imageNode, imageNode.naturalWidth, imageNode.naturalHeight);
      }
      URL.revokeObjectURL(imageUrl);
    };
    imageNode.onerror = (e) => {
      ddb.error('The image has failed.', e);
    };
    imageNode.src = imageUrl;
    return imageNode;
  }
  
  _getBuffer(buffer, options) {
    const realBuffer = "Buffer" === buffer.type ? Uint8Array.from(buffer.data) : buffer;
    if(options?.transferEncoding) {
      const result = this._parseChunked(realBuffer);
      if(result.result) {
        return result.buffer;
      }
    }
    else {
      return realBuffer;
    }
  }
  
  _parseChunked(buffer) {
    const buffers = [];
    const result = {
      buffer: null,
      result: false
    };
    let pos = 0;
    let start = 0;
    while(true) {
      const r = buffer.indexOf(13, pos); // \r
      if(-1 !== r) {
        const n = buffer.indexOf(10, r); // \n
        if(-1 !== n && 1 === n - r) {
          start = n + 1;
        }
        else {
          return result;
        }
      }
      else {
        return result;
      }
      const sizeString = new TextDecoder("utf-8").decode(buffer.slice(pos, start - 2));
      const size = Number.parseInt(sizeString, 16);
      if(0 === size) {
        if(1 === buffers.length) {
          result.buffer = buffers[0];
        }
        else {
          let length = 0;
          this.buffers.forEach((buffer) => {
            length += buffer.length;
          });
          let mergePos = 0;
          const mergedBuffer = new Uint8Array(length);
          this.buffers.forEach((buffer) => {
            mergedBuffer.set(buffer, mergePos);
            mergePos += buffer.length;
          });
          result.buffer = mergedBuffer;
        }
        result.result = true;
        return result;
      }
      const stop = start + size;
      buffers.push(buffer.slice(start, stop));
      pos = stop + 2; // \r\n
//      break;
    }
  }
}

module.exports = new ImageDecoder();
